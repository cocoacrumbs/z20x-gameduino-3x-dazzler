#include "spi.h"
#include "timer.h"
#include <String.h>

#include "GD_Defines.h"
#include "GD_Transport.h"

/* ************************************************************************* */

byte ft8xx_model;

/* ************************************************************************* */

void __GDTR_end(GD_Transport *self)   // end the SPI transaction
{
    unselect_Dazzler_3X_CS();
} /* end __GDTR_end */


unsigned int __GDTR_rd16(GD_Transport *self,
                         uint32_t addr)
{
    unsigned int r;

    __GDTR_start(self, addr);
    spi_transfer(0);                        // dummy
    r  = spi_transfer(0);
    r |= (spi_transfer(0) << 8);
    __GDTR_end(self);

    return r;
} /* end __GDTR_rd16 */


void __GDTR_start(GD_Transport *self,
                  uint32_t addr)            // start an SPI transaction to addr
{
    select_Dazzler_3X_CS();
    spi_transfer((addr >> 16) & 0xFF);
    spi_transfer((addr >> 8) & 0xFF);
    spi_transfer(addr & 0xFF);
} /* end __GDTR_start */


void __GDTR_wr16(GD_Transport *self,
                 uint32_t addr, 
                 unsigned int v)
{
    __GDTR_wstart(self, addr);
    spi_transfer(lowByte(v));
    spi_transfer(highByte(v));
    __GDTR_end(self);
} /* end __GDTR_wr16 */


void __GDTR_wstart(GD_Transport *self,
                   uint32_t addr)           // start an SPI write transaction to addr
{
    select_Dazzler_3X_CS();
    spi_transfer(0x80 | ((addr >> 16) & 0xFF));
    spi_transfer((addr >> 8) & 0xFF);
    spi_transfer(addr & 0xFF);
} /* end __GDTR_wstart */


void GDTR_begin0(GD_Transport *self)
{
    GDTR_hostcmd(self, 0x42);    // SLEEP
    GDTR_hostcmd(self, 0x00);    // ACTIVE
    GDTR_hostcmd(self, 0x68);    // RST_PULSE
} /* end GDTR_begin0 */


void GDTR_begin1(GD_Transport *self) 
{
    delayms(320);

    // So that FT800,801      FT810-3   FT815,6
    // model       0             1         2
    switch (__GDTR_rd16(self, 0x0c0000UL) >> 8) 
    {
        case 0x10:
        case 0x11:
        case 0x12:
        case 0x13: ft8xx_model = 1; break;

        case 0x15:
        case 0x16: ft8xx_model = 2; break;

        default:   ft8xx_model = 2; break;
    } /* end switch */

    self->wp        = 0;
    self->freespace = 4096 - 4;

    GDTR_stream(self);
} /* end GDTR_begin1 */


void GDTR_bulk(GD_Transport *self,
               uint32_t addr) 
{
    __GDTR_end(self);                // stop streaming
    __GDTR_start(self, addr);
} /* end GDTR_bulk */


void GDTR_capture_error_message(GD_Transport *self)
{
    byte    i;

    __GDTR_end(self);
    if (ft8xx_model >= 2)
        for (i = 0; i < 128; i += 2)
        __GDTR_wr16(self, i, __GDTR_rd16(self, 0x309800UL + i));
} /* end GDTR_capture_error_message */


void GDTR_cmd32(GD_Transport *self, 
                uint32_t x)
{
    union {
      uint32_t c;
      uint8_t b[4];
    } data;

    if (self->freespace < 4) 
      GDTR_getfree(self, 4);

    self->wp += 4;
    self->freespace -= 4;

    data.c = x;
    spi_transfer(data.b[0]);
    spi_transfer(data.b[1]);
    spi_transfer(data.b[2]);
    spi_transfer(data.b[3]);
} /* end GDTR_cmd32 */


void GDTR_cmdbyte(GD_Transport *self, 
                  byte x) 
{
    if (self->freespace == 0) 
    {
      GDTR_getfree(self, 1);
    } /* end if */
    self->wp++;
    self->freespace--;
    spi_transfer(x);
} /* end GDTR_cmdbyte */


void GDTR_cmd_n(GD_Transport *self, 
                byte *s, 
                uint16_t n) 
{
    if (self->freespace < n) 
        GDTR_getfree(self, n);

    self->wp += n;
    self->freespace -= n;

    while (n > 8) 
    {
        n -= 8;
        spi_transfer(*s++);
        spi_transfer(*s++);
        spi_transfer(*s++);
        spi_transfer(*s++);
        spi_transfer(*s++);
        spi_transfer(*s++);
        spi_transfer(*s++);
        spi_transfer(*s++);
    } /* end while */

    while (n--)
        spi_transfer(*s++);
} /* end GDTR_cmd_n */


void GDTR_coprocessor_recovery(GD_Transport *self) 
{
    byte    i;
    
    __GDTR_end(self);

    if (ft8xx_model >= 2)
        for (i = 0; i < 128; i += 2)
            __GDTR_wr16(self, i, __GDTR_rd16(self, 0x309800UL + i));

    __GDTR_wr16(self, REG_CPURESET, 1);
    __GDTR_wr16(self, REG_CMD_WRITE, 0);
    __GDTR_wr16(self, REG_CMD_READ, 0);
    self->wp = 0;
    __GDTR_wr16(self,REG_CPURESET, 0);
    GDTR_stream(self);
} /* end GDTR_coprocessor_recovery */


void GDTR_daz_rd(GD_Transport *self,
                 uint8_t *s, 
                 uint32_t n) 
{   
    __GDTR_end(self);
    // digitalWrite(10, LOW);
    select_Dazzler_3X_Terminal_CS();
    memset(s, 0xFF, n);
    // spi_transfer((char*)s, n);
    while (n--)
        *s++ = spi_transfer(0);
    // digitalWrite(10, HIGH);
    unselect_Dazzler_3X_Terminal_CS();
    GDTR_resume(self);
} /* end GDTR_daz_rd */


void GDTR_external_crystal(GD_Transport *self) 
{
    __GDTR_end(self);
    GDTR_hostcmd(self, 0x44);
} /* end GDTR_external_crystal */


void GDTR_finish(GD_Transport *self) 
{
    self->wp &= 0xffc;
    __GDTR_end(self);
    __GDTR_wr16(self, REG_CMD_WRITE, self->wp);
    while (GDTR_rp(self) != self->wp)
        ;
    GDTR_stream(self);
} /* end GDTR_finish */


void GDTR_flush(GD_Transport *self) 
{
    GDTR_getfree(self, 0);
} /* end GDTR_flush */


void GDTR_getfree(GD_Transport *self, 
                  uint16_t n)
{
    self->wp &= 0xFFF;
    __GDTR_end(self);
    __GDTR_wr16(self, REG_CMD_WRITE, self->wp & 0xFFC);
    do 
    {
        uint16_t fullness = (self->wp - GDTR_rp(self)) & 4095;
        self->freespace = (4096 - 4) - fullness;
    } while (self->freespace < n);
    GDTR_stream(self);
} /* end GDTR_getfree */


uint32_t GDTR_getwp(GD_Transport *self) 
{
    return RAM_CMD + (self->wp & 0xffc);
} /* end GDTR_getwp */


void GDTR_hostcmd(GD_Transport *self,
                  byte a)
{
    select_Dazzler_3X_CS();
    spi_transfer(a);
    spi_transfer(0x00);
    spi_transfer(0x00);
    unselect_Dazzler_3X_CS();
} /* end GDTR_hostcmd */


void GDTR_resume(GD_Transport *self)
{
    GDTR_stream(self);
} /* end GDTR_resume */


byte GDTR_rd(GD_Transport *self,
             uint32_t addr)
{
    byte r;
    
    __GDTR_end(self);                    // stop streaming
    __GDTR_start(self, addr);
    spi_transfer(0);                            // dummy
    r = spi_transfer(0);
    GDTR_stream(self);

    return r;
} /* end GDTR_rd */


uint16_t GDTR_rd16(GD_Transport *self,
                   uint32_t addr)
{
    uint16_t r;

    __GDTR_end(self);                    // stop streaming
    __GDTR_start(self, addr);
    spi_transfer(0);
    r  = spi_transfer(0);
    r |= (spi_transfer(0) << 8);

    GDTR_stream(self);
    return r;
} /* end GDTR_rd16 */


uint32_t GDTR_rd32(GD_Transport *self,
                   uint32_t addr)
{
    union {
      uint32_t  c;
      uint8_t   b[4];
    } data;
    
    __GDTR_end(self);                    // stop streaming
    __GDTR_start(self, addr);
    spi_transfer(0);

    data.b[0] = spi_transfer(0);
    data.b[1] = spi_transfer(0);
    data.b[2] = spi_transfer(0);
    data.b[3] = spi_transfer(0);

    GDTR_stream(self);
    return data.c;
} /* end GDTR_rd32 */


void GDTR_rd_n(GD_Transport *self,
               byte *dst, 
               uint32_t addr, 
               uint16_t n)
{
    __GDTR_end(self);                    // stop streaming
    __GDTR_start(self, addr);
    spi_transfer(0);
    while (n--)
        *dst++ = spi_transfer(0);
    GDTR_stream(self);
} /* end GDTR_rd_n */


uint16_t GDTR_rp(GD_Transport *self) 
{
    uint16_t r = __GDTR_rd16(self, REG_CMD_READ);
    // if (r == 0xfff)
    //     GD_alert();
    //     printf("GD_alert()\n");
    return r;
} /* end GDTR_rp */


void GDTR_stop(GD_Transport *self)
{
    self->wp &= 0xFFC;

    __GDTR_end(self);
    __GDTR_wr16(self, REG_CMD_WRITE, self->wp);
} /* end GDTR_stop */


void GDTR_stream(GD_Transport *self) 
{
    __GDTR_end(self);
    __GDTR_wstart(self, RAM_CMD + (self->wp & 0xfff));
} /* end GDTR_stream */


void GDTR_wr(GD_Transport *self,
             uint32_t addr, 
             byte v)
{
    __GDTR_end(self);                    // stop streaming
    __GDTR_wstart(self, addr);
    spi_transfer(v);
    GDTR_stream(self);
} /* end GDTR_wr */


void GDTR_wr16(GD_Transport *self,
               uint32_t addr, 
               uint32_t v)
{
    __GDTR_end(self);                    // stop streaming
    __GDTR_wstart(self, addr);
    spi_transfer(v & 0xFF);
    spi_transfer((v >> 8) & 0xFF);
    GDTR_stream(self);
} /* end GDTR_wr16 */


void GDTR_wr32(GD_Transport *self,
               uint32_t addr, 
               unsigned long v)
{
    __GDTR_end(self);                    // stop streaming
    __GDTR_wstart(self, addr);
    spi_transfer(v & 0xFF);
    spi_transfer((v >> 8)  & 0xFF);
    spi_transfer((v >> 16) & 0xFF);
    spi_transfer((v >> 24) & 0xFF);
    
    GDTR_stream(self);
} /* end GDTR_wr32 */


void GDTR_wr_n(GD_Transport *self,
               uint32_t addr, 
               byte *src, 
               uint16_t n)
{
    
    __GDTR_end(self);            // stop streaming
    __GDTR_wstart(self, addr);
    while (n--)
      spi_transfer(*src++);

    GDTR_stream(self);
} /* end GDTR_wr_n */

/* ************************************************************************* */
