#ifndef SPI_H
#define SPI_H

void init_hw();

int spi_begin();
unsigned char spi_transfer(char d);

void select_Dazzler_3X_CS();
void unselect_Dazzler_3X_CS();
void deselect_Dazzler_3X_CS();

void select_Dazzler_SD_CS();
void unselect_Dazzler_SD_CS();
void deselect_Dazzler_SD_CS();

void select_Dazzler_3X_Terminal_CS();
void unselect_Dazzler_3X_Terminal_CS();
void deselect_Dazzler_3X_Terminal_CS();

#endif SPI_H
