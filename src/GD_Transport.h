#ifndef GD_TRANSPORT_H
#define GD_TRANSPORT_H

#include "GD_Defines.h"

/* ************************************************************************* */

typedef struct GD_Transport_T
{
    byte        streaming;
    uint16_t    wp;
    uint16_t    freespace;
} GD_Transport;

/* ************************************************************************* */

void __GDTR_end(GD_Transport *self);            // end the SPI transaction

unsigned int __GDTR_rd16(GD_Transport *self,
                         uint32_t addr);

void __GDTR_start(GD_Transport *self,
                  uint32_t addr);               // start an SPI transaction to addr

void __GDTR_wr16(GD_Transport *self,
                 uint32_t addr, 
                 unsigned int v);

void __GDTR_wstart(GD_Transport *self,
                   uint32_t addr);              // start an SPI write transaction to addr

void GDTR_begin0(GD_Transport *self);

void GDTR_begin1(GD_Transport *self);

void GDTR_bulk(GD_Transport *self,
               uint32_t addr);

void GDTR_capture_error_message(GD_Transport *self);

void GDTR_cmd32(GD_Transport *self, 
                uint32_t x);

void GDTR_cmdbyte(GD_Transport *self, 
                  byte x);

void GDTR_cmd_n(GD_Transport *self, 
                byte *s, 
                uint16_t n);

void GDTR_coprocessor_recovery(GD_Transport *self);

void GDTR_daz_rd(GD_Transport *self,
                 uint8_t *s, 
                 uint32_t n);

void GDTR_external_crystal(GD_Transport *self);

void GDTR_finish(GD_Transport *self);

void GDTR_flush(GD_Transport *self);

void GDTR_getfree(GD_Transport *self, 
                  uint16_t n);

uint32_t GDTR_getwp(GD_Transport *self);

void GDTR_hostcmd(GD_Transport *self,
                  byte a);

void GDTR_resume(GD_Transport *self);

byte GDTR_rd(GD_Transport *self,
             uint32_t addr);

uint16_t GDTR_rd16(GD_Transport *self,
                   uint32_t addr);

uint32_t GDTR_rd32(GD_Transport *self,
                   uint32_t addr);

void GDTR_rd_n(GD_Transport *self,
               byte *dst, 
               uint32_t addr, 
               uint16_t n);

uint16_t GDTR_rp(GD_Transport *self);

void GDTR_stop(GD_Transport *self);

void GDTR_stream(GD_Transport *self);

void GDTR_wr(GD_Transport *self,
             uint32_t addr, 
             byte v);

void GDTR_wr16(GD_Transport *self,
               uint32_t addr, 
               uint32_t v);

void GDTR_wr32(GD_Transport *self,
               uint32_t addr, 
               unsigned long v);


void GDTR_wr_n(GD_Transport *self,
               uint32_t addr, 
               byte *src, 
               uint16_t n);

/* ************************************************************************* */

#endif GD_TRANSPORT_H