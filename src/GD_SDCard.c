#include <stdio.h>

#include "GD_SDCard.h"
#include "timer.h"
#include "spi.h"


#define DEFAULT_LBA     0
#define DEFAULT_CRC     0x95


void GD_SDCard_appcmd(byte cc, 
                      uint32_t lba) 
{
    GD_SDCard_cmd(55, DEFAULT_LBA, DEFAULT_CRC);
    GD_SDCard_R1();
    GD_SDCard_cmd(cc, lba, DEFAULT_CRC);
} /* end GD_SDCard_appcmd */


void GD_SDCard_begin(GD_SDCard *self)
{
    byte    type_code;
    byte    sdhc;
    byte    r1;
    int     attempts;

    GD_SDCard_desel();
    delayms(50);                    // wait for boot
    GD_SDCard_delay(10);            // deselected, 80 pulses

    DEBUG_INFO("Attempting card reset...\n");
    attempts = 0;
    do 
    {                               // reset, enter idle
        GD_SDCard_cmd(0, DEFAULT_LBA, DEFAULT_CRC);
        while ((r1 = spi_transfer(0xFF)) & 0x80)
            if (attempts++ == 1000)
                goto finished;
        GD_SDCard_desel();
        spi_transfer(0xFF);         // trailing byte
        DEBUG_VAR(r1);
    } while (r1 != 1);
    DEBUG_INFO("reset ok\n");

    sdhc = 0;
    GD_SDCard_cmd(8, 0x1aa, 0x87);
    r1   = GD_SDCard_R7();
    sdhc = (r1 == 1);

    DEBUG_VAR(sdhc);

    DEBUG_INFO("Sending card init command\n");
    attempts = 0;
    while (1) 
    {
        GD_SDCard_appcmd(41, sdhc ? (1UL << 30) : 0); // card init
        r1 = GD_SDCard_R1();
        // DEBUG_VAR(r1);
        if ((r1 & 1) == 0)
            break;
        if (attempts++ == 300)
            goto finished;
        delayms(1);
    }
    DEBUG_INFO("OK\n");

    if (sdhc) 
    {
        uint32_t    OCR = 0;
        int         i;

        for (i = 10; i; i--) 
        {
            GD_SDCard_cmd(58, DEFAULT_LBA, DEFAULT_CRC);
            GD_SDCard_R3(&OCR);
            DEBUG_VAR(OCR);
        } /* end for */
        self->ccs = 1UL & (OCR >> 30);
    } 
    else 
    {
      self->ccs = 0;
    }
    DEBUG_VAR(self->ccs);

#if 0
    // Test point: dump sector 0 to serial.
    // should see first 512 bytes of card, ending 55 AA.
    {
        int     i;
        byte    b;

        DEBUG_INFO("Dumping first sector.\n");
        GD_SDCard_cmd17(self, 0);
        for (i = 0; i < 512; i++) 
        {
            delayms(10);
            b = spi_transfer(0xFF);
            printf("%x ", b);
            if ((i & 15) == 15)
                printf("\n");
        } /* end for */
        GD_SDCard_desel();
        DEBUG_INFO("END SECTOR DUMP.\n");
        for (;;);   // Endless loop since this code messes up the state and code hereafter 
                    // will not function as expected anymore.
    }
#endif

    type_code = GD_SDCard_read(self, 0x1BE + 0x4);
    switch (type_code) 
    {
        default:
            self->type = FAT16;
            DEBUG_INFO("FAT16\n");
            break;
        case 0x0b:
        case 0x0c:
            self->type = FAT32;
            DEBUG_INFO("FAT32\n");
            break;
    } /* end switch */
    DEBUG_VAR(type_code);

    self->o_partition         = 512L * GD_SDCard_read4(self, 0x1BE + 0x8);
    self->sectors_per_cluster = GD_SDCard_read(self, self->o_partition + 0xD);
    self->reserved_sectors    = GD_SDCard_read2(self, self->o_partition + 0xE);
    self->cluster_size        = 512L * self->sectors_per_cluster;
    self->bytes_per_sector    = GD_SDCard_read2(self, self->o_partition + 0xB);
    DEBUG_VAR(self->sectors_per_cluster);

    DEBUG_INFO("Bytes per sector:    "); DEBUG_VAR(self->bytes_per_sector);
    DEBUG_INFO("Sectors per cluster: "); DEBUG_VAR(self->sectors_per_cluster);

    if (self->type == FAT16) 
    {
        uint16_t    sectors_per_fat;

        self->max_root_dir_entries = GD_SDCard_read2(self, self->o_partition + 0x11);
        sectors_per_fat = GD_SDCard_read2(self, self->o_partition + 0x16);
        self->o_fat = self->o_partition + 512L * self->reserved_sectors;
        self->o_root = self->o_fat + (2 * 512L * sectors_per_fat);

        // data area starts with cluster 2, so offset it here
        self->o_data = self->o_root + (self->max_root_dir_entries * 32L) - (2L * self->cluster_size);
    } 
    else 
    {
        uint32_t    sectors_per_fat;
        uint32_t    fat_begin_lba;
        uint32_t    cluster_begin_lba;

        sectors_per_fat                   = GD_SDCard_read4(self, self->o_partition + 0x24);
        self->root_dir_first_cluster = GD_SDCard_read4(self, self->o_partition + 0x2c);
        fat_begin_lba                     = (self->o_partition >> 9) + self->reserved_sectors;
        cluster_begin_lba                 = (self->o_partition >> 9) + self->reserved_sectors + (2 * sectors_per_fat);

        // printf("fat_begin_lba     : %ld (%x)\n", fat_begin_lba, fat_begin_lba);
        // printf("cluster_begin_lba : %ld (%x)\n", cluster_begin_lba, cluster_begin_lba );

        self->o_fat  = 512L * fat_begin_lba;
        self->o_root = (512L * (cluster_begin_lba + (self->root_dir_first_cluster - 2) * self->sectors_per_cluster));
        self->o_data = (512L * (cluster_begin_lba - 2 * self->sectors_per_cluster));
    } /* end if */

    // printf("max_root_dir_entries   : %d\n", self->max_root_dir_entries);
    // printf("sectors_per_cluster    : %d\n", self->sectors_per_cluster);
    // printf("reserved_sectors       : %d\n", self->reserved_sectors);
    // printf("cluster_size           : %d\n", self->cluster_size);
    // printf("root_dir_first_cluster : %d\n", self->root_dir_first_cluster);
    // printf("o_fat                  : %d\n", self->o_fat);
    // printf("o_root                 : %d\n", self->o_root);
    // printf("o_data                 : %d\n", self->o_data);
    // printf("o_partition            : %d\n", self->o_partition);

finished:
    DEBUG_INFO("finished\n");
} /* end GD_SDCard_begin */


void GD_SDCard_cmd(byte cmd, 
                   uint32_t lba, 
                   uint8_t crc) 
{
    GD_SDCard_sel();
    spi_transfer(0x40 | cmd);
    spi_transfer(0xFF & (lba >> 24));
    spi_transfer(0xFF & (lba >> 16));
    spi_transfer(0xFF & (lba >> 8));
    spi_transfer(0xFF & (lba));
    spi_transfer(crc);
} /* end GD_SDCard_cmd */


void GD_SDCard_cmd17(GD_SDCard *self,
                     uint32_t off)
{
    DEBUG_VAR(off);
    if (self->ccs)
        GD_SDCard_cmd(17, off >> 9, DEFAULT_CRC);
    else
        GD_SDCard_cmd(17, off & ~511L, DEFAULT_CRC);
    GD_SDCard_R1();
    GD_SDCard_sel();
    while (spi_transfer(0xFF) != 0xFE)
        ;
} /* end GD_SDCard_cmd17 */


void GD_SDCard_delay(byte n) 
{
    while (n--)
        spi_transfer(0xFF);
} /* end GD_SDCard_delay */


void GD_SDCard_desel() 
{
    unselect_Dazzler_SD_CS();
    spi_transfer(0xFF);
} /* end GD_SDCard_desel */


byte GD_SDCard_sdR3(uint32_t *ocr) 
{  
    byte    i;
    byte    r;
    
    // read response R3
    r = GD_SDCard_response();

    for (i = 4; i; i--)
        *ocr = (*ocr << 8) | spi_transfer(0xFF);
    spi_transfer(0xFF);   // trailing byte

    GD_SDCard_desel();

    return r;
} /* end GD_SDCard_sdR3 */


byte GD_SDCard_sdR7() 
{  
    byte    i;
    byte    r;

    // read response R7
    r = GD_SDCard_response();
    for (i = 4; i; i--)
        spi_transfer(0xFF);

    GD_SDCard_desel();

    return r;
} /* end GD_SDCard_sdR7 */


byte GD_SDCard_R1() 
{   
    // read response R1
    byte    r;
    
    r = GD_SDCard_response();
    GD_SDCard_desel();
    spi_transfer(0xFF);             // trailing byte

    return r;
} /* end GD_SDCard_R1 */


byte GD_SDCard_R3(uint32_t *ocr) 
{  
    // read response R3
    byte    r;
    byte    i;

    r = GD_SDCard_response();
    for (i = 4; i; i--)
        *ocr = (*ocr << 8) | spi_transfer(0xFF);
    spi_transfer(0xFF);             // trailing byte
    GD_SDCard_desel();

    return r;
} /* end GD_SDCard_R3 */


byte GD_SDCard_R7() 
{  
    // read response R7
    byte    i;
    byte    r;
    
    r = GD_SDCard_response();
    for (i = 4; i; i--)
        spi_transfer(0xFF);
    GD_SDCard_desel();

    return r;
} /* end GD_SDCard_R7 */


byte GD_SDCard_read(GD_SDCard *self,
                    uint32_t off) 
{
    byte    r;

    GD_SDCard_readN(self, (byte*)&r, off, sizeof(r));
    return r;
} /* end GD_SDCard_read */


uint16_t GD_SDCard_read2(GD_SDCard *self,
                         uint32_t off) 
{
    uint16_t    r;

    GD_SDCard_readN(self, (byte*)&r, off, sizeof(r));
    return r;
} /* end GD_SDCard_read2 */


uint32_t GD_SDCard_read4(GD_SDCard *self,
                         uint32_t off) 
{
    uint32_t    r;

    GD_SDCard_readN(self, (byte*)&r, off, sizeof(r));
    return r;
} /* end GD_SDCard_read4 */


void GD_SDCard_readN(GD_SDCard *self,
                     byte *d, 
                     uint32_t off, 
                     uint16_t n) 
{
    uint16_t i;
    uint16_t bo = (off & 511);

    GD_SDCard_cmd17(self, off);
    for (i = 0; i < bo; i++)
        spi_transfer(0xFF);
    for (i = 0; i < n; i++)
        *d++ = spi_transfer(0xFF);
    for (i = 0; i < (514 - bo - n); i++)
        spi_transfer(0xFF);
    GD_SDCard_desel();
} /* end GD_SDCard_readN */


byte GD_SDCard_response() 
{
    byte    r;

    r = spi_transfer(0xFF);
    while (r & 0x80)
        r = spi_transfer(0xFF);

    return r;
} /* end GD_SDCard_response */


void GD_SDCard_sel() 
{
    select_Dazzler_SD_CS();
    delayms(1);
} /* end GD_SDCard_sel */

/* ************************************************************************* */
