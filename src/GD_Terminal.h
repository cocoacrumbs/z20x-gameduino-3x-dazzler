#ifndef GD_TERMINAL_H
#define GD_TERMINAL_H

#include "GD_Defines.h"
#include "GD.h"

/* ************************************************************************* */

#define COLOR_BLACK                         0x000000  // #000000
#define COLOR_VALHALLA                      0x222034  // #222034
#define COLOR_LOULOU                        0x45283c  // #45283c
#define COLOR_OILED_CEDAR                   0x663931  // #663931
#define COLOR_ROPE                          0x8f563b  // #8f563b
#define COLOR_TAHITI_GOLD                   0xdf7126  // #df7126
#define COLOR_TWINE                         0xd9a066  // #d9a066
#define COLOR_PANCHO                        0xeec39a  // #eec39a

#define COLOR_GOLDEN_FIZZ                   0xfbf236  // #fbf236
#define COLOR_ATLANTIS                      0x99e550  // #99e550
#define COLOR_RAINFOREST                    0x6abe30  // #6abe30
#define COLOR_ELF_GREEN                     0x37946e  // #37946e
#define COLOR_DELL                          0x4b692f  // #4b692f
#define COLOR_VERDIGRIS                     0x524b24  // #524b24
#define COLOR_OPAL                          0x323c39  // #323c39
#define COLOR_DEEP_KOAMARU                  0x3f3f74  // #3f3f74

#define COLOR_VENICE_BLUE                   0x306082  // #306082
#define COLOR_ROYAL_BLUE                    0x5b6ee1  // #5b6ee1
#define COLOR_CORNFLOWER                    0x639bff  // #639bff
#define COLOR_VIKING                        0x5fcde4  // #5fcde4
#define COLOR_LIGHT_STEEL_BLUE              0xcbdbfc  // #cbdbfc
#define COLOR_WHITE                         0xffffff  // #ffffff
#define COLOR_HEATHER                       0x9badb7  // #9badb7
#define COLOR_TOPAZ                         0x847e87  // #847e87

#define COLOR_DIM_GRAY                      0x696a6a  // #696a6a
#define COLOR_SMOKEY_ASH                    0x595652  // #595652
#define COLOR_CLAIRVOYANT                   0x76428a  // #76428a
#define COLOR_RED                           0xac3232  // #ac3232
#define COLOR_MANDY                         0xd95763  // #d95763
#define COLOR_PLUM                          0xd77bba  // #d77bba
#define COLOR_STINGER                       0x8f974a  // #8f974a
#define COLOR_BROWN                         0x8a6f30  // #8a6f30

#define TERMINAL_KEY_BELL                     7
#define TERMINAL_KEY_BACKSPACE                8 
#define TERMINAL_KEY_CR                      13
#define TERMINAL_KEY_LF                      10
#define TERMINAL_KEY_SPACE                   32
#define TERMINAL_KEY_ESC                     27
#define TERMINAL_KEY_DEL                    127

#define SCROLLBAR_WIDTH                      20
#define SCROLLBAR_HALF_WIDTH                 10

#define TAG_SCROLLBAR                       201

#define LINE_FULL                             0
#define CHAR_READ                             1

#define TERMINAL_VGA_BLACK                    0
#define TERMINAL_VGA_BLUE                     1
#define TERMINAL_VGA_GREEN                    2
#define TERMINAL_VGA_CYAN                     3
#define TERMINAL_VGA_RED                      4
#define TERMINAL_VGA_MAGENTA                  5
#define TERMINAL_VGA_YELLOW                   6
#define TERMINAL_VGA_WHITE                    7
#define TERMINAL_VGA_BRIGHT_BLACK             8
#define TERMINAL_VGA_BRIGHT_BLUE              9
#define TERMINAL_VGA_BRIGHT_GREEN            10
#define TERMINAL_VGA_BRIGHT_CYAN             11
#define TERMINAL_VGA_BRIGHT_RED              12
#define TERMINAL_VGA_BRIGHT_MAGENTA          13
#define TERMINAL_VGA_BRIGHT_YELLOW           14
#define TERMINAL_VGA_BRIGHT_WHITE            15

#define TERMINAL_BITMAP_HANDLE_TEXT          14
#define TERMINAL_BITMAP_HANDLE_BACKGROUND    13

/* ************************************************************************* */

typedef struct GD_Terminal_T
{
    GD          *my_GD;

    uint16_t    cursor_index;
    uint16_t    line_count;
    uint16_t    last_line_address;
    uint16_t    lines_per_screen;
    uint8_t     line_pixel_height;
    uint8_t     characters_per_line;
    uint8_t     bytes_per_line;
    uint8_t     current_font;
    bool        background_colors_enabled;
    uint8_t     foreground_color;
    uint8_t     background_color;
    uint16_t    scrollback_length;
    float       lines_per_screen_percent;
    uint16_t    scrollbar_size;
    uint16_t    scrollbar_size_half;
    uint16_t    scrollbar_position;
    float       scrollbar_position_percent;
    uint16_t    scroll_offset;
    int         draw_x_coord;
    int         draw_y_coord;
    uint16_t    draw_width;
    uint16_t    draw_height;
    uint32_t    terminal_window_bg_color;
    uint8_t     terminal_window_opacity;
    uint16_t    bell;
} GD_Terminal;

/* ************************************************************************* */

uint8_t GD_Terminal_append_character(GD_Terminal *self,
                                     char newchar);

void GD_Terminal_append_string(GD_Terminal *self,
                               const char* str);

void GD_Terminal_begin(GD_Terminal *self,
                       uint8_t initial_font_mode);

uint32_t GD_Terminal_bitmap_byte_size(GD_Terminal *self); 

void GD_Terminal_change_size(GD_Terminal *self, 
                             uint16_t rows, 
                             uint16_t columns);

void GD_Terminal_disable_vga_background_colors(GD_Terminal *self);

void GD_Terminal_draw(GD_Terminal *self);

void GD_Terminal_draw_XY(GD_Terminal *self,
                         int startx, 
                         int starty);

void GD_Terminal_enable_vga_background_colors(GD_Terminal *self);

void GD_Terminal_erase_line_buffer(GD_Terminal *self);

void GD_Terminal_init(GD_Terminal *self,
                      GD *my_GD);

void GD_Terminal_new_line(GD_Terminal *self);

void GD_Terminal_put_char(GD_Terminal *self,
                          char newchar);

uint32_t GD_Terminal_ram_end_address(GD_Terminal *self);

void GD_Terminal_reset(GD_Terminal *self);

void GD_Terminal_ring_bell(GD_Terminal *self);

void GD_Terminal_set_font_8x8(GD_Terminal *self);

void GD_Terminal_set_font_vga(GD_Terminal *self);

void GD_Terminal_set_position(GD_Terminal *self,
                              int x, 
                              int y);

void GD_Terminal_set_scrollbar_handle_size(GD_Terminal *self);

void GD_Terminal_set_size_fullscreen(GD_Terminal *self);

void GD_Terminal_set_window_bg_color(GD_Terminal *self,
                                     uint32_t color);

void GD_Terminal_set_window_opacity(GD_Terminal *self,
                                    uint8_t opacity);

void GD_Terminal_update_scrollbar(GD_Terminal *self);

void GD_Terminal_update_scrollbar_position(GD_Terminal *self,
                                           uint16_t new_position);

void GD_Terminal_upload_to_graphics_ram(GD_Terminal *self);

/* ************************************************************************* */

#endif GD_TERMINAL_H
