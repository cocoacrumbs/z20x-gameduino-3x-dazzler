#include <ez80.h>

#include "spi.h"
#include "timer.h"


/* Delays, SPI and SD card retry constants */
#define WAIT4RESET  8000
#define SPIRETRY    1000


#define TP_CS       1   // PB1
#define SD_CS       4   // PB4
#define GD3X_CS     5   // PB5


#define SPI_MOSI    7   // PB7
#define SPI_MISO    6   // PB6
#define SPI_CLK     3   // PB3


/* Clear, set bits in registers */
#define BIT(n)              (1 << n)
#define CLEAR_BIT(reg, n)   (reg &= ~(1 << n))
#define SET_BIT(reg, n)     (reg |=  (1 << n))


void select_Dazzler_3X_CS()
{
    PB_DR &= ~BIT(TP_CS);
} /* end select_Dazzler_3X_CS */

void unselect_Dazzler_3X_CS()
{
    PB_DR |= BIT(TP_CS); 
} /* end unselect_Dazzler_3X_CS */

void deselect_Dazzler_3X_CS()
{
    PB_DR |= BIT(TP_CS); 
    spi_transfer(0xFF);
} /* end unselect_Dazzler_3X_CS */


void select_Dazzler_SD_CS()
{
    PB_DR &= ~BIT(SD_CS);
} /* end select_Dazzler_SD_CS */

void unselect_Dazzler_SD_CS()
{
    PB_DR |= BIT(SD_CS); 
} /* end unselect_Dazzler_SD_CS */

void deselect_Dazzler_SD_CS()
{
    PB_DR |= BIT(SD_CS); 
    spi_transfer(0xFF);
} /* end unselect_Dazzler_SD_CS */


void select_Dazzler_3X_Terminal_CS()
{
    PB_DR &= ~BIT(GD3X_CS);
} /* end select_Dazzler_3X_Terminal_CS */

void unselect_Dazzler_3X_Terminal_CS()
{
    PB_DR |= BIT(GD3X_CS); 
} /* end unselect_Dazzler_3X_Terminal_CS */

void deselect_Dazzler_3X_Terminal_CS()
{
    PB_DR |= BIT(GD3X_CS); 
    spi_transfer(0xFF);
} /* end deselect_Dazzler_3X_Terminal_CS */


unsigned char spi_transfer(char d)
{
    int i = 0;

    /* Write the data to exchange */
    SPI_TSR = d;

    do
    {
        if (SPI_SR & (1 << 7)) 
        {
            break;
        } /* end if */
        i++;
    } while (i < SPIRETRY);

    return SPI_RBR;
} /* end spi_transfer */


/* Reset system devices */
void init_hw()
{
    long i;

    /* SS must remain high for spi to work properly */
    PB_DR   |=  BIT(2);
    PB_ALT1 &= ~BIT(2);
    PB_ALT2 &= ~BIT(2);
    PB_DDR  &= ~BIT(2);

    // Enable the chip select outputs and de-select
    PB_DR   |=  (BIT(SD_CS) | BIT(TP_CS) | BIT(GD3X_CS));
    PB_ALT1 &= ~(BIT(SD_CS) | BIT(TP_CS) | BIT(GD3X_CS));
    PB_ALT2 &= ~(BIT(SD_CS) | BIT(TP_CS) | BIT(GD3X_CS));
    PB_DDR  &= ~(BIT(SD_CS) | BIT(TP_CS) | BIT(GD3X_CS));

    /* Set port B pins 7 (MOSI), 6 (MISO), 3 (SCK), 2 (/SS) to SPI */
    PB_ALT1 &= ~(BIT(SPI_MOSI) | BIT(SPI_MISO) | BIT(SPI_CLK));
    PB_ALT2 |=  (BIT(SPI_MOSI) | BIT(SPI_MISO) | BIT(SPI_CLK));

    i = 0;
    while (i < WAIT4RESET)
        i++;

    // /* Reset Dazzler GD3X terminal. */
    // select_Dazzler_3X_Terminal_CS();
    // spi_transfer(0x40);                 // Reset Dazzler 3X
    // spi_transfer(0x00);                 // 0 parameter 
    // unselect_Dazzler_3X_Terminal_CS();
    // delayms(250);        
} /* end init_hw */


void mode_spi(int d)
{
    // Disable SPI
    SPI_CTL = 0;

    // SPI Data Rate (bps) = System Clock Frequency / (2 X SPI Baud Rate Generator Divisor)
    SPI_BRG_H = d / 256;
    SPI_BRG_L = d % 256;

    // Enable SPI
    SPI_CTL = 0x30;         // [5] SPI is enabled.
                            // [4] When enabled, the SPI operates as a master.
} /* end mode_spi */


int spi_begin()
{
    // BYTE response[4];
    int i;

    /* Set SPI in master mode, "mode 0" transfers, 200 kHz */
    mode_spi(50);

    /* Write 80 clock pulses with /CS and DI (MOSI) high */
    unselect_Dazzler_3X_CS();
    unselect_Dazzler_SD_CS();
    unselect_Dazzler_3X_Terminal_CS();
    
    i = 0;
    while (i < 10) {
        spi_transfer(0xFF);
        ++i;
    }

    /* Set SPI data rate to 8.33 MHz */
    // mode_spi(3); A value of 4 seems to work but not with 3. Keeping it at 50 for now.
    mode_spi(4);

    return 0;
} /* end spi_begin */
