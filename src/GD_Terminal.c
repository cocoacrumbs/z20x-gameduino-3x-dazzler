#include <math.h>
#include <String.h>

#include "GD_Terminal.h"

/* ************************************************************************* */

static char terminal_linebuffer[] = "                                                                                                                                                                 ";
uint8_t *const linebuffer_const = (uint8_t*)terminal_linebuffer;

/* ************************************************************************* */

uint8_t GD_Terminal_append_character(GD_Terminal *self,
                                     char newchar) 
{
    if (   (self->cursor_index >= self->characters_per_line)
        || (newchar == TERMINAL_KEY_CR)
        || (newchar == TERMINAL_KEY_LF) ) 
    {
        GD_Terminal_new_line(self);
        return LINE_FULL;
    } /* end if */

    switch (newchar) 
    {
        case TERMINAL_KEY_BACKSPACE:
            // delete current char if not at the beginning of the line
            if (self->cursor_index > 0) 
            {
                GD_Terminal_put_char(self, ' ');
                self->cursor_index--;
            } /* end if */
            break;
        default:
            GD_Terminal_put_char(self, newchar);
            self->cursor_index++;
            break;
    } /* end switch */
    return CHAR_READ;
} /* end GD_Terminal_append_character */


void GD_Terminal_append_string(GD_Terminal *self,
                               const char* str) 
{
    uint16_t    i;

    for (i=0; i<strlen(str); i++) 
    {
        GD_Terminal_append_character(self, str[i]);
    } /* end for */
} /* end GD_Terminal_append_string */


void GD_Terminal_begin(GD_Terminal *self,
                       uint8_t initial_font_mode) 
{
    if (initial_font_mode == TEXTVGA)
    {
        GD_Terminal_set_font_vga(self);
    }
    else 
    {
        GD_Terminal_set_font_8x8(self);
    } /* end if */
    GD_Terminal_set_size_fullscreen(self);
} /* end GD_Terminal_begin */


uint32_t GD_Terminal_bitmap_byte_size(GD_Terminal *self) 
{
    return self->scrollback_length * self->bytes_per_line;
} /* end GD_Terminal_bitmap_byte_size */


void GD_Terminal_change_size(GD_Terminal *self, 
                             uint16_t rows, 
                             uint16_t columns)
{
    self->lines_per_screen    = rows;
    self->characters_per_line = columns;
    self->bytes_per_line      = self->characters_per_line;
    
    if (self->current_font == TEXTVGA)
        self->bytes_per_line *= 2;

    self->draw_width  = self->characters_per_line * 8;
    self->draw_height = self->lines_per_screen * self->line_pixel_height;

    // Create a 1x1 white pixel bitmap handle for drawing shaded rectangles
    GD_cmd_memset(self->my_GD, GD_Terminal_bitmap_byte_size(self), 0xFF, 2);    // Two bytes for 1 RGB565 pixel
    GD_BitmapHandle(self->my_GD, TERMINAL_BITMAP_HANDLE_BACKGROUND);
    GD_BitmapSource(self->my_GD, GD_Terminal_bitmap_byte_size(self));
    GD_BitmapLayout(self->my_GD, RGB565, 2, 1);
    GD_BitmapSize(self->my_GD, NEAREST, REPEAT, REPEAT,
                  self->characters_per_line * 8,
                  self->lines_per_screen * self->line_pixel_height);
    GD_Terminal_reset(self);
} /* end GD_Terminal_change_size */


void GD_Terminal_disable_vga_background_colors(GD_Terminal *self) 
{
    self->background_colors_enabled = false;
} /* end GD_Terminal_disable_vga_background_colors */


void GD_Terminal_draw(GD_Terminal *self) 
{
    GD_Terminal_draw_XY(self, self->draw_x_coord, self->draw_y_coord);
} /* end GD_Terminal_draw */


void GD_Terminal_draw_XY(GD_Terminal *self,
                         int startx, 
                         int starty) 
{
    uint16_t    current_line_address;
    int16_t     ycoord;
    uint16_t    min_lines;
    uint16_t    max_xoffset;
    int         i;

    // Upload any lingering data from append_character calls.
    GD_Terminal_upload_to_graphics_ram(self);

    if ((startx != self->draw_x_coord) || (starty != self->draw_y_coord)) 
    {
        self->draw_x_coord = startx;
        self->draw_y_coord = starty;
    } /* end */

    GD_SaveContext(self->my_GD);

    // Use bitmap handle 14 for text bitmaps
    GD_BitmapHandle(self->my_GD, TERMINAL_BITMAP_HANDLE_TEXT);
    GD_BitmapSize(self->my_GD, NEAREST, BORDER, BORDER, 480, self->line_pixel_height);
    GD_BitmapLayout(self->my_GD, self->current_font, self->bytes_per_line, 1);

    current_line_address = self->last_line_address;
    if (self->scroll_offset > 0)
        current_line_address = (current_line_address + (self->scrollback_length - self->scroll_offset)) % self->scrollback_length;

    min_lines = self->line_count;
    if (self->line_count > self->lines_per_screen)
        min_lines = self->lines_per_screen;

    max_xoffset = 0;
    if (self->bell > 10) 
    {
        max_xoffset = self->bell / 10;
        self->bell--;
    } /* end if */

    GD_Begin(self->my_GD, BITMAPS);

    // Draw Background

    if (self->current_font == TEXTVGA && self->background_colors_enabled)
        // Use VGA background colors if enabled
        GD_BlendFunc(self->my_GD, ONE, ZERO);
    else 
    {
        // Draw a shaded Terminal Background with a 1x1 stretched bitmap
        GD_BlendFunc(self->my_GD, SRC_ALPHA, ONE_MINUS_SRC_ALPHA);
        GD_ColorRGB(self->my_GD, self->terminal_window_bg_color);
        GD_ColorA(self->my_GD, self->terminal_window_opacity);
        GD_Vertex2ii(self->my_GD, self->draw_x_coord, self->draw_y_coord, TERMINAL_BITMAP_HANDLE_BACKGROUND, 0);
        GD_ColorA(self->my_GD, 255);
    } /* end if */

    GD_ColorRGB(self->my_GD, COLOR_WHITE);
    // Draw Terminal Text
    for (i=0; i<min_lines; i++) 
    {
        ycoord = (self->draw_y_coord + (self->lines_per_screen * self->line_pixel_height)) - self->line_pixel_height - (self->line_pixel_height * i);
        
        // Change the bitmap start address
        GD_BitmapSource(self->my_GD, current_line_address * self->bytes_per_line);
        GD_Vertex2ii(self->my_GD, 
                     max_xoffset ? GD_random_1(self->my_GD, max_xoffset) + self->draw_x_coord : self->draw_x_coord,
                     ycoord,
                     TERMINAL_BITMAP_HANDLE_TEXT,
                     0);
        current_line_address = (current_line_address + (self->scrollback_length - 1)) % self->scrollback_length;
    } /* end for */

    // Draw Scrollbar
    GD_BlendFunc(self->my_GD, SRC_ALPHA, ONE_MINUS_SRC_ALPHA);
    GD_ColorA(self->my_GD, self->terminal_window_opacity);
    GD_cmd_bgcolor(self->my_GD, COLOR_VALHALLA);
    GD_cmd_fgcolor(self->my_GD, COLOR_LIGHT_STEEL_BLUE);

    GD_Tag(self->my_GD, TAG_SCROLLBAR);
    GD_cmd_scrollbar(self->my_GD, 
                     self->draw_x_coord + self->draw_width - SCROLLBAR_WIDTH,
                     self->draw_y_coord + SCROLLBAR_HALF_WIDTH,
                     SCROLLBAR_WIDTH,
                     self->draw_height - SCROLLBAR_WIDTH,
                     OPT_FLAT,
                     self->scrollbar_position - self->scrollbar_size_half,
                     self->scrollbar_size,
                     65535);
    GD_cmd_track(self->my_GD, 
                 self->draw_x_coord + self->draw_width - SCROLLBAR_WIDTH,
                 self->draw_y_coord + SCROLLBAR_HALF_WIDTH,
                 SCROLLBAR_WIDTH,
                 self->draw_height - SCROLLBAR_WIDTH,
                 TAG_SCROLLBAR);

    GD_RestoreContext(self->my_GD);
} /* end GD_Terminal_draw_XY */


void GD_Terminal_enable_vga_background_colors(GD_Terminal *self) 
{
    self->background_colors_enabled = true;
} /* end GD_Terminal_enable_vga_background_colors */


void GD_Terminal_erase_line_buffer(GD_Terminal *self) 
{
    uint8_t i;

    // erase current line
    for (i=0; i<120; i++) 
    {
        terminal_linebuffer[i] = ' ';
    } /* end for */

    // Set TEXTVGA default colors
    if (self->current_font == TEXTVGA) 
    {
        for (i=1; i<120; i+=2) 
        {
            terminal_linebuffer[i] = (self->background_color << 4) | self->foreground_color;
        } /* end for */
    } /* end if */
} /* end GD_Terminal_erase_line_buffer */


void GD_Terminal_init(GD_Terminal *self,
                      GD *my_GD)
{
    self->my_GD                     = my_GD;
    
    self->scrollback_length         = 100;
    self->foreground_color          = 15;
    self->background_color          = 0;
    GD_Terminal_disable_vga_background_colors(self);
    self->draw_x_coord              = 0;
    self->draw_y_coord              = 0;
    self->terminal_window_bg_color  = 0;
    self->terminal_window_opacity   = 200;
    GD_Terminal_reset(self);
} /* end GD_Terminal_init */


void GD_Terminal_new_line(GD_Terminal *self) 
{
    // copy terminal_linebuffer to FT810 RAM
    GD_Terminal_upload_to_graphics_ram(self);

    self->cursor_index = 0;
    self->line_count++;

    if (self->line_count >= self->scrollback_length)
        self->line_count = self->scrollback_length;

    self->last_line_address++;

    if (self->last_line_address > self->scrollback_length)
        self->last_line_address = 0;

    GD_Terminal_erase_line_buffer(self);
    GD_Terminal_set_scrollbar_handle_size(self);
} /* end GD_Terminal_new_line */


void GD_Terminal_put_char(GD_Terminal *self,
                          char newchar) 
{
    if (self->current_font == TEXTVGA) 
    {
        terminal_linebuffer[self->cursor_index*2+1] = (self->background_color << 4) | self->foreground_color;
        terminal_linebuffer[self->cursor_index*2] = newchar;
    }
    else 
    {
        terminal_linebuffer[self->cursor_index] = newchar;
    } /* end if */
} /* end GD_Terminal_put_char */


uint32_t GD_Terminal_ram_end_address(GD_Terminal *self) 
{
    return GD_Terminal_bitmap_byte_size(self) + 2;
} /* end GD_Terminal_ram_end_address */


void GD_Terminal_reset(GD_Terminal *self) 
{
    self->bell = 0;
    self->line_count = 1;
    self->cursor_index = 0;
    self->last_line_address = 0;

    GD_Terminal_erase_line_buffer(self);
    GD_Terminal_set_scrollbar_handle_size(self);
} /* end GD_Terminal_reset */


void GD_Terminal_ring_bell(GD_Terminal *self) 
{
    // Bell animation for 60 frames.
    self->bell = 60;
} /* end GD_Terminal_ring_bell */


void GD_Terminal_set_font_8x8(GD_Terminal *self) 
{
    self->current_font      = TEXT8X8;
    self->line_pixel_height = 8;
} /* end GD_set_font_8x8 */


void GD_Terminal_set_font_vga(GD_Terminal *self) 
{
    self->current_font      = TEXTVGA;
    self->line_pixel_height = 16;
} /* end GD_Terminal_set_font_vga */


void GD_Terminal_set_position(GD_Terminal *self,
                              int x, 
                              int y) 
{
    self->draw_x_coord = x;
    self->draw_y_coord = y;
} /* end GD_Terminal_set_position */


void GD_Terminal_set_scrollbar_handle_size(GD_Terminal *self) 
{
    self->lines_per_screen_percent = ((float)self->lines_per_screen) / ((float)self->line_count);
    
    if (self->lines_per_screen_percent > 1.0)
        self->lines_per_screen_percent = 1.0;
    
    self->scrollbar_size      = (uint16_t)floor(self->lines_per_screen_percent * 65535.0);
    self->scrollbar_size_half = (uint16_t)floor(self->lines_per_screen_percent * 0.5 * 65535.0);
    GD_Terminal_update_scrollbar_position(self, 65535);
} /* end GD_Terminal_set_scrollbar_handle_size */


void GD_Terminal_set_size_fullscreen(GD_Terminal *self) 
{
    int character_width     = 8;
    int character_height    = 8;
    int row_count;
    int col_count;

    if (self->current_font == TEXTVGA) 
    {
        character_height = 16;
    } /* end if */

    row_count = floor(self->my_GD->h / character_height);
    col_count = floor(self->my_GD->w / character_width);
    GD_Terminal_change_size(self, row_count, col_count);
} /* end GD_Terminal_set_size_fullscreen */


void GD_Terminal_set_window_bg_color(GD_Terminal *self,
                                     uint32_t color) 
{
    self->terminal_window_bg_color = color;
} /* end GD_Terminal_set_window_bg_color */


void GD_Terminal_set_window_opacity(GD_Terminal *self,
                                    uint8_t opacity) 
{
    self->terminal_window_opacity = opacity;
} /* end GD_Terminal_set_window_opacity */


void GD_Terminal_update_scrollbar(GD_Terminal *self) 
{
    switch (self->my_GD->inputs.track_tag & 0xFF) 
    {
        case TAG_SCROLLBAR:
            GD_Terminal_update_scrollbar_position(self, self->my_GD->inputs.track_val);
            break;
        default:
            break;
    } /* end switch */
} /* end GD_Terminal_update_scrollbar */


void GD_Terminal_update_scrollbar_position(GD_Terminal *self,
                                           uint16_t new_position) 
{
    self->scrollbar_position = new_position;

    if (self->scrollbar_position < self->scrollbar_size_half)
        self->scrollbar_position = self->scrollbar_size_half;

    if (self->scrollbar_position > 65535 - self->scrollbar_size_half)
        self->scrollbar_position = 65535 - self->scrollbar_size_half;

    self->scrollbar_position_percent = ((float)self->scrollbar_position - (float)self->scrollbar_size_half) / (65535.0 - (float)self->scrollbar_size);
    self->scrollbar_position_percent *= 100;
    self->scrollbar_position_percent = floor(self->scrollbar_position_percent);
    self->scrollbar_position_percent /= 100;
    self->scrollbar_position_percent = 1.0 - self->scrollbar_position_percent;

    if (self->line_count <= self->lines_per_screen) 
    {
        self->scroll_offset = 0;
    }
    else 
    {
        self->scroll_offset = (uint16_t)(self->scrollbar_position_percent * ((float)self->line_count - self->lines_per_screen));
    } /* end if */
} /* end GD_Terminal_update_scrollbar_position */


void GD_Terminal_upload_to_graphics_ram(GD_Terminal *self) 
{
    GD_cmd_memwrite(self->my_GD, self->last_line_address * self->bytes_per_line, self->bytes_per_line);
    GD_copy(self->my_GD, linebuffer_const, self->bytes_per_line);
} /* end GD_Terminalupload_to_graphics_ram */

/* ************************************************************************* */
