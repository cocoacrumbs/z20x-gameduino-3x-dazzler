#ifndef GD_DEFINES_H
#define GD_DEFINES_H

#define     byte        unsigned char
#define     int8_t      signed char
#define     uint8_t     unsigned char
#define     int16_t     short
#define     uint16_t    unsigned short
#define     int32_t     long
#define     uint32_t    unsigned long
#define     bool        unsigned char

#define     false       0
#define     true        1

/* ************************************************************************* */

#ifdef VERBOSE
    #include <stdio.h>
    #define DEBUG_INFO(X)       printf((X))
    #define DEBUG_VAR(VAR)      printf(#VAR " = %ld (%x)\n", (VAR), (VAR))
    #define DEBUG_CHAR(CHAR)    printf(#CHAR " = %s\n", (CHAR))
#else
    #define DEBUG_INFO(X)
    #define DEBUG_VAR(VAR)
    #define DEBUG_CHAR(CHAR)
#endif

/* ************************************************************************* */

#define min(a, b)               (((a) < (b)) ? (a) : (b))
#define max(a, b)               (((a) > (b)) ? (a) : (b))
#define lowByte(w)              ((uint8_t) ((w) & 0xFF))
#define highByte(w)             ((uint8_t) ((w) >> 8))

#define IS_POWER_2(x)           (((x) & ((x) - 1)) == 0)

#define M_PI		            3.14159265358979323846
#define M_TWO_PI                6.28318530717958647692

#define RGB(r, g, b)            ((uint32_t)((((r) & 0xffL) << 16) | (((g) & 0xffL) << 8) | ((b) & 0xffL)))
#define F8(x)                   (int((x) * 256L))
#define F16(x)                  ((int32_t)((x) * 65536L))

#define pgm_read_byte_near(x)               (*(char*)(x))
#define pgm_read_byte(address_short)        pgm_read_byte_near(address_short)
#define pgm_read_word_near(address_short)   ((uint16_t)(address_short))
#define pgm_read_dword_near(address_short)  ((uint16_t)(address_short))
#define pgm_read_dword(address_short)       pgm_read_dword_near(address_short)

/* ************************************************************************* */

#define LOW_FREQ_BOUND  47040000UL
// #define LOW_FREQ_BOUND  32040000UL

// convert integer pixels to subpixels
#define PIXELS(x)  (int)((x) * 16)

// Convert degrees to Furmans
#define DEGREES(n) ((65536L * (n)) / 360)

#define NEVER                   0
#define LESS                    1
#define LEQUAL                  2
#define GREATER                 3
#define GEQUAL                  4
#define EQUAL                   5
#define NOTEQUAL                6
#define ALWAYS                  7

#define ARGB1555                0
#define L1                      1
#define L4                      2
#define L8                      3
#define RGB332                  4
#define ARGB2                   5
#define ARGB4                   6
#define RGB565                  7
#define PALETTED                8
#define TEXT8X8                 9
#define TEXTVGA                 10
#define BARGRAPH                11
#define PALETTED565             14
#define PALETTED4444            15
#define PALETTED8               16
#define L2                      17
#define GLFORMAT                31

#define NEAREST                 0
#define BILINEAR                1

#define BORDER                  0
#define REPEAT                  1

#define KEEP                    1
#define REPLACE                 2
#define INCR                    3
#define DECR                    4
#define INVERT                  5

#define DLSWAP_DONE             0
#define DLSWAP_LINE             1
#define DLSWAP_FRAME            2

#define INT_SWAP                1
#define INT_TOUCH               2
#define INT_TAG                 4
#define INT_SOUND               8
#define INT_PLAYBACK            16
#define INT_CMDEMPTY            32
#define INT_CMDFLAG             64
#define INT_CONVCOMPLETE        128

#define TOUCHMODE_OFF           0
#define TOUCHMODE_ONESHOT       1
#define TOUCHMODE_FRAME         2
#define TOUCHMODE_CONTINUOUS    3

#define ZERO                    0
#define ONE                     1
#define SRC_ALPHA               2
#define DST_ALPHA               3
#define ONE_MINUS_SRC_ALPHA     4
#define ONE_MINUS_DST_ALPHA     5

#define OPT_MONO                1
#define OPT_NODL                2
#define OPT_FLAT                256
#define OPT_CENTERX             512
#define OPT_CENTERY             1024
#define OPT_CENTER              (OPT_CENTERX | OPT_CENTERY)
#define OPT_NOBACK              4096
#define OPT_NOTICKS             8192
#define OPT_NOHM                16384
#define OPT_NOPOINTER           16384
#define OPT_NOSECS              32768
#define OPT_NOHANDS             49152
#define OPT_RIGHTX              2048
#define OPT_SIGNED              256
#define OPT_SOUND               32

#define OPT_NOTEAR              4
#define OPT_FULLSCREEN          8
#define OPT_MEDIAFIFO           16

#define LINEAR_SAMPLES          0
#define ULAW_SAMPLES            1
#define ADPCM_SAMPLES           2

// 'instrument' argument to GD.play()

#define SILENCE                 0x00

#define SQUAREWAVE              0x01
#define SINEWAVE                0x02
#define SAWTOOTH                0x03
#define TRIANGLE                0x04

#define BEEPING                 0x05
#define ALARM                   0x06
#define WARBLE                  0x07
#define CAROUSEL                0x08

#define PIPS(n)                 (0x0f + (n))

#define HARP                    0x40
#define XYLOPHONE               0x41
#define TUBA                    0x42
#define GLOCKENSPIEL            0x43
#define ORGAN                   0x44
#define TRUMPET                 0x45
#define PIANO                   0x46
#define CHIMES                  0x47
#define MUSICBOX                0x48
#define BELL                    0x49

#define CLICK                   0x50
#define SWITCH                  0x51
#define COWBELL                 0x52
#define NOTCH                   0x53
#define HIHAT                   0x54
#define KICKDRUM                0x55
#define POP                     0x56
#define CLACK                   0x57
#define CHACK                   0x58

#define MUTE                    0x60
#define UNMUTE                  0x61

#define RAM_PAL                 1056768UL

#define RAM_CMD                 (ft8xx_model ? 0x308000UL : 0x108000UL)
#define RAM_DL                  (ft8xx_model ? 0x300000UL : 0x100000UL)
#define REG_CLOCK               (ft8xx_model ? 0x302008UL : 0x102408UL)
#define REG_CMD_DL              (ft8xx_model ? 0x302100UL : 0x1024ecUL)
#define REG_CMD_READ            (ft8xx_model ? 0x3020f8UL : 0x1024e4UL)
#define REG_CMD_WRITE           (ft8xx_model ? 0x3020fcUL : 0x1024e8UL)
#define REG_CPURESET            (ft8xx_model ? 0x302020UL : 0x10241cUL)
#define REG_CSPREAD             (ft8xx_model ? 0x302068UL : 0x102464UL)
#define REG_DITHER              (ft8xx_model ? 0x302060UL : 0x10245cUL)
#define REG_DLSWAP              (ft8xx_model ? 0x302054UL : 0x102450UL)
#define REG_FRAMES              (ft8xx_model ? 0x302004UL : 0x102404UL)
#define REG_FREQUENCY           (ft8xx_model ? 0x30200cUL : 0x10240cUL)
#define REG_GPIO                (ft8xx_model ? 0x302094UL : 0x102490UL)
#define REG_GPIO_DIR            (ft8xx_model ? 0x302090UL : 0x10248cUL)
#define REG_HCYCLE              (ft8xx_model ? 0x30202cUL : 0x102428UL)
#define REG_HOFFSET             (ft8xx_model ? 0x302030UL : 0x10242cUL)
#define REG_HSIZE               (ft8xx_model ? 0x302034UL : 0x102430UL)
#define REG_HSYNC0              (ft8xx_model ? 0x302038UL : 0x102434UL)
#define REG_HSYNC1              (ft8xx_model ? 0x30203cUL : 0x102438UL)
#define REG_ID                  (ft8xx_model ? 0x302000UL : 0x102400UL)
#define REG_INT_EN              (ft8xx_model ? 0x3020acUL : 0x10249cUL)
#define REG_INT_FLAGS           (ft8xx_model ? 0x3020a8UL : 0x102498UL)
#define REG_INT_MASK            (ft8xx_model ? 0x3020b0UL : 0x1024a0UL)
#define REG_MACRO_0             (ft8xx_model ? 0x3020d8UL : 0x1024c8UL)
#define REG_MACRO_1             (ft8xx_model ? 0x3020dcUL : 0x1024ccUL)
#define REG_OUTBITS             (ft8xx_model ? 0x30205cUL : 0x102458UL)
#define REG_PCLK                (ft8xx_model ? 0x302070UL : 0x10246cUL)
#define REG_PCLK_POL            (ft8xx_model ? 0x30206cUL : 0x102468UL)
#define REG_PLAY                (ft8xx_model ? 0x30208cUL : 0x102488UL)
#define REG_PLAYBACK_FORMAT     (ft8xx_model ? 0x3020c4UL : 0x1024b4UL)
#define REG_PLAYBACK_FREQ       (ft8xx_model ? 0x3020c0UL : 0x1024b0UL)
#define REG_PLAYBACK_LENGTH     (ft8xx_model ? 0x3020b8UL : 0x1024a8UL)
#define REG_PLAYBACK_LOOP       (ft8xx_model ? 0x3020c8UL : 0x1024b8UL)
#define REG_PLAYBACK_PLAY       (ft8xx_model ? 0x3020ccUL : 0x1024bcUL)
#define REG_PLAYBACK_READPTR    (ft8xx_model ? 0x3020bcUL : 0x1024acUL)
#define REG_PLAYBACK_START      (ft8xx_model ? 0x3020b4UL : 0x1024a4UL)
#define REG_PWM_DUTY            (ft8xx_model ? 0x3020d4UL : 0x1024c4UL)
#define REG_PWM_HZ              (ft8xx_model ? 0x3020d0UL : 0x1024c0UL)
#define REG_ROTATE              (ft8xx_model ? 0x302058UL : 0x102454UL)
#define REG_SOUND               (ft8xx_model ? 0x302088UL : 0x102484UL)
#define REG_SWIZZLE             (ft8xx_model ? 0x302064UL : 0x102460UL)
#define REG_TAG                 (ft8xx_model ? 0x30207cUL : 0x102478UL)
#define REG_TAG_X               (ft8xx_model ? 0x302074UL : 0x102470UL)
#define REG_TAG_Y               (ft8xx_model ? 0x302078UL : 0x102474UL)
#define REG_TOUCH_ADC_MODE      (ft8xx_model ? 0x302108UL : 0x1024f4UL)
#define REG_TOUCH_CHARGE        (ft8xx_model ? 0x30210cUL : 0x1024f8UL)
#define REG_TOUCH_DIRECT_XY     (ft8xx_model ? 0x30218cUL : 0x102574UL)
#define REG_TOUCH_DIRECT_Z1Z2   (ft8xx_model ? 0x302190UL : 0x102578UL)
#define REG_TOUCH_MODE          (ft8xx_model ? 0x302104UL : 0x1024f0UL)
#define REG_TOUCH_OVERSAMPLE    (ft8xx_model ? 0x302114UL : 0x102500UL)
#define REG_TOUCH_RAW_XY        (ft8xx_model ? 0x30211cUL : 0x102508UL)
#define REG_TOUCH_RZ            (ft8xx_model ? 0x302120UL : 0x10250cUL)
#define REG_TOUCH_RZTHRESH      (ft8xx_model ? 0x302118UL : 0x102504UL)
#define REG_TOUCH_SCREEN_XY     (ft8xx_model ? 0x302124UL : 0x102510UL)
#define REG_TOUCH_SETTLE        (ft8xx_model ? 0x302110UL : 0x1024fcUL)
#define REG_TOUCH_TAG           (ft8xx_model ? 0x30212cUL : 0x102518UL)
#define REG_TOUCH_TAG_XY        (ft8xx_model ? 0x302128UL : 0x102514UL)
#define REG_TOUCH_TRANSFORM_A   (ft8xx_model ? 0x302150UL : 0x10251cUL)
#define REG_TOUCH_TRANSFORM_B   (ft8xx_model ? 0x302154UL : 0x102520UL)
#define REG_TOUCH_TRANSFORM_C   (ft8xx_model ? 0x302158UL : 0x102524UL)
#define REG_TOUCH_TRANSFORM_D   (ft8xx_model ? 0x30215cUL : 0x102528UL)
#define REG_TOUCH_TRANSFORM_E   (ft8xx_model ? 0x302160UL : 0x10252cUL)
#define REG_TOUCH_TRANSFORM_F   (ft8xx_model ? 0x302164UL : 0x102530UL)
#define REG_TRACKER             (ft8xx_model ? 0x309000UL : 0x109000UL)
#define REG_TRIM                (ft8xx_model ? 0x302180UL : 0x10256cUL)
#define REG_VCYCLE              (ft8xx_model ? 0x302040UL : 0x10243cUL)
#define REG_VOFFSET             (ft8xx_model ? 0x302044UL : 0x102440UL)
#define REG_VOL_PB              (ft8xx_model ? 0x302080UL : 0x10247cUL)
#define REG_VOL_SOUND           (ft8xx_model ? 0x302084UL : 0x102480UL)
#define REG_VSIZE               (ft8xx_model ? 0x302048UL : 0x102444UL)
#define REG_VSYNC0              (ft8xx_model ? 0x30204cUL : 0x102448UL)
#define REG_VSYNC1              (ft8xx_model ? 0x302050UL : 0x10244cUL)
#define FONT_ROOT               (ft8xx_model ? 0x2ffffcUL : 0x0ffffcUL)

// FT81x only registers 
#define REG_CMDB_SPACE                       0x302574UL
#define REG_CMDB_WRITE                       0x302578UL
#define REG_MEDIAFIFO_READ                   0x309014UL
#define REG_MEDIAFIFO_WRITE                  0x309018UL

// FT815 only registers
#define REG_FLASH_SIZE                       0x00309024 
#define REG_FLASH_STATUS                     0x003025f0 
#define REG_ADAPTIVE_FRAMERATE               0x0030257c

/* ************************************************************************* */

#define WII_A      (1 << 12)
#define WII_B      (1 << 14)
#define WII_SELECT (1 << 4)
#define WII_HOME   (1 << 3)
#define WII_START  (1 << 2)
#define WII_X      (1 << 11)
#define WII_Y      (1 << 13)
#define WII_UP     (1 << 8)
#define WII_DOWN   (1 << 6)
#define WII_LEFT   (1 << 9)
#define WII_RIGHT  (1 << 7)

/* ************************************************************************* */

#endif GD_DEFINES_H