#ifndef GD_H
#define GD_H

#include "GD_Transport.h"
#include "GD_Reader.h"
#include "GD_SDCard.h"
#include "xy.h"

/* ************************************************************************* */

                            // Options for GD.begin():
#define GD_CALIBRATE    1   // enable touchscreen calibration at startup
#define GD_TRIM         2   // trim the built-in oscillator
#define GD_STORAGE      4   // initialize attached microSD
#define GD_NOBACKLIGHT  8   // don't turn on the backlight

/* ************************************************************************* */

enum Primitive
{
    BITMAPS         = 1,
    POINTS          = 2,
    LINES           = 3,
    LINE_STRIP      = 4,
    EDGE_STRIP_R    = 5,
    EDGE_STRIP_L    = 6,
    EDGE_STRIP_A    = 7,
    EDGE_STRIP_B    = 8,
    RECTS           = 9,
};

/* ************************************************************************* */

typedef struct wii_T
{
    byte        active;
    xy          l;
    xy          r;
    uint16_t    buttons;
};

typedef struct inputs_T
{
    uint16_t        track_tag;
    uint16_t        track_val;
    uint16_t        rz;
    uint16_t        __dummy_1;
    int16_t         y;
    int16_t         x;
    int16_t         tag_y;
    int16_t         tag_x;
    uint8_t         tag;
    uint8_t         ptag;
    uint8_t         touching;
    xy              xytouch;
    struct wii_T    wii[2];
} inputs;

typedef struct GD_T
{
    GD_Transport    *my_GD_Transport;
    GD_Reader       *my_GD_Reader;
    GD_SDCard       *my_GD_SDCard;
    byte            vxf;        // Vertex Format
    uint16_t        w;
    uint16_t        h;
    uint32_t        loadptr;
    struct inputs_T inputs;

    // Private
    uint32_t        rseed;
    byte            cprim;      // current primitive
} GD;

/* ************************************************************************* */

void __GD_end(GD *self);

void GD_align(GD *self,
              byte n);

void GD_AlphaFunc(GD *self,
                  byte func, 
                  byte ref);

uint16_t GD_atan2(int16_t y, 
                  int16_t x);

void GD_Begin(GD *self,
              enum Primitive prim);

void GD_begin(GD *self,
              GD_Transport *GD_Transport,
              GD_SDCard *GD_SDCard,
              GD_Reader *GD_Reader,
              uint8_t options);

void GD_begin_default_options(GD *self,
                              GD_Transport *GD_Transport,
                              GD_SDCard *GD_SDCard,
                              GD_Reader *GD_Reader);
void GD_BlendFunc(GD *self,
                  byte src, 
                  byte dst);

void GD_BitmapExtFormat(GD *self,
                        uint16_t format);
                         
void GD_BitmapHandle(GD *self,
                     byte handle);

void GD_BitmapLayout(GD *self,
                     byte format, 
                     uint16_t linestride,
                     uint16_t height);

void GD_BitmapLayoutH(GD *self,
                      byte linestride, 
                      byte height);

void GD_BitmapSize(GD *self,
                   byte filter, 
                   byte wrapx, 
                   byte wrapy, 
                   uint16_t width, 
                   uint16_t height);

void GD_BitmapSource(GD *self,
                     uint32_t addr);

void GD_BitmapSwizzle(GD *self,
                      byte r, 
                      byte g, 
                      byte b, 
                      byte a);

void GD_BitmapTransformA(GD *self,
                         int32_t a);

void GD_BitmapTransformB(GD *self,
                         int32_t b);

void GD_BitmapTransformC(GD *self,
                         int32_t c);

void GD_BitmapTransformD(GD *self,
                         int32_t d);

void GD_BitmapTransformE(GD *self,
                         int32_t e);

void GD_BitmapTransformF(GD *self,
                         int32_t f);

void GD_bulkrd(GD *self,
               uint32_t a);

void GD_Call(GD *self,
             uint16_t dest);

void GD_Cell(GD *self,
             byte cell);

void GD_cFFFFFF(GD *self,
                byte v);

void GD_cH(GD *self,
           uint16_t v);

void GD_ch(GD *self,
           int16_t v);

void GD_cI(GD *self,
           uint32_t v);

void GD_ci(GD *self,
           int32_t v);

void GD_Clear(GD *self);

void GD_ClearColorA(GD *self,
                    byte alpha);

void GD_ClearColorRGB(GD *self,
                      uint32_t rgb);

void GD_ClearStencil(GD *self,
                     byte s);

void GD_ClearTag(GD *self,
                 byte s);

void GD_cmd32(GD *self,
              uint32_t b);

void GD_cmdbyte(GD *self,
                uint8_t b);

void GD_cmd_append(GD *self,
                   uint32_t ptr, 
                   uint32_t num);

void GD_cmd_bgcolor(GD *self,
                    uint32_t c);

void GD_cmd_button(GD *self,
                   int16_t x, 
                   int16_t y, 
                   uint16_t w, 
                   uint16_t h, 
                   byte font, 
                   uint16_t options, 
                   const char *s);

void GD_cmd_calibrate(GD *self);

void GD_cmd_clock(GD *self,
                  int16_t x, 
                  int16_t y, 
                  int16_t r, 
                  uint16_t options, 
                  uint16_t h, 
                  uint16_t m, 
                  uint16_t s, 
                  uint16_t ms);

void GD_cmd_coldstart(GD *self);

void GD_cmd_dial(GD *self,
                 int16_t x, 
                 int16_t y, 
                 int16_t r, 
                 uint16_t options, 
                 uint16_t val);
                 
void GD_cmd_dlstart(GD *self);

void GD_cmd_fgcolor(GD *self,
                    uint32_t c);

void GD_cmd_flashattach(GD *self);

void GD_cmd_flashdetach(GD *self);

void GD_cmd_flasherase(GD *self);

uint32_t GD_cmd_flashfast(GD *self,
                          uint32_t *r);

void GD_cmd_flashread(GD *self,
                      uint32_t dst, 
                      uint32_t src, 
                      uint32_t n);

void GD_cmd_flashspidesel(GD *self);

void GD_cmd_flashspirx(GD *self,
                       uint32_t ptr, 
                       uint32_t num);

void GD_cmd_flashspitx(GD *self,
                       uint32_t num);

void GD_cmd_flashupdate(GD *self,
                        uint32_t dst, 
                        uint32_t src, 
                        uint32_t n);

void GD_cmd_flashwrite(GD *self,
                       uint32_t dest, 
                       uint32_t num);

void GD_cmd_gauge(GD *self,
                  int16_t x, 
                  int16_t y, 
                  int16_t r, 
                  uint16_t options,
                  uint16_t major, 
                  uint16_t minor, 
                  uint16_t val, 
                  uint16_t range);

void GD_cmd_getmatrix(GD *self);

void GD_cmd_getprops(GD *self,
                     uint32_t *ptr, 
                     uint32_t *w, 
                     uint32_t *h);

void GD_cmd_getptr(GD *self);

void GD_cmd_gradcolor(GD *self,
                      uint32_t c);

void GD_cmd_gradient(GD *self,
                     int16_t x0, 
                     int16_t y0, 
                     uint32_t rgb0, 
                     int16_t x1, 
                     int16_t y1, 
                     uint32_t rgb1);

void GD_cmd_inflate(GD *self,
                    uint32_t ptr);

void GD_cmd_interrupt(GD *self,
                      uint32_t ms);

void GD_cmd_keys(GD *self,
                 int16_t x, 
                 int16_t y, 
                 int16_t w, 
                 int16_t h, 
                 byte font, 
                 uint16_t 
                 options, 
                 const char*s);

void GD_cmd_loadidentity(GD *self);

void GD_cmd_loadimage(GD *self,
                      uint32_t ptr, 
                      int32_t options);

void GD_cmd_mediafifo(GD *self,
                      uint32_t ptr, 
                      uint32_t size); 

void GD_cmd_memcpy(GD *self,
                   uint32_t dest, 
                   uint32_t src, 
                   uint32_t num);

void GD_cmd_memset(GD *self,
                   uint32_t ptr, 
                   byte value, 
                   uint32_t num);

void GD_cmd_memwrite(GD *self,
                     uint32_t ptr, 
                     uint32_t num);

void GD_cmd_number(GD *self,
                   int16_t x, 
                   int16_t y, 
                   byte font, 
                   uint16_t options, 
                   uint32_t n);

void GD_cmd_playvideo(GD *self,
                      int32_t options);

void GD_cmd_progress(GD *self,
                     int16_t x, 
                     int16_t y, 
                     int16_t w, 
                     int16_t h, 
                     uint16_t options, 
                     uint16_t val, 
                     uint16_t range);

void GD_cmd_regread(GD *self,
                    uint32_t ptr);

void GD_cmd_regwrite(GD *self,
                     uint32_t ptr, 
                     uint32_t val);

void GD_cmd_rotate(GD *self,
                   int32_t a);

void GD_cmd_romfont(GD *self,
                    uint32_t font, 
                    uint32_t romslot);

void GD_cmd_scale(GD *self,
                  int32_t sx, 
                  int32_t sy);

void GD_cmd_screensaver(GD *self);

void GD_cmd_scrollbar(GD *self,
                      int16_t x, int16_t y, 
                      int16_t w, int16_t h, 
                      uint16_t options, 
                      uint16_t val, 
                      uint16_t size, 
                      uint16_t range);

void GD_cmd_setbase(GD *self,
                    uint32_t b);

void GD_cmd_setbitmap(GD *self,
                      uint32_t source, 
                      uint16_t fmt, 
                      uint16_t w, 
                      uint16_t h);

void GD_cmd_setfont2(GD *self, 
                     uint32_t font, 
                     uint32_t ptr, 
                     uint32_t firstchar);

void GD_cmd_setrotate(GD *self, 
                      uint32_t r);

void GD_cmd_sketch(GD *self, 
                   int16_t x, 
                   int16_t y, 
                   uint16_t w, 
                   uint16_t h, 
                   uint32_t ptr, 
                   uint16_t format);

void GD_cmd_setmatrix(GD *self);

void GD_cmd_slider(GD *self,
                   int16_t x, 
                   int16_t y, 
                   uint16_t w, 
                   uint16_t h, 
                   uint16_t options, 
                   uint16_t val, 
                   uint16_t range);

void GD_cmd_snapshot(GD *self,
                     uint32_t ptr);

void GD_cmd_snapshot2(GD *self,
                      uint32_t fmt, 
                      uint32_t ptr, 
                      int16_t x, 
                      int16_t y, 
                      int16_t w, 
                      int16_t h);

void GD_cmd_spinner(GD *self,
                    int16_t x, 
                    int16_t y, 
                    byte style, 
                    byte scale);

void GD_cmd_stop(GD *self);

void GD_cmd_swap(GD *self);

void GD_cmd_sync(GD *self);

void GD_cmd_text(GD *self,
                 int16_t x, 
                 int16_t y, 
                 byte font, 
                 uint16_t options, 
                 const char *s);

void GD_textsize(GD *self,
                 int *w, 
                 int *h, 
                 int font,
                 const char *s);
                 
void GD_cmd_toggle(GD *self,
                   int16_t x, 
                   int16_t y, 
                   int16_t w, 
                   byte font, 
                   uint16_t options, 
                   uint16_t state, 
                   const char *s);

void GD_cmd_track(GD *self,
                  int16_t x, int16_t y, 
                  uint16_t w, uint16_t h, 
                  byte tag);
                  
void GD_cmd_translate(GD *self,
                      int32_t tx, 
                      int32_t ty);

void GD_cmd_videoframe(GD *self,
                       uint32_t dst, 
                       uint32_t ptr);

void GD_cmd_videostart(GD *self);

void GD_ColorA(GD *self,
               byte alpha);

void GD_ColorMask(GD *self,
                  byte r, 
                  byte g, 
                  byte b, 
                  byte a);

void GD_ColorR_G_B(GD *self, 
                   byte red, 
                   byte green, 
                   byte blue);

void GD_ColorRGB(GD *self, 
                 uint32_t rgb);

void GD_copy(GD *self,
             const uint8_t *src, 
             int count);

void GD_copyram(GD *self,
                byte *src, 
                int count);

void GD_cs(GD *self,
           const char *s);

void GD_Display(GD *self);

void GD_End(GD *self);

void GD_finish(GD *self);

void GD_flush(GD *self);

void GD_get_inputs(GD *self);

void GD_Jump(GD *self, 
             uint16_t dest);

void GD_LineWidth(GD *self, 
                  uint16_t width);

byte GD_load(GD *self,
             const char *filename);

uint32_t GD_measure_freq(GD *self);

void GD_Nop(GD *self);

void GD_play(GD *self,
             uint8_t instrument, 
             uint8_t note);

void GD_playNote(GD *self, 
                 uint8_t instrument, 
                 uint8_t note);

void GD_PointSize(GD *self,
                 uint16_t size);

void GD_polar(int *x, 
              int *y, 
              int16_t r, 
              uint16_t th);

uint16_t GD_random_0(GD *self);

uint16_t GD_random_1(GD *self,
                     uint16_t n);

uint16_t GD_random_2(uint16_t n0, 
                     uint16_t n1);

int16_t GD_rcos(int16_t r, 
                uint16_t th);

byte GD_rd(GD *self,
           uint32_t addr);


uint16_t GD_rd16(GD *self,
                 uint32_t addr);

uint32_t GD_rd32(GD *self,
                 uint32_t addr);

void GD_rd_n(GD *self,
             byte *dst, 
             uint32_t addr, 
             uint32_t n);

void GD_reset(GD *self);

void GD_RestoreContext(GD *self);

void GD_resume(GD *self);

void GD_Return(GD *self);

int16_t GD_rsin(int16_t r, 
                uint16_t th);

void GD_safeload(GD *self,
                 const char *filename);

void GD_SaveContext(GD *self);

void GD_ScissorSize(GD *self,
                    uint16_t width, 
                    uint16_t height);

void GD_ScissorXY(GD *self,
                  uint16_t x, 
                  uint16_t y);

void GD_seed(GD *self,
             uint16_t n);

void GD_self_calibrate(GD *self);

void GD_StencilFunc(GD *self,
                    byte func, 
                    byte ref, 
                    byte mask);

void GD_StencilMask(GD *self,
                    byte mask);

void GD_StencilOp(GD *self,
                  byte sfail, 
                  byte spass);

void GD_storage(GD *self);

void GD_swap(GD *self);


void GD_Tag(GD *self,
            byte s);

void GD_TagMask(GD *self,
                byte mask);

void GD_tune(GD *self);
     
void GD_Vertex2f(GD *self,
                 int16_t x, 
                 int16_t y);

void GD_Vertex2ii(GD *self,
                  uint16_t x, 
                  uint16_t y,
                  byte handle, /* 0 */
                  byte cell    /* 0 */);

void GD_VertexFormat(GD *self,
                     byte frac);

void GD_VertexTranslateX(GD *self,
                         uint32_t x); 

void GD_VertexTranslateY(GD *self,
                         uint32_t y);

void GD_wr(GD *self,
           uint32_t addr, 
           uint8_t v);
           
void GD_wr16(GD *self,
             uint32_t addr, 
             uint32_t v);

void GD_wr32(GD *self,
             uint32_t addr, 
             uint32_t v);
             
void GD_wr_n(GD *self,
             uint32_t addr, 
             byte *src, 
             uint32_t n);

#endif GD_H
