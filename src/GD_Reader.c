#include <CTYPE.h>
#include <String.h>

#include "GD_Reader.h"
#include "GD_SDCard.h"
#include "spi.h"


/* ************************************************************************* */

static void dos83(byte dst[11], 
                  const char *ps)
{
    byte    i = 0;

    while (*ps) 
    {
        if (*ps != '.')
            dst[i++] = toupper(*ps);
        else 
        {
            while (i < 8)
                dst[i++] = ' ';
        } /* end if */
        ps++;
    } /* end while */
    while (i < 11)
        dst[i++] = ' ';
} /* end dos83 */

/* ************************************************************************* */

void Reader_begin(GD_Reader *self,
                  dirent *de) 
{
    self->nseq     = 0;
    self->size     = de->size;
    self->cluster0 = de->cluster;
    if (self->GD_SDCard->type == FAT32)
        self->cluster0 |= ((long)de->cluster_hi << 16);
    Reader_rewind(self);
} /* end Reader_begin */


int Reader_eof(GD_Reader *self) 
{
    return self->size <= self->offset;
} /* end Reader_eof */


void Reader_fetch512(byte *dst) 
{
    int i;

    memset(dst, 0xFF, 512);
    for (i = 0; i < 512; i++) 
        *dst++ = spi_transfer(0xFF);

    spi_transfer(0xff);   // consume CRC
    spi_transfer(0xff);

    GD_SDCard_desel();
} /* end Reader_fetch512 */


void Reader_nextcluster(GD_Reader *self) 
{
    if (self->GD_SDCard->type == FAT16)
        self->cluster = GD_SDCard_read2(self->GD_SDCard, self->GD_SDCard->o_fat + 2 * self->cluster);
    else
        self->cluster = GD_SDCard_read4(self->GD_SDCard, self->GD_SDCard->o_fat + 4 * self->cluster);
} /* end Reader_nextcluster */


void Reader_nextcluster2(GD_Reader *self,
                         GD_SDCard *GD_SDCard,
                         byte *dst) 
{
    uint32_t    off;
    uint32_t    c;
    int         i;

    if (self->nseq) 
    {
        self->nseq--;
        self->cluster++;
        return;
    } /* end if */

    off = GD_SDCard->o_fat + 4 * self->cluster;
    GD_SDCard_cmd17(GD_SDCard, off & ~511L);
    Reader_fetch512(dst);
    i = off & 511;
    self->cluster = *(uint32_t*)&dst[i];
    self->nseq    = 0;
    for (c = self->cluster; (i < 512) && *(uint32_t*)&dst[i] == c; i += 4, c++)
        self->nseq++;
} /* end Reader_nextcluster2 */


int Reader_openfile(GD_Reader *self,
                    const char *filename)
{
    int     i = 0;
    byte    dosname[11];
    dirent  de;

    dos83(dosname, filename);
    do 
    {
        GD_SDCard_readN(self->GD_SDCard, (byte*)&de, self->GD_SDCard->o_root + i * 32, sizeof(de));
        if (0 == memcmp(de.name, dosname, 11)) 
        {
            Reader_begin(self, &de);
            return 1;
        } /* end if */
        i++;
    } while (de.name[0]);
    return 0;
} /* end Reader_openfile */


void Reader_readsector(GD_Reader *self,
                       GD_SDCard *GD_SDCard,
                       byte *dst) 
{
    uint32_t off;

    if (self->sector == GD_SDCard->sectors_per_cluster) 
    {
        self->sector = 0;
        Reader_nextcluster2(self, GD_SDCard, dst);
    } /* end if */
    DEBUG_VAR(self->cluster);
    
    off = GD_SDCard->o_data + ((long)GD_SDCard->cluster_size * self->cluster) + (512L * self->sector);
    DEBUG_VAR(off);
    GD_SDCard_cmd17(GD_SDCard, off & ~511L);
    DEBUG_VAR(off);
    self->sector++;
    self->offset += 512;
    Reader_fetch512(dst);
} /* end Reader_readsector */


void Reader_rewind(GD_Reader *self) 
{
    self->cluster = self->cluster0;
    self->sector  = 0;
    self->offset  = 0;
} /* end Reader_rewind */


void Reader_seek(GD_Reader *self,
                 uint32_t o) 
{
    uint32_t co = ~0;

    union 
    {
        uint8_t     buf[512];
        uint32_t    fat32[128];
        uint16_t    fat16[256];
    } data;

    if (o < self->offset)
        Reader_rewind(self);

    while (self->offset < o) 
    {
        if ((self->sector == self->GD_SDCard->sectors_per_cluster) && ((o - self->offset) > (long)self->GD_SDCard->cluster_size)) 
        {
            uint32_t o;

            if (self->GD_SDCard->type == FAT16)
                o = (self->GD_SDCard->o_fat + 2 * self->cluster) & ~511;
            else
                o = (self->GD_SDCard->o_fat + 4 * self->cluster) & ~511;

            if (o != co) 
            {
                GD_SDCard_readN(self->GD_SDCard, data.buf, o, 512);
                co = o;
            } /* end if */
            self->cluster = data.fat32[self->cluster & 127];
            self->offset += self->GD_SDCard->cluster_size;
        } 
        else
            Reader_skipsector(self);
    } /* end while */
} /* end Reader_seek */


void Reader_skipcluster(GD_Reader *self)
{
    Reader_nextcluster(self);
    self->offset += self->GD_SDCard->cluster_size;
} /* end Reader_skipcluster */


void Reader_skipsector(GD_Reader *self)
{
    if (self->sector == self->GD_SDCard->sectors_per_cluster) 
    {
        self->sector = 0;
        Reader_nextcluster(self);
    } /* end if */
    self->sector++;
    self->offset += 512;
} /* end Reader_skipsector */

/* ************************************************************************* */
