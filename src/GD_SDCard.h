#ifndef GD_SDCard_H
#define GD_SDCard_H

#include "GD_Defines.h"

/* ************************************************************************* */

#define FAT16 0
#define FAT32 1

/* ************************************************************************* */

typedef struct GD_SDCard_T
{
    byte        ccs;
    byte        type;
    uint16_t    sectors_per_cluster;
    uint16_t    reserved_sectors;
    uint16_t    max_root_dir_entries;
    // uint16_t    sectors_per_fat;
    uint16_t    cluster_size;
    uint32_t    root_dir_first_cluster;
    uint16_t    bytes_per_sector;

    // These are all linear addresses, hence the o_ prefix
    uint32_t    o_partition;
    uint32_t    o_fat;
    uint32_t    o_root;
    uint32_t    o_data;
} GD_SDCard;


typedef struct DIRENT_T
{
    char      name[8];
    char      ext[3];
    byte      attribute;
    byte      reserved[8];
    uint16_t  cluster_hi;           // FAT32 only
    uint16_t  time;
    uint16_t  date;
    uint16_t  cluster;
    uint32_t  size;
} dirent;

/* ************************************************************************* */

void GD_SDCard_appcmd(byte cc, 
                      uint32_t lba);

void GD_SDCard_begin(GD_SDCard *self);

void GD_SDCard_cmd(byte cmd, 
                   uint32_t lba, 
                   uint8_t crc);

void GD_SDCard_cmd17(GD_SDCard *self,
                     uint32_t off);

void GD_SDCard_delay(byte n);

void GD_SDCard_desel();

byte GD_SDCard_sdR3(uint32_t *ocr);

byte GD_SDCard_sdR7();

byte GD_SDCard_R1();

byte GD_SDCard_R3(uint32_t *ocr);

byte GD_SDCard_R7();

byte GD_SDCard_read(GD_SDCard *self,
                    uint32_t off);
uint16_t GD_SDCard_read2(GD_SDCard *self,
                         uint32_t off);
uint32_t GD_SDCard_read4(GD_SDCard *self,
                         uint32_t off);
void GD_SDCard_readN(GD_SDCard *self,
                     byte *d, 
                     uint32_t off, 
                     uint16_t n);

byte GD_SDCard_response();

void GD_SDCard_sel();

/* ************************************************************************* */

#endif GD_SDCard_H
