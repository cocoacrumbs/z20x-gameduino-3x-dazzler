#ifndef BITMAP_H
#define BITMAP_H

#include "GD_Defines.h"
#include "GD.h"
#include "xy.h"

/* ************************************************************************* */

typedef struct Bitmap_T
{
    GD         *my_GD;
    xy          size;
    xy          center;
    uint32_t    source;
    uint16_t    format;
    int8_t      handle;

    // Private
    uint32_t    rseed;
    byte        cprim;      // current primitive
} Bitmap;

/* ************************************************************************* */

void Bitmap_begin(Bitmap *self,
                  GD *my_GD);

void Bitmap_bind(Bitmap *self,
                 uint8_t h);

void Bitmap_fromfile(Bitmap *self,
                     const char* filename, 
                     int format);
void Bitmap_fromfile_default_format(Bitmap *self,
                                    const char* filename);

void Bitmap_setup(Bitmap *self);

void Bitmap_wallpaper(Bitmap *self);

/* ************************************************************************* */

#endif BITMAP_H


