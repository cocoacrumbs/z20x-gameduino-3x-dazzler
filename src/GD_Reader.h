#ifndef GD_READER_H
#define GD_READER_H

#include "GD_Defines.h"
#include "GD_SDCard.h"

/* ************************************************************************* */

typedef struct GD_Reader_T
{
    GD_SDCard *GD_SDCard;

    uint32_t  cluster;
    uint32_t  cluster0;
    uint32_t  offset;
    uint32_t  size;
    byte      sector;
    byte      nseq;
} GD_Reader;

/* ************************************************************************* */

void Reader_begin(GD_Reader *self,
                  dirent *de);

int Reader_eof(GD_Reader *self);

void Reader_fetch512(byte *dst);

void Reader_nextcluster(GD_Reader *self);

void Reader_nextcluster2(GD_Reader *self,
                         GD_SDCard *GD_SDCard,
                         byte *dst);

int Reader_openfile(GD_Reader *self,
                    const char *filename);

void Reader_readsector(GD_Reader *self,
                       GD_SDCard *GD_SDCard,
                       byte *dst);
                       
void Reader_rewind(GD_Reader *self);

void Reader_seek(GD_Reader *self,
                 uint32_t o);

void Reader_skipcluster(GD_Reader *self);

void Reader_skipsector(GD_Reader *self);

/* ************************************************************************* */

#endif GD_READER_H
