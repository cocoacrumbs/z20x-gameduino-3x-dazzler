#ifndef CONTROLLER_H
#define CONTROLLER_H

#include "GD_Defines.h"
#include "GD.h"

/* ************************************************************************* */

#define CONTROL_LEFT  1
#define CONTROL_RIGHT 2
#define CONTROL_UP    4
#define CONTROL_DOWN  8

/* ************************************************************************* */

typedef struct Controller_T
{   GD      *my_GD;

    byte    prev;
} Controller;

/* ************************************************************************* */

void Controller_begin(Controller *self,
                      GD *my_GD);

byte Controller_read(Controller *self);

/* ************************************************************************* */

#endif CONTROLLER_H
