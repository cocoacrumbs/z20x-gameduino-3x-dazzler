#include <stdlib.h>

#include "frogger.h"
#include "frogger_assets.h"
#include "Controller.h"

/* ************************************************************************* */

extern byte ft8xx_model;

/* ************************************************************************* */

static Controller Control;

// Game variables
static unsigned int     t               = 0;
static int              frogx, frogy;       // screen position
static int              leaping;            // 0 means not leaping, 1-8 animates the leap
static int              frogdir;            // while leaping, which direction is the leap?
static int              frogface;           // which way is the frog facing, in furmans for CMD_ROTATE
static int              dying;              // 0 means not dying, 1-64 animation counter
static long             score;
static long             hiscore;
static byte             lives;
static byte             done[5];
static byte             homes[5]        = { 24, 72, 120, 168, 216 };
static int              ti;

/* ************************************************************************* */

#define sf              3
#define PADX(x)         (480 + (x - 3) * 48)
#define PADY(y)         (272 + (y - 3) * 48)

/* ************************************************************************* */

static void frog_start()
{
    frogx    = 120;
    frogy    = 232;
    leaping  = 0;
    frogdir  = 0;
    frogface = 0x0000;
    dying    = 0;
    ti       = 120 << 7;
} /* end frog_start */


static void level_start()
{
    byte    i;

    for (i = 0; i < 5; i++)
        done[i] = 0;
} /* end level_start */


static void game_start()
{
    lives = 4;
    score = 0;
} /* end game_start */


static void game_setup(GD *my_GD)
{
    Controller_begin(&Control, my_GD);
    game_start();
    level_start();
    frog_start();
} /* end game_setup */


static void sprite(GD *my_GD,
                   byte x, 
                   byte y, 
                   byte anim, 
                   uint16_t rot/* = 0xFFFF */)
{
    x -= 16;
    y -= 8;
    if (rot != 0xFFFF) 
    {
        GD_cmd_loadidentity(my_GD);
        GD_cmd_translate(my_GD, F16(8),F16(8));
        GD_cmd_rotate(my_GD, rot);
        GD_cmd_translate(my_GD, F16(-8),F16(-8));
        GD_cmd_scale(my_GD, F16(sf), F16(sf));
        GD_cmd_setmatrix(my_GD);
    } /* end if */
    GD_Cell(my_GD, anim);
    if (x > 224) 
    {
        GD_Vertex2f(my_GD, sf * (x - 256), sf * y);
    } 
    else 
    {
        GD_Vertex2f(my_GD, sf * x, sf * y);
    } /* end if */
} /* end sprite */


static void turtle3(GD *my_GD, 
                    byte x, 
                    byte y)
{
    byte anim = 50 + ((t / 32) % 3);

    sprite(my_GD, x, y, anim, 0xFFFF);
    sprite(my_GD, x + 16, y, anim, 0xFFFF);
    sprite(my_GD, x + 32, y, anim, 0xFFFF);
} /* end turtle3 */


static void turtle2(GD *my_GD,
                    byte x, 
                    byte y)
{
    byte anim = 50 + ((t / 32) % 3);
    sprite(my_GD, x, y, anim, 0xFFFF);
    sprite(my_GD, x + 16, y, anim, 0xFFFF);
} /* end turtle2 */


static void frogger_log(GD *my_GD,
                        byte length, 
                        byte x, 
                        byte y)
{
    sprite(my_GD, x, y, 86, 0xFFFF);
    while (length--) 
    {
        x += 16;
        sprite(my_GD, x, y, 87, 0xFFFF);
    } /* end while */
    sprite(my_GD, x + 16, y, 88, 0xFFFF);
} /* end frogger_log */


static int riverat(byte y, 
                   uint16_t tt)
{
    switch (y) 
    {
        case 120: 
            return -tt;
        case 104: 
            return tt;
        case 88:  
            return 5 * tt / 4;
        case 72:  
            return -tt / 2;
        case 56:  
            return tt / 2;
        default: 
            return 0;
    } /* end switch */
} /* end riverat */


static void squarewave(GD *my_GD,
                       uint16_t freq, 
                       byte amp)
{
    GD_wr(my_GD, REG_VOL_SOUND, amp);
    GD_wr16(my_GD, REG_SOUND, (freq << 8) | 0x01);
    GD_wr(my_GD, REG_PLAY, 1);
} /* end squarewave */


static void sound(GD *my_GD)
{
    byte note;

    if (dying) 
    { 
        note = 84 - (dying / 2);
        squarewave(my_GD, note, 100);
    } 
    else if (leaping) 
    {
        if (leaping & 1)
        note = 60 + leaping;
        else
        note = 72 + leaping;
        squarewave(my_GD, note, 100);
    } 
    else 
    {
        squarewave(my_GD, 0, 0);  // silence
    } /* end if */
} /* end sound */


static void game_over(GD *my_GD)
{
    byte    i;
    int     x, y, space;

    GD_wr(my_GD, REG_VOL_SOUND, 0);
    for (i = 0; i < 60; i++) 
    {
        GD_Clear(my_GD);

        // Draw "F R O G G E R" using the sprites 90-94
        x     = 160;
        y     = 50;
        space = 24;

        GD_Begin(my_GD, BITMAPS);
        GD_Vertex2ii(my_GD, x, y, SPRITES_HANDLE, 90); x += space;   // F
        GD_Vertex2ii(my_GD, x, y, SPRITES_HANDLE, 91); x += space;   // R
        GD_Vertex2ii(my_GD, x, y, SPRITES_HANDLE, 92); x += space;   // O
        GD_Vertex2ii(my_GD, x, y, SPRITES_HANDLE, 93); x += space;   // G
        GD_Vertex2ii(my_GD, x, y, SPRITES_HANDLE, 93); x += space;   // G
        GD_Vertex2ii(my_GD, x, y, SPRITES_HANDLE, 94); x += space;   // E
        GD_Vertex2ii(my_GD, x, y, SPRITES_HANDLE, 91); x += space;   // R

        GD_cmd_text(my_GD, 240, 136, FONT_HANDLE, OPT_CENTER, "GAME OVER");
        if (i == 59)
            GD_cmd_text(my_GD, 240, 200, FONT_HANDLE, OPT_CENTER, "PRESS TO PLAY");
        GD_swap(my_GD);
    }
    while (my_GD->inputs.x == -32768)
        GD_get_inputs(my_GD);
    while (my_GD->inputs.x != -32768)
        GD_get_inputs(my_GD);
} /* end game_over */

/* ************************************************************************* */

void setup_frogger(GD *my_GD,
                   GD_Transport *my_GD_Transport,
                   GD_SDCard *my_GD_SDCard,
                   GD_Reader *my_GD_Reader)
{
    GD_begin(my_GD, my_GD_Transport, my_GD_SDCard, my_GD_Reader, ~GD_STORAGE);

    GD_copy(my_GD, __assets, sizeof(__assets));

    GD_BitmapHandle(my_GD, BACKGROUND_HANDLE);
    GD_BitmapSize(my_GD, NEAREST, BORDER, BORDER, my_GD->w, my_GD->h);
    GD_BitmapHandle(my_GD, SPRITES_HANDLE);
    GD_BitmapSize(my_GD, NEAREST, BORDER, BORDER, 3 * 16, 3 * 16);

    game_setup(my_GD);
} /* end setup_frogger */


void run_frogger(GD *my_GD)
{
    static uint32_t counter;
    static int      prevt;
    int             x0          = 0;
    int             i;
    byte            tw, tx;
    byte            con;
    byte            tag;
    byte            touching;

    while (1)
    {
        GD_VertexFormat(my_GD, 0);
        GD_Clear(my_GD);
        x0 = (1280 - sf * 224) / 2;
        GD_VertexTranslateX(my_GD, 16 * x0);
        GD_cmd_scale(my_GD, F16(sf), F16(sf));
        GD_cmd_setmatrix(my_GD);

        GD_Tag(my_GD, 1);
        GD_BitmapHandle(my_GD, SPRITES_HANDLE);
        GD_SaveContext(my_GD);
        GD_ScissorXY(my_GD, x0, 0);
        GD_ScissorSize(my_GD, sf * 224, sf * 256);
        GD_Begin(my_GD, BITMAPS);
        GD_Vertex2ii(my_GD, 0, 0, BACKGROUND_HANDLE, 0);        // Background bitmap

        GD_wr(my_GD, REG_TAG_X, sf * (frogx - 8));
        GD_wr(my_GD, REG_TAG_Y, sf * (frogy));

        GD_Tag(my_GD, 2);
        GD_AlphaFunc(my_GD, GREATER, 0);                        // on road, don't tag transparent pixels

        // Completed homes
        for (i = 0; i < 5; i++) 
        {
            if (done[i])
                sprite(my_GD, homes[i], 40, 63, 0xFFFF);
        } /* end if */

        // Yellow cars
        sprite(my_GD, -t,       216, 3, 0xFFFF);
        sprite(my_GD, -t + 128, 216, 3, 0xFFFF);

        // Dozers
        sprite(my_GD, t,       200, 4, 0xFFFF);
        sprite(my_GD, t + 50,  200, 4, 0xFFFF);
        sprite(my_GD, t + 150, 200, 4, 0xFFFF);

        // Purple cars
        sprite(my_GD, -t,         184, 7, 0xFFFF);
        sprite(my_GD, -t + 75,    184, 7, 0xFFFF);
        sprite(my_GD, -t + 150,   184, 7, 0xFFFF);

        // Green and white racecars
        sprite(my_GD, 2 * t,      168, 8, 0xFFFF);

        // Trucks
        sprite(my_GD, -t/2,       152, 5, 0xFFFF);
        sprite(my_GD, -t/2 + 16,  152, 6, 0xFFFF);
        sprite(my_GD, -t/2 + 100, 152, 5, 0xFFFF);
        sprite(my_GD, -t/2 + 116, 152, 6, 0xFFFF);

        GD_AlphaFunc(my_GD, GREATER, 0);                        // on river, tag transparent pixels

        // Turtles
        for (i = 0; i < 256; i += 64)
            turtle3(my_GD, riverat(120, t) + i, 120);

        // Short logs
        for (i = 0; i < 240; i += 80)
            frogger_log(my_GD, 1, riverat(104, t) + i, 104);

        // Long logs
        for (i = 0; i < 256; i += 128)
            frogger_log(my_GD, 5, riverat(88, t) + i, 88);

        // Turtles again, but slower
        for (i = 0; i < 250; i += 50)
            turtle2(my_GD, riverat(72, t) + i, 72);

        // Top logs
        for (i = 0; i < 210; i += 70)
            frogger_log(my_GD, 2, riverat(56, t) + i, 56);

        // The frog himself, or his death animation
        GD_TagMask(my_GD, 0);

        if (!dying) 
        {
            static byte frog_anim[] = {2, 1, 0, 0, 2};

            sprite(my_GD, frogx, frogy, frog_anim[leaping / 2], frogface);
        } 
        else 
        {
            static byte die_anim[]  = {31, 32, 33, 30};

            sprite(my_GD, frogx, frogy, die_anim[dying / 16], frogface);
        } /* end if */

        prevt   = t;
        t++;
        ti      = max(0, ti - 1);
        if ((ti == 0) && (dying == 0))
            dying = 1;

        // Draw 'time remaining' by clearing a black rectangle
        tw  = 120 - (ti >> 7);
        tx  = 72;

        GD_SaveContext(my_GD);
        GD_ScissorXY(my_GD, sf * tx, sf * 248);
        GD_ScissorSize(my_GD, sf * tw, sf * 8);
        GD_Clear(my_GD);
        GD_RestoreContext(my_GD);

        GD_TagMask(my_GD, 1);

        // player control.  If button pressed, start the 'leaping' counter
        con = Controller_read(&Control);
        if (!dying && (leaping == 0) && con) 
        {
            frogdir = con;
            leaping = 1;
            score += 10;
        }
        else if (leaping > 0) 
        {
            if (leaping <= 8) 
            {
                if (frogdir == CONTROL_LEFT) 
                {
                    frogx -= 2;
                    frogface = 0xc000;
                } /* end if */ 
                if (frogdir == CONTROL_RIGHT) 
                {
                    frogx += 2;
                    frogface = 0x4000;
                } /* end if */ 
                if (frogdir == CONTROL_UP) 
                {
                    frogy -= 2;
                    frogface = 0x0000;
                } /* end if */
                if (frogdir == CONTROL_DOWN) 
                {
                    frogy += 2;
                    frogface = 0x8000;
                } /* end if */
                leaping++;
            } /* end if */
            else 
            {
                leaping = 0;
            } /* end if */
        } /* end if */

        GD_RestoreContext(my_GD);
        GD_SaveContext(my_GD);
        GD_Begin(my_GD, BITMAPS);

        GD_swap(my_GD);

        GD_get_inputs(my_GD);
        tag = GD_rd(my_GD, REG_TAG);
        touching = (tag == 2);

        if (dying) 
        {
            if (++dying == 64) 
            {
                if (--lives == 0 || ti == 0) 
                {
                    game_over(my_GD);
                    game_start();
                    level_start();
                } /* end if */
                frog_start();
            } /* end if */
        }
        else if (frogx < 8 || frogx > 224) 
        {
            dying = 1;
        }
        else if (frogy >= 136) 
        {    
            // road section
            // if touching something, frog dies
            if (tag == 2)
                dying = 1;
        }
        else if (frogy > 40) 
        {      
            // river section
            if (!leaping) 
            {
                // if touching something, frog is safe
                if (tag != 1) 
                {
                    // move frog according to lane speed
                    int oldx = riverat(frogy, prevt);
                    int newx = riverat(frogy, t);
                    int river_velocity = newx - oldx;
                    frogx += river_velocity;
                } 
                else 
                {
                    dying = 1;
                } /* end if */
            } /* end if */
        }
        else 
        {                      
            // riverbank section
            if (!leaping) 
            {
                byte landed = 0;
                for (i = 0; i < 5; i ++) 
                {
                    if (!done[i] && abs(homes[i] - frogx) < 4) 
                    {
                        done[i] = 1;
                        landed = 1;
                        score += 10;
                    } /* end if */
                } /* end for */
                if (landed) 
                {
                    if (done[0] && done[1] && done[2] && done[3] && done[4])
                        level_start();
                    frog_start();
                } 
                else 
                    // if frog did not land in a home, die!
                    dying = 1;
            } /* end if */
        } /* end if */
        sound(my_GD);
        hiscore = max(score, hiscore);
    } /* end while */
} /* end run_frogger */

/* ************************************************************************* */
