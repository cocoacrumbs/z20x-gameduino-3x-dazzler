#include "Controller.h"

/* ************************************************************************* */

void Controller_begin(Controller *self,
                      GD *my_GD)
{
    self->my_GD = my_GD;
    self->prev  = 0;
} /* end Controller_begin */


byte Controller_read(Controller *self)
{
      byte  r       = self->my_GD->inputs.tag & (CONTROL_LEFT | CONTROL_RIGHT | CONTROL_UP | CONTROL_DOWN);
      byte  edge    = r & ~(self->prev);

      self->prev = r;
      return edge;
} /* end Controller_read */

/* ************************************************************************* */
