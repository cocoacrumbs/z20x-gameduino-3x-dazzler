#include <math.h>>
#include <string.h>>

#include "Bitmap.h"
#include "cube.h"
#include "utils.h"

/* ************************************************************************* */

#define PICTURE_HANDLE  0
#define P               125
#define N               -P

#define N_VERTICES      (sizeof(CUBE_vertices) / 3)

/* ************************************************************************* */
extern float    angle;
extern float    axis[3];
extern float    lastPos[3];
extern float    model_mat[9];
extern float    normal_mat[9];

/* ************************************************************************* */

static const int8_t CUBE_vertices[] = 
{
    P,P,P,
    N,P,P,
    P,N,P,
    N,N,P,

    P,P,N,
    N,P,N,
    P,N,N,
    N,N,N
}; /* end CUBE_vertices */


// each line is a face: count, normal, 4 vertices
static const int8_t CUBE_faces[] = 
{
    4, 0,0,127,   0, 1, 3, 2,
    4, 0,0,-127,  6, 7, 5, 4,

    4, 0,127,0,   4, 5, 1, 0,
    4, 0,-127,0,  2, 3, 7, 6,

    4, 127,0,0,   0, 2, 6, 4,
    4, -127,0,0,  3, 1, 5, 7,

    -1
}; /* end CUBE_faces */

/* ************************************************************************* */

static point2   projected[N_VERTICES];
static Bitmap   background;
static Bitmap   foreground;
static byte     prev_touching           = 0;

/* ************************************************************************* */

static void draw_faces(GD *my_GD,
                       int dir)
{
    int     R = 15;
    int8_t *p = CUBE_faces; 
    byte    n;

    GD_BlendFunc(my_GD, ONE, ONE_MINUS_SRC_ALPHA);
    GD_Begin(my_GD, BITMAPS);

    while ((n = pgm_read_byte_near(p++)) != 0xFF) 
    {
        int8_t  nx = pgm_read_byte_near(p++);
        int8_t  ny = pgm_read_byte_near(p++);
        int8_t  nz = pgm_read_byte_near(p++);
        byte    v1 = pgm_read_byte_near(p);
        byte    v2 = pgm_read_byte_near(p + 1);
        byte    v3 = pgm_read_byte_near(p + 2);
        // p += n;

        long x1 = projected[v1].x;
        long y1 = projected[v1].y;
        long x2 = projected[v2].x;
        long y2 = projected[v2].y;
        long x3 = projected[v3].x;
        long y3 = projected[v3].y;
        long area = (x1 - x3) * (y2 - y1) - (x1 - x2) * (y3 - y1);

        byte face = (area < 0);

        p += n;
        if (face == dir) 
        {
            uint16_t r = 20, g = 20, b = 80;        // Ambient

            if (face) 
            {
                int d;

                transform_normal(&nx, &ny, &nz);
                d = max(0, -nz);                    // diffuse light from +ve Z
                r += 2 * d;
                g += 2 * d;
                b += 2 * d;
            } /* end if */
            GD_ColorR_G_B(my_GD, min(255, r), min(255, g), min(255, b));

            x1 >>= 4;
            y1 >>= 4;
            x2 >>= 4;
            y2 >>= 4;
            x3 >>= 4;
            y3 >>= 4;
            quad(my_GD, x1, y1, x2, y2, x3, y3, -R, -R, 128 + R, 128 + R);
        } /* end if */
    } /* end while */
} /* end draw_faces */

/* ************************************************************************* */

void setup_cube(GD *my_GD,
                GD_Transport *my_GD_Transport,
                GD_SDCard *my_GD_SDCard,
                GD_Reader *my_GD_Reader)
{
    GD_begin_default_options(my_GD, my_GD_Transport, my_GD_SDCard, my_GD_Reader);
    Bitmap_begin(&background, my_GD);
    Bitmap_begin(&foreground, my_GD);

    Bitmap_fromfile_default_format(&background, "tree.jpg");
    Bitmap_fromfile_default_format(&foreground, "healsky3.jpg");

    Bitmap_bind(&foreground, PICTURE_HANDLE);
    GD_BitmapHandle(my_GD, PICTURE_HANDLE);
    GD_BitmapSize(my_GD, NEAREST, BORDER, BORDER, my_GD->w, my_GD->h);

    startMotion(my_GD, 240, 136);
    trackMotion(my_GD, 247, 138);
} /* end setup_cube */


void run_cube(GD *my_GD)
{
    while (1)
    {
        GD_get_inputs(my_GD);

        GD_Clear(my_GD);
        GD_SaveContext(my_GD);
        GD_ColorRGB(my_GD, 0x605040);
        Bitmap_wallpaper(&background);
        GD_RestoreContext(my_GD);

        if (!prev_touching && my_GD->inputs.touching)
            startMotion(my_GD, my_GD->inputs.x, my_GD->inputs.y);
        else if (my_GD->inputs.touching)
            trackMotion(my_GD, my_GD->inputs.x, my_GD->inputs.y);
        prev_touching = my_GD->inputs.touching;

        if (angle != 0.0f)
            rotation(angle, axis);

        project_2D(my_GD, CUBE_vertices, sizeof(CUBE_vertices), projected, ((64 * my_GD->h) / 280));
        draw_faces(my_GD, 0);
        draw_faces(my_GD, 1);
        GD_RestoreContext(my_GD);

        GD_swap(my_GD);
    } /* end while */
} /* end cube */

/* ************************************************************************* */
