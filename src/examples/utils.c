#include <Math.h>
#include <String.h>

#include "utils.h"

/* ************************************************************************* */

#define M(nm,i,j)       ((nm)[3 * (i) + (j)])

/* ************************************************************************* */

float model_mat[9] = { 1.0, 0.0, 0.0,
                       0.0, 1.0, 0.0,
                       0.0, 0.0, 1.0 };

float normal_mat[9] = { 1.0, 0.0, 0.0,
                        0.0, 1.0, 0.0,
                        0.0, 0.0, 1.0 };

/* ************************************************************************* */

void mult_matrices(float *a, 
                   float *b, 
                   float *c)
{
    int     i, j, k;
    float   result[9];

    for (i = 0; i < 3; i++) 
    {
        for (j = 0; j < 3; j++) 
        {
            M(result,i,j) = 0.0f;
            for (k = 0; k < 3; k++) 
            {
                M(result,i,j) +=  M(a,i,k) *  M(b,k,j);
            } /* end for */
        } /* end for */
    } /* end for */
    memcpy(c, result, sizeof(result));
} /* end mult_matrices */


void project_2D(GD *GD,
                int8_t *vertices,
                uint16_t sizeof_vertices,
                point2 *dst,
                byte scale)
{
    byte     vx;
    int8_t  *pm         = vertices; 
    int8_t  *pm_e       = pm + sizeof_vertices;
    int16_t  x, y, z;
    float    xx, yy;

    while (pm < pm_e) 
    {
        x = (scale * (int8_t)pgm_read_byte_near(pm++)) >> 6;
        y = (scale * (int8_t)pgm_read_byte_near(pm++)) >> 6;
        z = (scale * (int8_t)pgm_read_byte_near(pm++)) >> 6;
        xx = x * model_mat[0] + y * model_mat[3] + z * model_mat[6];
        yy = x * model_mat[1] + y * model_mat[4] + z * model_mat[7];
        dst->x = 16 * (GD->w / 2 + xx);
        dst->y = 16 * (GD->h / 2 + yy);
        dst++;
    } /* end while */
} /* end project */


void project_3D(GD *my_GD,
                float distance,
                int8_t *vertices,
                uint16_t sizeof_vertices,
                xyz *dst,
                byte scale)
{
    int8_t  *pm     = vertices; 
    int8_t  *pm_e   = pm + sizeof_vertices;
    int8_t  x;
    int8_t  y;
    int8_t  z;
    int     hw      = my_GD->w / 2;

    while (pm < pm_e) 
    {
        float   xx;
        float   yy;
        float   zz;
        float   q;

        x = pgm_read_byte_near(pm++);
        y = pgm_read_byte_near(pm++);
        z = pgm_read_byte_near(pm++);
        
        xx = x * model_mat[0] + y * model_mat[3] + z * model_mat[6];
        yy = x * model_mat[1] + y * model_mat[4] + z * model_mat[7];
        zz = x * model_mat[2] + y * model_mat[5] + z * model_mat[8] + distance;
        q = hw / (100 + zz);
        
        dst->x = scale * (hw             + xx * q);
        dst->y = scale * ((my_GD->h / 2) + yy * q);
        dst->z = zz;
        dst++;
    } /* end while */
} /* end project */


void quad(GD *my_GD,
          int x1, int y1,
          int x2, int y2,
          int x3, int y3,
          int bx1, int by1,
          int bx3, int by3)
{
    int min_x1x2, max_x1x2;
    int min_x3x4, max_x3x4;
    int min_y1y2, max_y1y2;
    int min_y3y4, max_y3y4;

    // Compute the fourth vertex of the parallelogram, (x4,y4)
    int x4;
    int y4;

    // Apply Scissor to the extents of the quad
    int minx;
    int maxx;
    int miny;
    int maxy;

    x4 = x3 + (x1 - x2);
    y4 = y3 + (y1 - y2);

    min_x1x2 = min(x1, x2);
    min_x3x4 = min(x3, x4);
    min_y1y2 = min(y1, y2);
    min_y3y4 = min(y3, y4);

    max_x1x2 = max(x1, x2);
    max_x3x4 = max(x3, x4);
    max_y1y2 = max(y1, y2);
    max_y3y4 = max(y3, y4);

    minx = max(0,        min(min_x1x2, min_x3x4));
    maxx = min(my_GD->w, max(max_x1x2, max_x3x4));
    miny = max(0,        min(min_y1y2, min_y3y4));
    maxy = min(my_GD->h, max(max_y1y2, max_y3y4));

    GD_ScissorXY(my_GD, minx, miny);
    GD_ScissorSize(my_GD, maxx - minx, maxy - miny);

    // // Set the new bitmap transform
    GD_cmd32(my_GD, 0xFFFFFF21UL); // bitmap transform
    GD_cmd32(my_GD, x1 - minx); GD_cmd32(my_GD, y1 - miny);
    GD_cmd32(my_GD, x2 - minx); GD_cmd32(my_GD, y2 - miny);
    GD_cmd32(my_GD, x3 - minx); GD_cmd32(my_GD, y3 - miny);

    GD_cmd32(my_GD, bx1); GD_cmd32(my_GD, by1);
    GD_cmd32(my_GD, bx1); GD_cmd32(my_GD, by3);
    GD_cmd32(my_GD, bx3); GD_cmd32(my_GD, by3);
    GD_cmd32(my_GD, 0);

    // Draw the quad
    GD_Vertex2f(my_GD, PIXELS(minx), PIXELS(miny));
} /* end quad */


void rotation(float angle, 
              float *axis)
{
    float mat[9];
    float mati[9];

    rotate(mat, mati, angle, axis);
    mult_matrices(model_mat, mat, model_mat);
    mult_matrices(mati, normal_mat, normal_mat);
} /* end rotation */


// Based on glRotate()
// Returns 3x3 rotation matrix in 'm'
// and its invese in 'mi'
void rotate(float *m, 
            float *mi, 
            float angle, 
            float *axis)
{
    float x = axis[0];
    float y = axis[1];
    float z = axis[2];

    float s = sin(angle);
    float c = cos(angle);

    float xx = x*x*(1-c);
    float xy = x*y*(1-c);
    float xz = x*z*(1-c);
    float yy = y*y*(1-c);
    float yz = y*z*(1-c);
    float zz = z*z*(1-c);

    float xs = x * s;
    float ys = y * s;
    float zs = z * s;

    m[0] = xx + c;
    m[1] = xy - zs;
    m[2] = xz + ys;

    m[3] = xy + zs;
    m[4] = yy + c;
    m[5] = yz - xs;

    m[6] = xz - ys;
    m[7] = yz + xs;
    m[8] = zz + c;

    mi[0] = m[0];
    mi[1] = xy + zs;
    mi[2] = xz - ys;

    mi[3] = xy - zs;
    mi[4] = m[4];
    mi[5] = yz + xs;

    mi[6] = xz + ys;
    mi[7] = yz - xs;
    mi[8] = m[8];
} /* end rotate */


void transform_normal(int8_t *nx, 
                      int8_t *ny, 
                      int8_t *nz)
{
    int8_t xx = *nx * normal_mat[0] + *ny * normal_mat[1] + *nz * normal_mat[2];
    int8_t yy = *nx * normal_mat[3] + *ny * normal_mat[4] + *nz * normal_mat[5];
    int8_t zz = *nx * normal_mat[6] + *ny * normal_mat[7] + *nz * normal_mat[8];
    *nx = xx;
    *ny = yy;
    *nz = zz;
} /* end transform_normal */

/* ************************************************************************* */

/* simple trackball-like motion control */
/* Based on projtex.c - by David Yu and David Blythe, SGI */

float   angle;
float   axis[3]     = {0,1,0};
float   lastPos[3];

void ptov(int x, 
          int y, 
          int width, 
          int height, 
          float v[3])
{
    float d, a;

    /* project x,y onto a hemi-sphere centered within width, height */
    v[0] = (2.0 * x - width) / width;
    v[1] = (2.0 * y - height) / height;
    d = sqrt(v[0] * v[0] + v[1] * v[1]);
    v[2] = cos((M_PI / 2.0) * ((d < 1.0) ? d : 1.0));
    a = 1.0 / sqrt(v[0] * v[0] + v[1] * v[1] + v[2] * v[2]);
    v[0] *= a;
    v[1] *= a;
    v[2] *= a;
} /* end ptov */


void startMotion(GD *my_GD,
                 int x, 
                 int y)
{
    angle = 0.0;
    ptov(x, y, my_GD->w, my_GD->h, lastPos);
} /* end startMotion */


void trackMotion(GD *my_GD, 
                 int x, 
                 int y)
{
    float curPos[3], dx, dy, dz, mag;

    ptov(x, y, my_GD->w, my_GD->h, curPos);

    dx = curPos[0] - lastPos[0];
    dy = curPos[1] - lastPos[1];
    dz = curPos[2] - lastPos[2];
    angle = (M_PI / 2) * sqrt(dx * dx + dy * dy + dz * dz);

    axis[0] = lastPos[1] * curPos[2] - lastPos[2] * curPos[1];
    axis[1] = lastPos[2] * curPos[0] - lastPos[0] * curPos[2];
    axis[2] = lastPos[0] * curPos[1] - lastPos[1] * curPos[0];

    mag = 1 / sqrt(axis[0] * axis[0] + axis[1] * axis[1] + axis[2] * axis[2]);
    axis[0] *= mag;
    axis[1] *= mag;
    axis[2] *= mag;

    lastPos[0] = curPos[0];
    lastPos[1] = curPos[1];
    lastPos[2] = curPos[2];
} /* end trackMotion */

/* ************************************************************************* */
