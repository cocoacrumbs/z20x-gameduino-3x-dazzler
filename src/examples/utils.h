#ifndef UTILS_H
#define UTILS_H

#include "GD_Defines.h"
#include "GD.h"

/* ************************************************************************* */

typedef struct 
{
    int32_t x;
    int32_t y;   
} point2;

typedef struct 
{
    int     x;
    int     y;
    float   z;
} xyz;

/* ************************************************************************* */

void mult_matrices(float *a, 
                   float *b, 
                   float *c);

void project_2D(GD *GD,
                int8_t *vertices,
                uint16_t sizeof_vertices,
                point2 *dst,
                byte scale);

void project_3D(GD *my_GD,
                float distance,
                int8_t *vertices,
                uint16_t sizeof_vertices,
                xyz *dst,
                byte scale);

void quad(GD *my_GD,
          int x1, int y1,
          int x2, int y2,
          int x3, int y3,
          int bx1, int by1,
          int bx3, int by3);

void rotation(float angle, 
              float *axis);

void rotate(float *m, 
            float *mi, 
            float angle, 
            float *axis);

void transform_normal(int8_t *nx, 
                      int8_t *ny, 
                      int8_t *nz);

/* ************************************************************************* */

void ptov(int x, 
          int y, 
          int width, 
          int height, 
          float v[3]);

void startMotion(GD *my_GD,
                 int x, 
                 int y);

void trackMotion(GD *my_GD, 
                 int x, 
                 int y);

/* ************************************************************************* */

#endif UTILS_H