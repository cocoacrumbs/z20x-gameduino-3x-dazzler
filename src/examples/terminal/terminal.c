#include <math.h>
#include <stdio.h>

#include "timer.h"
#include "terminal.h"

// Some nice colors to work with
static uint32_t colors_endesga32[] = 
{
    0xBE4A2F, 0xD77643, 0xEAD4AA, 0xE4A672,
    0xB86F50, 0x733E39, 0x3E2731, 0xA22633,
    0xE43B44, 0xF77622, 0xFEAE34, 0xFEE761,
    0x63C74D, 0x3E8948, 0x265C42, 0x193C3E,
    0x124E89, 0x0099DB, 0x2CE8F5, 0xFFFFFF,
    0xC0CBDC, 0x8B9BB4, 0x5A6988, 0x3A4466,
    0x262B44, 0x181425, 0xFF0044, 0x68386C,
    0xB55088, 0xF6757A, 0xE8B796, 0xC28569,
}; /* end colors_endesga32 */

void setup_terminal(GD *my_GD,
                    GD_Transport *my_GD_Transport,
                    GD_SDCard *my_GD_SDCard,
                    GD_Reader *my_GD_Reader,
                    GD_Terminal *my_GD_Terminal)
{
    GD_Terminal_init(my_GD_Terminal, my_GD);
    GD_begin(my_GD, my_GD_Transport, my_GD_SDCard, my_GD_Reader, (GD_CALIBRATE | GD_TRIM | GD_STORAGE));
    
    // Setup the terminal. Must come after GD.begin -------------------
    GD_Terminal_begin(my_GD_Terminal, TEXTVGA);                 // alternative is to use TEXT8X8
    GD_Terminal_set_window_bg_color(my_GD_Terminal, 0x000000);  // Background window color
    GD_Terminal_set_window_opacity(my_GD_Terminal, 200);        // 0-255

    // Changing Fonts -------------------------------------------------
    // 8X8 Monochrome
    // GD_Terminal_set_font_8x8(my_GD_Terminal);
    // VGA 16-color
    GD_Terminal_set_font_vga(my_GD_Terminal);
    // VGA background colors are disabled by default.
    GD_Terminal_disable_vga_background_colors(my_GD_Terminal);
    // Enable character backgound color. Disables window color & opacity
    // GD_Terminal_enable_vga_background_colors(my_GD_Terminal);

    // Font changes must be followed by setting a size ---------------
    // Fullscreen
    GD_Terminal_set_size_fullscreen(my_GD_Terminal);
    // Custom Size
    // GD_Terminal_change_size(my_GD_Terminal, row_count, column_count);

    // Get FT81X first available ram address (after terminal data) ---
    // GD_Terminal_ram_end_address(my_GD_Terminal);

    GD_ClearColorRGB(my_GD, 0x000000);
} /* end setup_terminal */


static float start_offset = 0;

void draw_circles(GD *my_GD) 
{
    int start_x;
    int start_y;
    int i;
    int x;

    GD_SaveContext(my_GD);
    GD_Clear(my_GD);

    // Draw some moving circles
    GD_PointSize(my_GD, 16 * 30); // means 30 pixels
    GD_Begin(my_GD, POINTS);
    
    start_x = floor(my_GD->w / 34);
    start_y = floor(my_GD->h / 2);
    start_offset += M_TWO_PI/1024;

    if (start_offset > M_TWO_PI) 
    {
        start_offset = 0;
    } /* end if */

    for (i = 0; i < 32; i++) 
    {
        GD_ColorRGB(my_GD, colors_endesga32[i]);
        x = (i + 2) * start_x;
        GD_Vertex2ii(my_GD, x, start_y + ((my_GD->h/3) * sin(x + start_offset)), 0, 0);
    } /* end for */
    GD_RestoreContext(my_GD);
} /* end draw_circles */

// // FPS and time per game loop
// uint32_t delta_time_start = 0;
// uint32_t delta_time_micros = 30000; // initial guess
// // Display List size
uint16_t        display_list_offset     = 0;
static char     line_buffer[160];
static uint32_t last_terminal_refresh   = 0;

extern byte ft8xx_model;

void run_terminal(GD *my_GD,
                  GD_Terminal *my_GD_Terminal)
{
    char    c;
    int     i;

    while (1)
    {
    //   // Frame time start
    //   delta_time_start = micros();

        // Get touchscreen inputs
        GD_get_inputs(my_GD);
        GD_Terminal_update_scrollbar(my_GD_Terminal);

        // Your app update code here

        // Your app draw code here
        draw_circles(my_GD);

        // Every 2 seconds print something
        if (millis() > last_terminal_refresh + 2000) 
        {
            GD_Terminal_append_character(my_GD_Terminal, '\n');

            // Print time
            my_GD_Terminal->foreground_color = TERMINAL_VGA_BRIGHT_MAGENTA;
            my_GD_Terminal->background_color = TERMINAL_VGA_BRIGHT_CYAN;
            GD_Terminal_append_string(my_GD_Terminal, "[Time] ");

            my_GD_Terminal->foreground_color = TERMINAL_VGA_WHITE;
            my_GD_Terminal->background_color = TERMINAL_VGA_BLACK;
            sprintf(line_buffer, "%02d:%02d:%02d ", 16, 41, GD_random_1(my_GD, 60));
            GD_Terminal_append_string(my_GD_Terminal, line_buffer);

            my_GD_Terminal->foreground_color = TERMINAL_VGA_BRIGHT_WHITE;
            sprintf(line_buffer, "%04d-%02d-%02d\n", 2021, 2, 14);
            GD_Terminal_append_string(my_GD_Terminal, line_buffer);

            my_GD_Terminal->foreground_color = TERMINAL_VGA_BRIGHT_GREEN;
            sprintf(line_buffer, "[Battery] %1.2f V\n", 2.5 + 2 * ((float)GD_random_1(my_GD, 1024) / 1024.0));
            GD_Terminal_append_string(my_GD_Terminal, line_buffer);

            my_GD_Terminal->foreground_color = TERMINAL_VGA_BRIGHT_CYAN;
            GD_Terminal_append_string(my_GD_Terminal, "\nAll Characters:");

            // Print all characters
            for (i=0; i<256; i++) 
            {
                my_GD_Terminal->foreground_color = i % 16;
                c = i;
                if (c > 255 || c == '\n' || c == '\r')
                    c = ' ';
                GD_Terminal_append_character(my_GD_Terminal, c);

                if (i % my_GD_Terminal->characters_per_line == 0)
                    GD_Terminal_append_character(my_GD_Terminal, '\n');
            } /* end for */
            GD_Terminal_append_character(my_GD_Terminal, '\n');

            last_terminal_refresh = millis();
        } /* end if */

        // Draw terminal.
        GD_Terminal_draw_XY(my_GD_Terminal, 0, 0);

        // // Print extra debug info;
        // // FPS and Display List Usage for the previous frame.
        // float delta_time = (float)delta_time_micros * 1e-6;
        // sprintf(line_buffer,
        //         "FPS: %.2f --- DisplayList: %3.2f%% (%u / 8192)",
        //         1.0 / delta_time,
        //         100.0 * ((float) display_list_offset/8192),
        //         display_list_offset);
        // // Draw white text with a black outline.
        // GD.ColorRGB(COLOR_LIGHT_STEEL_BLUE);
        // GD.cmd_text(1, GD.h-16,   18, 0, line_buffer);

        // Get the size (current position) of the display list.
        GD_finish(my_GD);
        display_list_offset = GD_rd16(my_GD, REG_CMD_DL);

        GD_swap(my_GD);

        // // Frame time end.
        // delta_time_micros = micros() - delta_time_start;
    } /* end while */
} /* end terminal */

