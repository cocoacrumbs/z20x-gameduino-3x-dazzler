#ifndef TERMINAL_H
#define TERMINAL_H

#include "GD_Defines.h"
#include "GD.h"
#include "GD_Transport.h"
#include "GD_Reader.h"
#include "GD_SDCard.h"
#include "GD_Terminal.h"

void setup_terminal(GD *my_GD,
                    GD_Transport *my_GD_Transport,
                    GD_SDCard *my_GD_SDCard,
                    GD_Reader *my_GD_Reader,
                    GD_Terminal *my_GD_Terminal);

void run_terminal(GD *my_GD,
                  GD_Terminal *my_GD_Terminal);

#endif TERMINAL_H