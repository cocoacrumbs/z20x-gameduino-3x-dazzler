#include "blobs.h"
#include "xy.h"


#define NBLOBS      128

xy          blobs[NBLOBS];
const xy    offscreen       = {-16384, -16384};

void setup_blobs(GD *my_GD,
                 GD_Transport *my_GD_Transport,
                 GD_SDCard *my_GD_SDCard,
                 GD_Reader *my_GD_Reader)
{
    int i;

    GD_begin(my_GD, my_GD_Transport, my_GD_SDCard, my_GD_Reader, ~GD_STORAGE);

    for (i = 0; i < NBLOBS; i++)
        blobs[i] = offscreen;
} /* end setup_blobs */


void run_blobs(GD *my_GD)
{
    while (1)
    {
        static byte blob_i;
        int         i;

        GD_get_inputs(my_GD);
        if (my_GD->inputs.x != -32768)
            blobs[blob_i] = my_GD->inputs.xytouch;
        else
            blobs[blob_i] = offscreen;
        blob_i = (blob_i + 1) & (NBLOBS - 1);

        GD_ClearColorRGB(my_GD, 0xD0D0C0);
        GD_ClearColorRGB(my_GD, 0xE0E0E0);
        GD_Clear(my_GD);

        GD_ColorRGB(my_GD, 0xA0A0A0);
        GD_cmd_text(my_GD, 240, 136, 31, OPT_CENTER, "touch to draw");

        // Draw the blobs from oldest to youngest // JCB
        GD_Begin(my_GD, POINTS);
        for (i = 0; i < NBLOBS; i++) 
        {
            uint8_t j;
            byte    r, g, b;

            // Blobs fade away and swell as they age
            GD_ColorA(my_GD, i << 1);
            GD_PointSize(my_GD, (1024 + 16) - (i << 3));

            // Random color for each blob, keyed from (blob_i + i)
            j = (blob_i + i) & (NBLOBS - 1);
            r = j * 17;
            g = j * 23;
            b = j * 147;
            GD_ColorR_G_B(my_GD, r, g, b);

            // Draw it!
            GD_Vertex2f(my_GD, blobs[j].x - PIXELS(0), blobs[j].y - PIXELS(0));
        } /* end for */
        GD_swap(my_GD);
    } /* end while */
} /* end run_blobs */
