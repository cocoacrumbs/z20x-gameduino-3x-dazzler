#ifndef COBRA_H
#define COBRA_H

#include "GD_Defines.h"
#include "GD.h"
#include "GD_Transport.h"
#include "GD_Reader.h"
#include "GD_SDCard.h"

void setup_cobra(GD *my_GD,
                 GD_Transport *my_GD_Transport,
                 GD_SDCard *my_GD_SDCard,
                 GD_Reader *my_GD_Reader);

void run_cobra(GD *my_GD);

#endif COBRA_H
