#include <Math.h>
#include <String.h>

#include "cobra_assets.h"
#include "GD.h"
#include "Poly.h"
#include "utils.h"

/* ************************************************************************* */

#define EDGE_BYTES      5
#define N_VERTICES      (sizeof(COBRA_vertices) / 3)

/* ************************************************************************* */

extern byte     ft8xx_model;
extern float    angle;
extern float    axis[3];
extern float    lastPos[3];
extern float    model_mat[9];
extern float    normal_mat[9];

/* ************************************************************************* */

static xyz  projected[N_VERTICES];
static byte visible_edges[EDGE_BYTES];
static byte VXSCALE                     = 16;
static byte prev_touching;

/* ************************************************************************* */

static void draw_faces(GD *my_GD)
{
    int8_t    *p  = COBRA_faces; 
    byte      n;
    int       c   = 1;
    Poly      po;

    memset(visible_edges, 0, sizeof(visible_edges));

    while ((n = pgm_read_byte_near(p++)) != 0xFF) 
    {
        int         i, d;
        int8_t      nx                      = pgm_read_byte_near(p++);
        int8_t      ny                      = pgm_read_byte_near(p++);
        int8_t      nz                      = pgm_read_byte_near(p++);
        byte        face_edges[EDGE_BYTES];
        byte        v1, v2, v3;
        long        x1, y1, x2, y2, x3, y3;
        long        area;
        uint16_t    r, g, b;

        for (i = 0; i < EDGE_BYTES; i++)
            face_edges[i] = pgm_read_byte_near(p++);

        v1 = pgm_read_byte_near(p);
        v2 = pgm_read_byte_near(p + 1);
        v3 = pgm_read_byte_near(p + 2);
        x1 = projected[v1].x;
        y1 = projected[v1].y;
        x2 = projected[v2].x;
        y2 = projected[v2].y;
        x3 = projected[v3].x;
        y3 = projected[v3].y;
        area = (x1 - x3) * (y2 - y1) - (x1 - x2) * (y3 - y1);

        if (area > 0) 
        {
            for (i = 0; i < EDGE_BYTES; i++)
                visible_edges[i] |= face_edges[i];

            Poly_begin(&po, my_GD);

            for (i = 0; i < n; i++) 
            {
                byte    vi = pgm_read_byte_near(p++);
                xyz     *v = &projected[vi];
                Poly_v(&po, v->x, v->y);
            } /* end for */

            transform_normal(&nx, &ny, &nz);

            r = 10;                             // Ambient
            g = 10;
            b = 20;

            d = -ny;                            // diffuse light from +ve Y
            if (d > 0) 
            {
                r += d >> 2;
                g += d >> 1;
                b += d;
            } /* end if */
                                                // use specular half angle
            d = (ny * -90) +( nz * -90);        // Range -16384 to +16384
            if (d > 8192) 
            {
                byte l = pgm_read_byte_near(shiny + ((d - 8192) >> 4));
                r += l;
                g += l;
                b += l;
            } /* end if */

            GD_ColorR_G_B(my_GD, min(255, r), min(255, g), min(255, b));
            Poly_draw(&po);
        } 
        else 
        {
            p += n;
        } /* end if */
        c += 1;
    } /* end while */
} /* end draw_faces */


static void draw_edges(GD *my_GD)
{
    uint8_t *p      = COBRA_edges; 
    byte    *pvis   = visible_edges;
    byte    vis     = 0;
    byte    i;
    byte    v0, v1;

    GD_ColorRGB(my_GD, 0x2E666E);
    GD_Begin(my_GD, LINES);
    GD_LineWidth(my_GD, 20);

    for (i = 0; i < sizeof(COBRA_edges) / 2; i++) 
    {
        if ((i & 7) == 0)
            vis = *pvis++;
        v0 = pgm_read_byte_near(p++);
        v1 = pgm_read_byte_near(p++);

        if (vis & 1) 
        {
            int x0 = projected[v0].x;
            int y0 = projected[v0].y;
            int x1 = projected[v1].x;
            int y1 = projected[v1].y;

            GD_Vertex2f(my_GD, x0,y0);
            GD_Vertex2f(my_GD, x1,y1);
        } /* end if */
        vis >>= 1;
    } /* end for */
} /* end draw_edges */


static void draw_navlight(GD *my_GD,
                          byte nf)
{
    float l0z = projected[N_VERTICES - 2].z;
    float l1z = projected[N_VERTICES - 1].z;
    byte i;
    
    if (nf == 0)  // draw the one with smallest z
        i = (l0z < l1z) ? (N_VERTICES - 2) : (N_VERTICES - 1);
    else
        i = (l0z < l1z) ? (N_VERTICES - 1) : (N_VERTICES - 2);

    GD_SaveContext(my_GD);
    GD_BlendFunc(my_GD, SRC_ALPHA, ONE);
    GD_Begin(my_GD, BITMAPS);
    GD_BitmapHandle(my_GD, LIGHT_HANDLE);
    
    GD_ColorRGB(my_GD, (i == N_VERTICES - 2) ? 0xFE2B18 : 0x4FFF82);
    GD_Vertex2f(my_GD,
                projected[i].x - (VXSCALE * LIGHT_WIDTH / 2),
                projected[i].y - (VXSCALE * LIGHT_WIDTH / 2));
    GD_RestoreContext(my_GD);
} /* end draw_navlight */


static void draw_sun(GD *my_GD,
                     int x, 
                     int y, 
                     int rot)
{
    GD_cmd_loadidentity(my_GD);
    GD_cmd_translate(my_GD, F16(SUN_WIDTH / 2), F16(SUN_WIDTH / 2));
    GD_cmd_rotate(my_GD, rot);
    GD_cmd_translate(my_GD, -F16(SUN_WIDTH / 2), -F16(SUN_WIDTH / 2));
    GD_cmd_setmatrix(my_GD);
    GD_Vertex2f(my_GD, x - (VXSCALE * SUN_WIDTH / 2), y - (VXSCALE * SUN_WIDTH / 2));
} /* end draw_sun */

/* ************************************************************************* */

void setup_cobra(GD *my_GD,
                 GD_Transport *my_GD_Transport,
                 GD_SDCard *my_GD_SDCard,
                 GD_Reader *my_GD_Reader)
{
    GD_begin_default_options(my_GD, my_GD_Transport, my_GD_SDCard, my_GD_Reader);

    GD_safeload(my_GD, "cobra.gd2");
    GD_BitmapHandle(my_GD, BACKGROUND_HANDLE);
    GD_BitmapSize(my_GD, BILINEAR, REPEAT, REPEAT, my_GD->w, my_GD->h);

    startMotion(my_GD, 240, 136);
    trackMotion(my_GD, 243, 139);
} /* end setup_cobra */


void run_cobra(GD *my_GD)
{
    uint16_t    t       = 0;
    int         et;
    int         sun_x;
    int         sun_y;
    byte        touching;

    while(1)
    {
        if (ft8xx_model == 2) 
        {
            GD_VertexFormat(my_GD, 3);
            my_GD->vxf = 3;
            VXSCALE = 8;
        } /* end if */

        GD_Begin(my_GD, BITMAPS);
        GD_SaveContext(my_GD);
        GD_SaveContext(my_GD);
        GD_BitmapHandle(my_GD, BACKGROUND_HANDLE);
        GD_cmd_translate(my_GD, -(long)t << 14, (long)t << 13);
        GD_cmd_rotate(my_GD, 3312);
        GD_cmd_setmatrix(my_GD);
        GD_Vertex2ii(my_GD, 0, 0, 0, 0);
        GD_RestoreContext(my_GD);

        et = t - 0;
        sun_x = (my_GD->w * VXSCALE) - (et << 2),
        sun_y = (100 * VXSCALE) + (et << 1);
        
        GD_SaveContext(my_GD);
        GD_PointSize(my_GD, 52 * 16);
        GD_ColorRGB(my_GD, 0x000000);
        GD_Begin(my_GD, POINTS);
        GD_Vertex2f(my_GD, sun_x, sun_y);
        GD_RestoreContext(my_GD);

        GD_SaveContext(my_GD);
        GD_Begin(my_GD, BITMAPS);
        GD_BlendFunc(my_GD, ONE, ONE);
        GD_BitmapHandle(my_GD, SUN_HANDLE);
        GD_ColorRGB(my_GD, 0xb0a090);
        draw_sun(my_GD, sun_x, sun_y, t << 6);
        draw_sun(my_GD, sun_x, sun_y, -t << 6);
        GD_RestoreContext(my_GD);

        GD_get_inputs(my_GD);
        touching = (my_GD->inputs.x != -32768);
        if (!prev_touching && touching)
            startMotion(my_GD, my_GD->inputs.x, my_GD->inputs.y);
        else if (touching)
            trackMotion(my_GD, my_GD->inputs.x, my_GD->inputs.y);
        prev_touching = touching;

        if (angle != 0.0f)
            rotation(angle, axis);

        project_3D(my_GD, 0, COBRA_vertices, sizeof(COBRA_vertices), projected, VXSCALE);
        draw_navlight(my_GD, 1);
        draw_faces(my_GD);
        GD_RestoreContext(my_GD);
        draw_edges(my_GD);
        draw_navlight(my_GD, 0);
        GD_RestoreContext(my_GD);

        GD_swap(my_GD);

        t++;
    } /* end while */
} /* end cobra */

/* ************************************************************************* */
