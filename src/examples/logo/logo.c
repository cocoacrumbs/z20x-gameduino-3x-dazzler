#include "logo.h"
#include "logo_assets.h"
#include "xy.h"

/* ************************************************************************* */

#define NSTARS 256

/* ************************************************************************* */

byte clamp255(int x)
{
    if (x < 0)
        return 0;
    if (255 < x)
        return 255;
    return x;
} /* end clamp255 */

/* ************************************************************************* */

void setup_logo(GD *my_GD,
                GD_Transport *my_GD_Transport,
                GD_SDCard *my_GD_SDCard,
                GD_Reader *my_GD_Reader)
{
    GD_begin_default_options(my_GD, my_GD_Transport, my_GD_SDCard, my_GD_Reader);
    GD_safeload(my_GD, "logo.gd2");
} /* end setup_logo */


void run_logo(GD *my_GD)
{
    while (1)
    {
        byte    fade;
        int     i, t;
        xy      stars[NSTARS];

        for (i = 0; i < NSTARS; i++)
            xy_set(&stars[i], GD_random_1(my_GD, PIXELS(my_GD->w)), GD_random_1(my_GD, PIXELS(my_GD->h)));

        for (t = 0; t < 464; t++) 
        {
            GD_cmd_gradient(my_GD, 0, 0, 0x120000, 0, my_GD->h, 0x480000);
            GD_BlendFunc(my_GD, SRC_ALPHA, ONE);
            GD_Begin(my_GD, POINTS);
            for (i = 0; i < NSTARS; i++) 
            {
                GD_ColorA(my_GD, 64 + (i >> 2));
                GD_PointSize(my_GD, 8 + (i >> 5));
                GD_Vertex2f(my_GD, stars[i].x - PIXELS(0), stars[i].y - PIXELS(0));

                // stars drift left, then wrap around
                stars[i].x -= 1 + (i >> 5);
                if (stars[i].x < -256) 
                {
                    stars[i].x += PIXELS(my_GD->w + 20);
                    stars[i].y = GD_random_1(my_GD, PIXELS(my_GD->h));
                } /* end if */
            } /* end for */
            GD_RestoreContext(my_GD);
            GD_Begin(my_GD, BITMAPS);

            // Main logo fades up from black
            fade = clamp255(5 * (t - 15));
            GD_ColorR_G_B(my_GD, fade, fade, fade);
            GD_Vertex2ii(my_GD, 240 - GAMEDUINO_WIDTH/2, 65, GAMEDUINO_HANDLE, 0);
            GD_RestoreContext(my_GD);

            // The '2' and its glow
            fade = clamp255(8 * (t - 120));
            GD_ColorA(my_GD, fade);
            GD_Vertex2ii(my_GD, 270, 115, TWO_HANDLE, 0);
            fade = clamp255(5 * (t - 144));

            GD_BlendFunc(my_GD, SRC_ALPHA, ONE);
            GD_ColorA(my_GD, fade);
            GD_ColorR_G_B(my_GD, 85,85,85);
            GD_Vertex2ii(my_GD, 270, 115, TWO_HANDLE, 1);

            GD_RestoreContext(my_GD);

            // The text fades up. Its glow is a full-screen bitmap
            fade = clamp255(8 * (t - 160));
            GD_ColorA(my_GD, fade);
            GD_cmd_text(my_GD, 140, 200, 29, OPT_CENTER, "This time");
            GD_cmd_text(my_GD, 140, 225, 29, OPT_CENTER, "it's personal");
            fade = clamp255(5 * (t - 184));
            GD_BlendFunc(my_GD, SRC_ALPHA, ONE);
            GD_ColorA(my_GD, fade);
            GD_ColorR_G_B(my_GD, 85,85,85);
            GD_Vertex2ii(my_GD, 0, 0, PERSONAL_HANDLE, 0);

            // OSHW logo fades in
            GD_ColorR_G_B(my_GD, 0, 153 * 160 / 255, 176 * 160 / 255);
            GD_Vertex2ii(my_GD, 2, 2, OSHW_HANDLE, 0);
            GD_RestoreContext(my_GD);

            // Fade to white at the end by drawing a white rectangle on top
            fade = clamp255(5 * (t - 400));
            GD_ColorA(my_GD, fade);
            GD_Begin(my_GD, EDGE_STRIP_R);
            GD_Vertex2f(my_GD, 0, 0);
            GD_Vertex2f(my_GD, 0, PIXELS(my_GD->h));

            GD_swap(my_GD);
        } /* end for */
    } /* end while */
} /* end run_logo */
