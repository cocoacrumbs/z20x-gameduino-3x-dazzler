#include "movie_player.h"

// uint32_t f0;
// int qq;

/* ************************************************************************* */

int MP_begin(movie_player *self,
             const char *filename) 
{
    __GD_end(self->my_GD);

    if (!Reader_openfile(self->my_GD_Reader, filename))
    {
      // printf("Open failed\n");
      return 0;
    } /* end if */
    GD_resume(self->my_GD);

    // uint32_t t0 = millis();
    self->wp = 0;
    while (self->wp < 0xFE00U)
    {
        MP_loadsector(self);
    } /* end while */
    //     uint32_t took = (millis() - t0);
    // Serial.println(took);
    // Serial.print(1000L * wp / took);
    // Serial.println(" bytes/s");

    GD_cmd_mediafifo(self->my_GD, 0x0F0000UL, 0x10000UL);
    GD_cmd_regwrite(self->my_GD, REG_MEDIAFIFO_WRITE, self->wp);
    GD_finish(self->my_GD);

    if (0) 
    {
        GD_cmd_playvideo(self->my_GD, OPT_MEDIAFIFO);
    } 
    else 
    {
        GD_cmd_videostart(self->my_GD);
    } /* end if */

    // f0 = millis();
    return 1;
} /* end MP_begin */


void MP_init(movie_player *self,
             GD *my_GD,
             GD_SDCard *my_GD_SDCard,
             GD_Reader *my_GD_Reader)
{
    self->my_GD        = my_GD;
    self->my_GD_SDCard = my_GD_SDCard;
    self->my_GD_Reader = my_GD_Reader;
} /* end MP_init */


void MP_loadsector(movie_player *self) 
{
    byte buf[512];

    __GD_end(self->my_GD);
    Reader_readsector(self->my_GD_Reader, self->my_GD_SDCard, buf);
    GD_resume(self->my_GD);
    GD_wr_n(self->my_GD, 0x0F0000UL + self->wp, buf, 512);
    self->wp += 512;
} /* end MP_loadsector */


int MP_service(movie_player *self) 
{
    if (Reader_eof(self->my_GD_Reader))
    {
        return 0;
    } 
    else 
    {
        byte        buf[512];
        uint16_t    fullness    = self->wp - GD_rd16(self->my_GD, REG_MEDIAFIFO_READ);

        self->qq = 0;
        while (fullness < 0xFE00U) 
        {
            MP_loadsector(self);
            fullness += 512;
            self->qq += 512;
        } /* end while */
        GD_wr16(self->my_GD, REG_MEDIAFIFO_WRITE, self->wp);
        return 1;
    } /* end if */
} /* end MP_service */

/* ************************************************************************* */
