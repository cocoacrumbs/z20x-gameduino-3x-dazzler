#ifndef MOVIE_PLAYER_H
#define MOVIE_PLAYER_H

#include "GD.h"
#include "GD_SDCard.h"
#include "GD_Reader.h"

/* ************************************************************************* */

typedef struct movie_player_T
{
    GD          *my_GD;
    GD_SDCard   *my_GD_SDCard;
    GD_Reader   *my_GD_Reader;
    
    uint16_t    wp;
    int         qq;
} movie_player;

/* ************************************************************************* */

int MP_begin(movie_player *self,
             const char *filename);

void MP_init(movie_player *self,
             GD *my_GD,
             GD_SDCard *my_GD_SDCard,
             GD_Reader *my_GD_Reader);

void MP_loadsector(movie_player *self);

int MP_service(movie_player *self);

/* ************************************************************************* */

#endif MOVIE_PLAYER_H
