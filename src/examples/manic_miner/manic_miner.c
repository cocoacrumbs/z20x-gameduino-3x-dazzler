#include <stdlib.h>
#include <String.h>

#include "manic_miner.h"

/* ************************************************************************* */

extern byte ft8xx_model;

/* ************************************************************************* */

#define dazzler         1
#define TITLE_FRAMES    900         // How many frames to show the title screen
#define MAXRAY          40

// The basic colors
#define BLACK   RGB(0,   0,   0)
#define RED     RGB(255, 0,   0)
#define GREEN   RGB(0,   255, 0)
#define YELLOW  RGB(255, 255, 0)
#define BLUE    RGB(0,   0,   255)
#define MAGENTA RGB(255, 0,   255)
#define CYAN    RGB(0,   255, 255)
#define WHITE   RGB(255, 255, 255)

// Game has three controls: LEFT, RIGHT, JUMP.  You can map them
// to any pins by changing the definitions of PIN_L, PIN_R, PIN_J
// below.
#define CONTROL_LEFT        1
#define CONTROL_RIGHT       2
#define CONTROL_JUMP        4

// The map's blocks have fixed meanings:
#define ELEM_AIR            0
#define ELEM_FLOOR          1
#define ELEM_CRUMBLE        2
#define ELEM_WALL           3
#define ELEM_CONVEYOR       4
#define ELEM_NASTY1         5
#define ELEM_NASTY2         6

#define CHEAT_INVINCIBLE    0   // means Willy unaffected by nasties
#define START_LEVEL         0   // level to start on, 0-18
#define CHEAT_OPEN_PORTAL   0   // Portal always open

#define JUMP_APEX           9   
#define JUMP_FALL           11

/* ************************************************************************* */

struct state_t 
{
    byte            level;
    byte            lives;
    byte            alive;
    byte            t;
    uint32_t        score;
    uint32_t        hiscore;

    byte            bidir;
    byte            air;
    byte            conveyordir;
    byte            conveyor0[16];
    byte            conveyor1[16];
    byte            portalattr;
    byte            nitems;
    byte            items[5];
    byte            wx, wy;     // Willy x,y
    byte            wd, wf;     // Willy dir and frame
    byte            lastdx;     // Willy last x movement
    byte            convey;     // Willy caught on conveyor
    byte            jumping;
    signed char     wyv;
    byte            conveyor[2];
    unsigned char   *guardian;
    uint16_t        prevray[MAXRAY];
    byte            switch1;
    byte            switch2;
} state;


typedef struct guardian_T 
{
    byte        a;
    byte        x, y;
    signed char d;
    byte        x0, x1;
    byte        f;
} guardian;

/* ************************************************************************* */

typedef struct level_T
{
    char        name[33];
    uint32_t    border;
    struct 
    { 
        byte    x;
        byte    y;
    } items[5];
    byte        air;
    byte        conveyordir;
    byte        portalx, portaly;
    struct 
    {
        byte a, x, y, d, x0, x1; 
    } hguard[8];
    byte        wx, wy, wd, wf;
    byte        bidir;
} level; /* end level */

#include "manic_miner_assets.h"

/* ************************************************************************* */

static int      SF          = 3;
static int      X0          = 112;
static int      Y0          = 32;
static int      plain_font;
static int      OVER_FRAMES = 1;    // 5

static guardian guards[8];

static char marquee[] =
    "MANIC MINER . . "
    "(C) BUG-BYTE ltd. 1983 . . "
    "By Matthew Smith . . "
    "Gameduino2 conversion by James Bowman . . "
    "Guide Miner Willy through 19 lethal caverns";

/* ************************************************************************* */

int control(GD *my_GD) 
{
    int r = 0;

    if (!dazzler) 
    {
        r = my_GD->inputs.tag;
    } 
    else 
    {
        uint16_t    b   = ~(my_GD->inputs.wii[0].buttons);
        if (b & WII_LEFT)
            r |= CONTROL_LEFT;
        if (b & WII_RIGHT)
            r |= CONTROL_RIGHT;
        if (b & WII_A)
            r |= CONTROL_JUMP;
    } /* end if */
    return r;
} /* end control */


// Convert a spectrum attribute byte to a 24-bit RGB
static uint32_t attr(byte a)
{
    // bit 6 indicates bright version
    byte l = (a & 64) ? 0xFF : 0xAA;
    return RGB( ((a & 2) ? l : 0),
                ((a & 4) ? l : 0),
                ((a & 1) ? l : 0) );
} /* end attr */


static void screen2ii(GD *my_GD,
                      byte x, 
                      byte y, 
                      byte handle /* = 0 */, 
                      byte cell /* = 0 */)
{
    GD_BitmapHandle(my_GD, handle);
    GD_Cell(my_GD, cell);
    GD_Vertex2f(my_GD, SF * 8 * x, SF * 8 * y);
} /* end screen2ii */


static void screen(int *x, 
                   int *y)
{
    *x *= SF;
    *y *= SF;
} /* end screen */


static void toscreen(int *x, 
                     int *y, 
                     int ix, 
                     int iy)
{
    *x = ix;
    *y = iy;
    screen(x, y);
} /* end toscreen */


static void screenvx(GD *my_GD,
                     int x, 
                     int y)
{
    screen(&x, &y);
    GD_Vertex2f(my_GD, 8 * x, 8 * y);
} /* end screenvx */


static void qvertex(GD *my_GD,
                    int x, 
                    int y, 
                    byte handle, /* = 0 */ 
                    byte cell    /* = 0 */)
{
    screen(&x, &y);

    if ((x & ~511) | (y & ~511)) 
    {
        GD_Cell(my_GD, cell);
        GD_Vertex2f(my_GD, 8 * x, 8 * y);
    }
    else 
    {
        GD_Vertex2ii(my_GD, x, y, handle, cell);
    } /* end if */
} /* end qvertex */


static void bump_score(byte n)
{
    uint32_t  old_score = state.score;
    
    state.score += n;

    // Extra life on crossing 10K
    if ((old_score < 10000) && (10000 <= state.score))
        state.lives++;
} /* end bump_score */


static void load_assets(GD *my_GD)
{
    // LOAD_ASSETS();
    GD_copy(my_GD, __assets, sizeof(__assets));

    GD_BitmapHandle(my_GD, TITLE_HANDLE);
    GD_BitmapSize(my_GD, NEAREST, BORDER, BORDER, SF * TITLE_WIDTH, SF * TITLE_HEIGHT);
    GD_BitmapHandle(my_GD, WILLY_HANDLE);
    GD_BitmapSize(my_GD, NEAREST, BORDER, BORDER, SF * WILLY_WIDTH, SF * WILLY_HEIGHT);
    GD_BitmapHandle(my_GD, GUARDIANS_HANDLE);
    GD_BitmapSize(my_GD, NEAREST, BORDER, BORDER, SF * GUARDIANS_WIDTH, SF * GUARDIANS_HEIGHT);
    GD_BitmapHandle(my_GD, PORTALS_HANDLE);
    GD_BitmapSize(my_GD, NEAREST, BORDER, BORDER, SF * PORTALS_WIDTH, SF * PORTALS_HEIGHT);
    GD_BitmapHandle(my_GD, ITEMS_HANDLE);
    GD_BitmapSize(my_GD, NEAREST, BORDER, BORDER, SF * ITEMS_WIDTH, SF * ITEMS_HEIGHT);
    GD_BitmapHandle(my_GD, SPECIALS_HANDLE);
    GD_BitmapSize(my_GD, NEAREST, BORDER, BORDER, SF * SPECIALS_WIDTH, SF * SPECIALS_HEIGHT);
} /* end load_assets */

static void load_level(GD *my_GD)
{
    byte        i;
    level      *l               = &levels[state.level];
    uint32_t    level_chars;
    guardian   *pg;

    // the items are 5 sprites, using colors 16 up
    state.nitems = 0;
    for (i = 0; i < 5; i++) 
    {
        byte x      = pgm_read_byte_near(&l->items[i].x);
        byte y      = pgm_read_byte_near(&l->items[i].y);
        byte isitem = (x || y);
        state.items[i]  = isitem;
        state.nitems   += isitem;
    } /* end for */

    state.air = pgm_read_byte_near(&l->air);

    // Conveyor direction
    state.conveyordir = pgm_read_byte_near(&l->conveyordir);

    level_chars = TILES_MEM + 15 * 64 * state.level;

    GD_rd_n(my_GD, state.conveyor0, level_chars + 4 * 64, 8);
    memcpy(state.conveyor0 + 8, state.conveyor0, 8);

    GD_rd_n(my_GD, state.conveyor1, level_chars + 4 * 64 + 3 * 8 , 8);
    memcpy(state.conveyor1 + 8, state.conveyor1, 8);

    // the hguardians
    state.bidir = pgm_read_byte_near(&l->bidir);
    for (i = 0; i < 8; i++) 
    {
        byte a = pgm_read_byte_near(&l->hguard[i].a);

        guards[i].a = a;
        if (a) 
        {
            byte x = pgm_read_byte_near(&l->hguard[i].x);
            byte y = pgm_read_byte_near(&l->hguard[i].y);

            guards[i].x  = x;
            guards[i].y  = y;
            guards[i].d  = pgm_read_byte_near(&l->hguard[i].d);
            guards[i].x0 = pgm_read_byte_near(&l->hguard[i].x0);
            guards[i].x1 = pgm_read_byte_near(&l->hguard[i].x1);
            guards[i].f  = 0;
        } /* end if */
    } /* end for */

    pg = &guards[4];  // Use slot 4 for special guardians
    switch (state.level) 
    { 
        // Special level handling
        case 4:     // Eugene's lair
            pg->a  = 0;  // prevent normal guardian logic
            pg->x  = 120;
            pg->y  = 0;
            pg->d  = -1;
            pg->x0 = 0;
            pg->x1 = 88;
            // loadspr16(IMG_GUARD + 4, eugene, 255, 15);
            break;
        case 7:     // Miner Willy meets the Kong Beast
        case 11:    // Return of the Alien Kong Beast
            pg->a = 0;
            pg->x = 120;
            pg->y = 0;
            state.switch1 = 0;
            // loadspr8(IMG_SWITCH1, lightswitch, 0, 6);
            // sprite(IMG_SWITCH1, 48, 0, IMG_SWITCH1);
            state.switch2 = 0;
            // loadspr8(IMG_SWITCH2, lightswitch, 0, 6);
            // sprite(IMG_SWITCH2, 144, 0, IMG_SWITCH2);
            break;
        default:
            break;
    } /* end switch */

    for (i = 0; i < MAXRAY; i++)
        state.prevray[i] = 4095;

    // Willy
    state.wx = pgm_read_byte_near(&l->wx);
    state.wy = pgm_read_byte_near(&l->wy);
    state.wf = pgm_read_byte_near(&l->wf);
    state.wd = pgm_read_byte_near(&l->wd);
    state.jumping = 0;
    state.convey  = 0;
    state.wyv     = 0;
} /* end load_level */


static uint32_t level_base()
{
    return MANICMINER_ASSET_MAPS + (state.level << 9);
} /* end level_base */


static void draw_start(GD *my_GD)
{
    GD_VertexTranslateX(my_GD, 16 * X0);
    GD_VertexTranslateY(my_GD, 16 * Y0);
    GD_VertexFormat(my_GD, 3);

    GD_Begin(my_GD, BITMAPS);
    GD_cmd_loadidentity(my_GD);
    GD_cmd_scale(my_GD, F16(SF), F16(SF));
    GD_cmd_setmatrix(my_GD);
} /* end draw_start */


static void draw_level(GD *my_GD)
{
    level      *l           = &levels[state.level];
    uint32_t    pmap;
    byte        i;
    byte        y;
    byte        portalx;
    byte        portaly;
    uint32_t    colors[4]   = { MAGENTA, YELLOW, CYAN, GREEN };

    GD_ClearColorRGB(my_GD, pgm_read_dword(&(l->border)));
    GD_Clear(my_GD);
    GD_Tag(my_GD, CONTROL_JUMP);

    GD_BitmapHandle(my_GD, TILES_HANDLE);
    GD_BitmapSource(my_GD, TILES_MEM + 15 * 64 * state.level);
    GD_BitmapSize(my_GD, NEAREST, REPEAT, REPEAT, SF * 256, SF * 128);
    qvertex(my_GD, 0, 0, TILES_HANDLE, 0);
    GD_BitmapSize(my_GD, NEAREST, BORDER, BORDER, SF * 16, SF * 16);

    pmap = level_base();
    for (y = 0; y < 16; y++) 
    {
        byte x;
        byte line[32];

        GD_rd_n(my_GD, line, pmap, 32);
        pmap += 32;

        for (x = 0; x < 32; x++)
            if (line[x])
                qvertex(my_GD, 8 * x, 8 * y, TILES_HANDLE, line[x]);
    } /* end for */

    // the portal is a sprite
    portalx = pgm_read_byte_near(&l->portalx);
    portaly = pgm_read_byte_near(&l->portaly);
    
    // flash it when it is open
    if (CHEAT_OPEN_PORTAL || state.nitems == 0) 
    {
        byte    lum     = state.t << 5;

        GD_ColorR_G_B(my_GD, lum, lum, lum);
    } /* end if */
    screen2ii(my_GD, portalx, portaly, PORTALS_HANDLE, state.level);

    // the items are 5 sprites
    for (i = 0; i < 5; i++) 
        if (state.items[i]) 
        {
            byte x = pgm_read_byte_near(&l->items[i].x);
            byte y = pgm_read_byte_near(&l->items[i].y);

            GD_ColorRGB(my_GD, colors[(i + state.t) & 3]);
            screen2ii(my_GD, x, y, 4, state.level);
        } /* end if */
} /* end draw_level */


static void draw_status(GD *my_GD,
                        byte playing)
{
    level   *l  = &levels[state.level];
    int     x   = 0;
    int     y   = 128;

    GD_Begin(my_GD, RECTS);
    GD_ColorRGB(my_GD, BLACK);
    screenvx(my_GD, 0, 128);
    screenvx(my_GD, 256, 192);

    GD_ColorRGB(my_GD, 0xAAAA00);
    screenvx(my_GD, 0, 128);
    screenvx(my_GD, 256, 128 + 15);

    GD_ColorRGB(my_GD, 0xA00000);
    screenvx(my_GD, 0, 128 + 16);
    screenvx(my_GD, 64, 128 + 30);
    
    GD_ColorRGB(my_GD, 0x00A000);
    screenvx(my_GD, 64, 128 + 16);
    screenvx(my_GD, 256, 128 + 30);

    GD_SaveContext(my_GD);
    GD_cmd_loadidentity(my_GD);
    GD_cmd_setmatrix(my_GD);

    if (playing) 
    {
        char nm[33];

        GD_ColorRGB(my_GD, BLACK);
        strcpy(nm, l->name);
        toscreen(&x, &y, 128, 128 + 7);
        GD_cmd_text(my_GD, x, y, plain_font, OPT_CENTER, nm);

        GD_ColorRGB(my_GD, WHITE);
        toscreen(&x, &y, 26, 128 + 23);
        GD_cmd_text(my_GD, x, y, plain_font, OPT_CENTERY | OPT_RIGHTX, "AIR");

        x = 30 + state.air;
        y = 128 + 23;

        GD_Begin(my_GD, LINES);
        GD_LineWidth(my_GD, 64);
        GD_ColorRGB(my_GD, BLACK);
        GD_ColorA(my_GD, 128);
        screenvx(my_GD, 30, y);
        screenvx(my_GD, x, y);

        GD_LineWidth(my_GD, 32);
        GD_ColorRGB(my_GD, WHITE);
        GD_ColorA(my_GD, 255);
        screenvx(my_GD, 30, y);
        screenvx(my_GD, x, y);
    } /* end if */

    GD_ColorRGB(my_GD, YELLOW);

    toscreen(&x, &y, 0, 128 + 40);
    GD_cmd_text(my_GD, x, y, plain_font, OPT_CENTERY, "High Score");

    toscreen(&x, &y, 165, 128 + 40);
    GD_cmd_text(my_GD, x, y, plain_font, OPT_CENTERY, "Score");

    toscreen(&x, &y, 75, 128 + 40);
    GD_cmd_number(my_GD, x, y, plain_font, OPT_CENTERY | 6, state.hiscore);

    toscreen(&x, &y, 255, 128 + 40);
    GD_cmd_number(my_GD, x, y, plain_font, OPT_RIGHTX | OPT_CENTERY | 6, state.score);

    GD_RestoreContext(my_GD);

    if (playing) 
    {
        int i;

        GD_ColorRGB(my_GD, CYAN);
        GD_Begin(my_GD, BITMAPS);

        for (i = 0; i < (state.lives - 1); i++) 
        {
            screen2ii(my_GD, 2 + i * 16, 192 - 16, WILLY_HANDLE, 3 & (state.t >> 2));
        } /* end for */
    } /* end if */
} /* end draw_status */


static int qsin(byte r, byte a)
{
    byte t;

    switch (a & 3) 
    {
        default:
        case 0:
            t = 0;
            break;
        case 1:
        case 3:
            t = (45 * r) >> 6;
            break;
        case 2:
            t = r;
    } /* end switch */
    return (a & 4) ? -t : t;
} /* end qsin */


static void polar(GD *my_GD,
                  int x, 
                  int y, 
                  byte r, 
                  byte a)
{
    GD_Vertex2ii(my_GD, x + qsin(r, a), y + qsin(r, a + 2), 0, 0);
} /* end polar */


static void draw_button(GD *my_GD,
                        int x, 
                        int y, 
                        byte angle)
{
    int r   = 30;

    GD_PointSize(my_GD, 42 * 16);
    GD_ColorRGB(my_GD, 0x808080);
    GD_Begin(my_GD, POINTS);
    GD_Vertex2ii(my_GD, x, y, 0, 0);
    GD_ColorRGB(my_GD, WHITE);
    GD_LineWidth(my_GD, 6 * 16);
    GD_Begin(my_GD, LINES);
    polar(my_GD, x, y, r, angle + 2);
    polar(my_GD, x, y, r, angle + 6);
    GD_Begin(my_GD, LINE_STRIP);
    polar(my_GD, x, y, r, angle + 0);
    polar(my_GD, x, y, r, angle + 2);
    polar(my_GD, x, y, r, angle + 4);
} /* end draw_button */


static void draw_controls(GD *my_GD)
{
    if (dazzler)
        return;

    GD_Tag(my_GD, CONTROL_LEFT);
    draw_button(my_GD,       45, 272 - 45,  4);

    GD_Tag(my_GD, CONTROL_LEFT | CONTROL_JUMP);
    draw_button(my_GD,       45, 272 - 140, 3);

    GD_Tag(my_GD, CONTROL_RIGHT);
    draw_button(my_GD, 480 - 45, 272 - 45,  0);

    GD_Tag(my_GD, CONTROL_RIGHT | CONTROL_JUMP);
    draw_button(my_GD, 480 - 45, 272 - 140, 1);
} /* end draw_controls */


static void draw_willy(GD *my_GD)
{
    byte frame = state.wf ^ (state.wd ? 7 : 0);

    GD_ColorRGB(my_GD, WHITE);
    GD_Begin(my_GD, BITMAPS);
    screen2ii(my_GD, state.wx, state.wy, WILLY_HANDLE, frame);
} /* end draw_willy */


static void draw_guardians(GD *my_GD)
{
    guardian   *pg;
    byte        i;

    GD_BitmapHandle(my_GD, GUARDIANS_HANDLE);
    GD_BitmapSource(my_GD, GUARDIANS_MEM + 8 * 32 * state.level);

    for (i = 0; i < 8; i++) 
    {
        pg = &guards[i];
        
        if (pg->a) 
        {
            GD_ColorRGB(my_GD, attr(pg->a));
            screen2ii(my_GD, pg->x, pg->y, GUARDIANS_HANDLE, pg->f);
        } /* end if */
    } /* end for */

    pg = &guards[4];
    switch (state.level) 
    { 
        // Special level handling
        case 4: // Eugene's lair
            GD_ColorRGB(my_GD, WHITE);
            screen2ii(my_GD, pg->x, pg->y, 6, 0);
        default:
            break;
    } /* end switch */
} /* end draw_guardians */


static void move_guardians(GD *my_GD)
{
    guardian   *pg;
    byte        i;
    byte        lt;     // local time, for slow hguardians
    byte        frame;
    byte        color;

    for (i = 0; i < 8; i++) 
    {
        pg = &guards[i];
        if (pg->a) 
        {
            byte    vertical    = (i >= 4);
            byte    frame;

            switch (state.level) 
            {
                case 13:                    // Skylabs
                    if (pg->y != pg->x1) 
                    {
                        pg->f = 0;
                        pg->y += pg->d;
                    } 
                    else 
                    {
                        pg->f++;
                    } /* end if */
                    if (pg->f == 8) 
                    {
                        pg->f = 0;
                        pg->x += 64;
                        pg->y = pg->x0;
                    } /* end if */

                    // Color is pg->a
                    screen2ii(my_GD, pg->x, pg->y, 1, pg->f);
                    break;
                default:
                    lt = state.t;
                    if (!vertical && (pg->a & 0x80)) 
                    {
                        if (state.t & 1)
                            lt = state.t >> 1;
                        else
                            break;
                    } /* end if */

                    if (!vertical) 
                    {
                        if ((lt & 3) == 0) 
                        {
                            if (pg->x == pg->x0 && pg->d)
                                pg->d = 0;
                            else if (pg->x == pg->x1 && !pg->d)
                                pg->d = 1;
                            else
                                pg->x += pg->d ? -8 : 8;
                        }
                    } 
                    else 
                    {
                        if (pg->y <= pg->x0 && pg->d < 0)
                            pg->d = -pg->d;
                        else if (pg->y >= pg->x1 && pg->d > 0)
                            pg->d = -pg->d;
                        else
                            pg->y += pg->d;
                    } /* end if */

                    if (state.bidir)
                        frame = (lt & 3) ^ (pg->d ? 7 : 0);
                    else
                        if (vertical)
                            frame = lt & 3;
                        else
                            frame = 4 ^ (lt & 3) ^ (pg->d ? 3 : 0);
                    pg->f = frame;

                    break;
            } /* end switch */
        } /* end if */
    } /* end for */

    pg = &guards[4];
    switch (state.level) 
    { 
        // Special level handling
        case 4: // Eugene's lair
            // sprite(IMG_GUARD + 4, pg->x, pg->y, IMG_GUARD + 4);
            if (pg->y == pg->x0 && pg->d < 0)
                pg->d = 1;
            if (pg->y == pg->x1 && pg->d > 0)
                pg->d = -1;
            if (state.nitems == 0) 
            {  
                // all collected -> descend and camp
                if (pg->d == -1)
                    pg->d = 1;
                if (pg->y == pg->x1)
                    pg->d = 0;
            } /* end if */
            pg->y += pg->d;
            break;
        case 7: 
            // Miner Willy meets the Kong Beast
        case 11: 
            //  Return of the Alien Kong Beast
            if (!state.switch2) 
            {
                frame = (state.t >> 3) & 1;
                color = 8 + 4;
            } 
            else 
            {
                frame = 2 + ((state.t >> 1) & 1);
                color = 8 + 6;
                if (pg->y < 104) 
                {
                    pg->y += 4;
                    bump_score(100);
                } /* end if */
            } /* end if */
            break;
    } /* end switch */
} /* end move_guardians */


static uint32_t atxy(byte x, 
                     byte y)
{
    return level_base() + (y << 5) + x;
} /* end atxy */


static void rd2(GD *my_GD,
                byte *a, 
                byte *b, 
                uint32_t addr)
{
    byte u[2];

    GD_rd_n(my_GD, u, addr, 2);
    *a = u[0];
    *b = u[1];
} /* end rd2 */


// crumble block at s, which is the sequence
// 2 -> 8 -> 9 -> 10 -> 11 -> 12 -> 13 -> 14 -> AIR
static void crumble(GD *my_GD,
                    uint32_t s)
{
    byte        r   = GD_rd(my_GD, s);
    signed char nexts[] = 
    {
        -1,  -1,  8, -1, -1, -1, -1, -1,
         9,  10, 11, 12, 13, 14, ELEM_AIR 
    };
    
    if (r < sizeof(nexts) && nexts[r] != -1)
        GD_wr(my_GD, s, nexts[r]);
} /* end crumble */


static byte any(byte code, 
                byte a, 
                byte b, 
                byte c, 
                byte d)
{
    return ((a == code) ||
            (b == code) ||
            (c == code) ||
            (d == code));
} /* end any */


// is it legal for willy to be at (x,y)
static int canbe(GD *my_GD,
                 byte x, 
                 byte y)
{
    uint32_t    addr        = atxy(x / 8, y / 8);
    byte        a, b, c, d;

    rd2(my_GD, &a, &b, addr);
    rd2(my_GD, &c, &d, addr + 32);

    return !any(ELEM_WALL, a, b, c, d);
} /* end canbe */


// is Willy standing in a solar ray?
static int inray(GD *my_GD,
                 byte x, 
                 byte y)
{
    uint32_t addr = atxy(x / 8, y / 8);
    byte a, b, c, d;

    rd2(my_GD, &a, &b, addr);
    rd2(my_GD, &c, &d, addr + 32);
    return (a > 0x80 ) ||
           (b > 0x80) ||
           (c > 0x80) ||
           (d > 0x80);
} /* end inray */


static void under_willy(GD *my_GD,
                        byte *a, 
                        byte *b, 
                        byte *c, 
                        byte *d)
{
    uint32_t addr = atxy(state.wx / 8, state.wy / 8);

    rd2(my_GD, a, b, addr);
    rd2(my_GD, c, d, addr + 32);
} /* end under_willy */


static byte collide_16x16(byte x, 
                          byte y)
{
    return (abs(state.wx - x) < 7) && (abs(state.wy - y) < 15);
} /* end collide_16x16 */


static void move_all(GD *my_GD)
{
    byte        conveyor_offset;
    uint32_t    level_chars;
    byte        con;
    byte        ychanged;
    uint32_t    feet_addr;
    byte        elems[2];
    byte        elem;
    byte        dx;
    byte        first_jump;
    byte        onground;
    byte        a, b, c, d;
    byte        wx, wy;
    guardian   *pg;
    level      *l                   = &levels[state.level];
    byte        portalx, portaly;
    byte        i;

    state.t++;
    move_guardians(my_GD);

    // Animate conveyors: 
    conveyor_offset = (7 & state.t) ^ (state.conveyordir ? 0 : 7);
    level_chars = TILES_MEM + 15 * 64 * state.level;

    GD_cmd_memwrite(my_GD, level_chars + 4 * 64, 8);
    GD_copyram(my_GD, state.conveyor0 + conveyor_offset, 8);
    GD_cmd_memwrite(my_GD, level_chars + 4 * 64 + 3 * 8, 8);
    GD_copyram(my_GD, state.conveyor1 + (7 ^ conveyor_offset), 8);

    // Willy movement
    // See http://www.seasip.demon.co.uk/Jsw/manic.mac
    // and http://jetsetwilly.jodi.org/poke.html

    con = control(my_GD);
    ychanged = 0;   // if Y block changes must check for landing later
    if (state.jumping) 
    {
        signed char moves[] = {  -4, -4, -3, -3, -2, -2, -1, -1, 0, 0, 1, 1, 2, 2, 3, 3, 4 };
        byte        index   = min(sizeof(moves) - 1, state.jumping - 1);
        byte        newy    = state.wy + moves[index];

        state.jumping++;
        if (canbe(my_GD, state.wx, newy)) 
        {
            ychanged = (state.wy >> 3) != (newy >> 3);
            state.wy = newy;
        } 
        else 
        {
            state.jumping = max(state.jumping, JUMP_FALL);   // halt ascent
            ychanged = 1;
        } /* end if */
    } /* end if */

    feet_addr = atxy(state.wx >> 3, (state.wy + 16) >> 3);
    GD_rd_n(my_GD, elems, feet_addr, 2);
    elem = ((1 <= elems[0]) && (elems[0] <= 31)) ? elems[0] : elems[1];

    dx = 0xFF;
    first_jump = (con & CONTROL_JUMP) && state.lastdx == 0xFF;
    if (state.jumping) 
    {
        dx = state.lastdx;
    } 
    else if (!first_jump && (elem == ELEM_CONVEYOR) && (state.wd == state.conveyordir)) 
    {
        dx = state.conveyordir;
    } 
    else 
    {
        if (con & CONTROL_RIGHT) 
        {
            if (state.wd != 0) 
            {
                state.wf ^= 3;
                state.wd = 0;
            } 
            else 
            {
                dx = 0;
            } /* end if */
        } 
        else if (con & CONTROL_LEFT) 
        {
            if (state.wd == 0) 
            {
                state.wf ^= 3;
                state.wd = 1;
            } 
            else 
            {
                dx = 1;
            } /* end if */
        } /* end if */
    } /* end if */

    if (dx != 0xFF) 
    {
        if (state.wf != 3)
            state.wf++;
        else 
        {
            byte newx = state.wx + (dx ? -8 : 8);
            if (canbe(my_GD, newx, state.wy)) 
            {
                state.wf = 0;
                state.wx = newx;
            } /* end if */
        } /* end if */
    } /* end if */
    state.lastdx = dx;

    if ((elem == ELEM_CONVEYOR) && (dx == 0xFF) && !state.jumping)
        state.wd = state.conveyordir;

    if (!state.jumping && (con & CONTROL_JUMP)) 
    {
        if (canbe(my_GD, state.wx, state.wy - 3)) 
        {
            state.jumping = 1;
            state.wy -= 0;
        } /* end if */
    } /* end if */

    onground = ((1 <= elem) && (elem <= 4)) || ((7 <= elem) && (elem <= 16));
    if (state.jumping) 
    {
        if ((JUMP_APEX <= state.jumping) && ychanged && onground) 
        {
            state.jumping = 0;
            state.wy &= ~7;
        } /* end if */
        if (state.jumping >= 19)    // stop x movement late in the jump
            state.lastdx = 0xff;
    } 
    else 
    {
        if (!onground) 
        {    // nothing to stand on, start falling
            state.jumping = JUMP_FALL;
            state.lastdx = 0xff;
        } /* end if */
    } /* end if */
    if (!state.jumping) 
    {
        crumble(my_GD, feet_addr);
        crumble(my_GD, feet_addr + 1);
    } /* end if */

    if (((state.t & 7) == 0) ||
        ((state.level == 18) && inray(my_GD, state.wx, state.wy))) 
    {
        state.air--;
        if (state.air == 0) 
        {
            state.alive = 0;
        } /* end if */
    } /* end if */

    under_willy(my_GD, &a, &b, &c, &d);
    if (any(ELEM_NASTY1, a, b, c, d) | any(ELEM_NASTY2, a, b, c, d))
        state.alive = 0;

    for (i = 0; i < 8; i++) 
    {
        pg = &guards[i];
        if (pg->a && collide_16x16(pg->x, pg->y))
            state.alive = 0;
    } /* end if */

    state.alive |= CHEAT_INVINCIBLE;

    wx = state.wx / 8;
    wy = state.wy / 8;

    for (i = 0; i < 5; i++) 
        if (state.items[i]) 
        {
            byte x = pgm_read_byte_near(&l->items[i].x) / 8;
            byte y = pgm_read_byte_near(&l->items[i].y) / 8;
            if (((x == wx) || (x == (wx + 1))) &&
                ((y == wy) || (y == (wy + 1)))) 
            {
                bump_score(100);
                state.items[i] = 0;
                --state.nitems;
            } /* end if */
        } /* end if */

    portalx = pgm_read_byte_near(&l->portalx);
    portaly = pgm_read_byte_near(&l->portaly);

    if (((CHEAT_OPEN_PORTAL || state.nitems == 0)) &&
        collide_16x16(portalx, portaly)) 
    {
        while (state.air) 
        {
        // squarewave(0, 800 + 2 * state.air, 100);
            state.air--;
            bump_score(7);
            draw_start(my_GD);
            draw_level(my_GD);
            draw_willy(my_GD);
            draw_guardians(my_GD);
            draw_status(my_GD, 1);
            draw_controls(my_GD);
            GD_swap(my_GD);
        } /* end while */
        state.level = (state.level + 1) % 18;
        load_level(my_GD);
    } /* end if */
} /* end move_all */


static void draw_all(GD *my_GD)
{
    int     j;
    byte    midi    = pgm_read_byte_near(manicminer_tune + ((state.t >> 1) & 63));

    GD_cmd_regwrite(my_GD, REG_SOUND, 0x01 | (midi << 8));
    GD_cmd_regwrite(my_GD, REG_PLAY, 1);
    GD_get_inputs(my_GD);

    for (j = 0; j < OVER_FRAMES; j++) 
    {
        draw_start(my_GD);
        draw_level(my_GD);
        draw_willy(my_GD);
        draw_guardians(my_GD);
        draw_status(my_GD, 1);
        draw_controls(my_GD);
        GD_swap(my_GD);
        GD_cmd_regwrite(my_GD, REG_SOUND, 0);
        GD_cmd_regwrite(my_GD, REG_PLAY, 1);
    } /* end for */
} /* end draw_all */


static void game_over(GD *my_GD)
{
    byte    i;

    for (i = 0; i <= 96; i++) 
    {
        if (i & 1) 
        {
            GD_cmd_regwrite(my_GD, REG_SOUND, 0x01 | ((40 + (i / 2)) << 8));
        } 
        else 
        {
            GD_cmd_regwrite(my_GD, REG_SOUND, 0);
        } /* end if */
        GD_cmd_regwrite(my_GD, REG_PLAY, 1);

        GD_Clear(my_GD);
        GD_ClearColorRGB(my_GD, attr(9 + GD_random_1(my_GD, 7)));
        GD_ScissorSize(my_GD, 256, 128);
        GD_ScissorXY(my_GD, X0, Y0);
        GD_Clear(my_GD);

        if (i == 96)
            GD_cmd_text(my_GD, 122, 64, 29, OPT_CENTER, "GAME    OVER");

        GD_LineWidth(my_GD, 16 * 4);                            // Leg
        GD_Begin(my_GD, LINES);
        screen2ii(my_GD, 125, 0, 0, 0);
        screen2ii(my_GD, 125, i, 0, 0);

        GD_Begin(my_GD, BITMAPS);
        screen2ii(my_GD, 120, 128 - 16, SPECIALS_HANDLE, 1);    // Plinth
        screen2ii(my_GD, 120, i, SPECIALS_HANDLE, 2);           // Boot

        // Scissor so that boot covers up Willy
        GD_ScissorSize(my_GD, 480, 511);
        GD_ScissorXY(my_GD, 0, i + 16 + Y0);
        screen2ii(my_GD, 124, 128 - Y0, WILLY_HANDLE, 0);       // Willy
        GD_RestoreContext(my_GD);

        draw_status(my_GD, 1);
        GD_swap(my_GD);
    } /* end for */
} /* end game_over */


static int touch(GD *my_GD)
{
    if (dazzler)
        return (my_GD->inputs.wii[0].buttons != 0xFFFF);
    else
        return my_GD->inputs.touching;
} /* end touch */


static void title_screen(GD *my_GD)
{
    for (;;) 
    {
        uint16_t    i;
        int         x;
        int         y;

        for (i = 0; i < TITLE_FRAMES; i++) 
        {
            GD_get_inputs(my_GD);
            draw_start(my_GD);
            GD_ClearColorRGB(my_GD, attr(2));
            GD_Clear(my_GD);
            GD_Begin(my_GD, BITMAPS);
            screen2ii(my_GD, 0, 0, TITLE_HANDLE, 0);
            draw_status(my_GD, 0);

            GD_cmd_loadidentity(my_GD);
            GD_cmd_setmatrix(my_GD);

            // Text crawl across bottom
            GD_ScissorXY(my_GD, X0, Y0);
            GD_ScissorSize(my_GD, SF * 256, SF * 272);
            GD_ColorRGB(my_GD, WHITE);
            toscreen(&x, &y, 256 - i, 192 - 8);
            GD_cmd_text(my_GD, x, y, plain_font, OPT_CENTER, marquee);

            GD_swap(my_GD);
            if (touch(my_GD))
                return;
        } /* end for */

        for (state.level = 0; state.level < 19; state.level++) 
        {
            byte    t;
            
            load_level(my_GD);

            for (t = 0; t < 30; t++) 
            {
                draw_all(my_GD);
                move_all(my_GD);

                if (touch(my_GD))
                    return;
            } /* end for */
        } /* end for */
    } /* end for */
} /* end title_screen */

void setup_manic_miner(GD *my_GD,
                      GD_Transport *my_GD_Transport,
                      GD_SDCard *my_GD_SDCard,
                      GD_Reader *my_GD_Reader)
{
    GD_begin(my_GD, my_GD_Transport, my_GD_SDCard, my_GD_Reader, ~GD_STORAGE);

    // Handle     Graphic
    //   0        Miner Willy
    //   1        Guardians
    //   2        Background tiles
    //   3        Portals
    //   4        Items
    //   5        Title screen
    //   6        Specials: Eugene, Plinth and Boot
    load_assets(my_GD);

    GD_Clear(my_GD);
    GD_swap(my_GD);

    SF = 3;
    X0 = (my_GD->w - SF * 256) / 2;
    Y0 = (my_GD->h - SF * 192) / 2;
    plain_font = (SF < 3) ? 26 : 31;
} /* end setup_manic_miner */


void run_manic_miner(GD *my_GD)
{
    title_screen(my_GD);

    do 
    {
        GD_get_inputs(my_GD);
        GD_Clear(my_GD);
        GD_swap(my_GD);
    } while (touch(my_GD));

    state.level = START_LEVEL;
    state.score = 0;

    for (state.lives = 3; state.lives; state.lives--) 
    {
        load_assets(my_GD);
        load_level(my_GD);
        state.alive = 1;
        while (state.alive) 
        {
            draw_all(my_GD);
            move_all(my_GD);
        } /* end while */
    } /* end for */

    game_over(my_GD);
} /* end run_manic_miner */
