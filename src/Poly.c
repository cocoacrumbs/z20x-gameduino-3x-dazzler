#include "Poly.h"

/* ************************************************************************* */

void Poly_begin(Poly *self,
                GD *my_GD) 
{
    self->my_GD = my_GD;

    Poly_restart(self);

    GD_ColorMask(self->my_GD, 0, 0, 0, 0);
    GD_StencilOp(self->my_GD, KEEP, INVERT);
    GD_StencilFunc(self->my_GD, ALWAYS, 255, 255);
} /* end Poly_begin */

void Poly_draw(Poly *self) 
{
    Poly_paint(self);
    Poly_finish(self);
} /* end Poly_draw */

void Poly_finish(Poly *self)
{
    GD  *my_GD  = self->my_GD;

    GD_ColorMask(my_GD, 1, 1, 1, 1);
    GD_StencilFunc(my_GD, EQUAL, 255, 255);

    GD_Begin(my_GD, EDGE_STRIP_R);
    GD_Vertex2f(my_GD, 0, 0);
    GD_Vertex2f(my_GD, 0, (1 << my_GD->vxf) * my_GD->h);
} /* end Poly_finish */

void Poly_paint(Poly *self) 
{
    GD  *my_GD  = self->my_GD;

    self->x0 = max(0, self->x0);
    self->y0 = max(0, self->y0);
    self->x1 = min((1 << my_GD->vxf) * my_GD->w, self->x1);
    self->y1 = min((1 << my_GD->vxf) * my_GD->h, self->y1);
    GD_ScissorXY(my_GD, self->x0, self->y0);
    GD_ScissorSize(my_GD, self->x1 - self->x0 + 1, self->y1 - self->y0 + 1);
    GD_Begin(my_GD, EDGE_STRIP_B);
    Poly_perim(self);
} /* end Poly_paint */

void Poly_perim(Poly *self) 
{
    byte    i;

    for (i = 0; i < self->n; i++)
        GD_Vertex2f(self->my_GD, self->x[i], self->y[i]);
    GD_Vertex2f(self->my_GD, self->x[0], self->y[0]);
} /* end Poly_perim */

void Poly_restart(Poly *self) 
{
    GD  *my_GD  = self->my_GD;

    self->n  = 0;
    self->x0 = (1 << my_GD->vxf) * my_GD->w;
    self->x1 = 0;
    self->y0 = (1 << my_GD->vxf) * my_GD->h;
    self->y1 = 0;
} /* end Poly_restart */

void Poly_v(Poly *self,
            int _x, 
            int _y) 
{
    self->x0 = min(self->x0, _x >> self->my_GD->vxf);
    self->x1 = max(self->x1, _x >> self->my_GD->vxf);
    self->y0 = min(self->y0, _y >> self->my_GD->vxf);
    self->y1 = max(self->y1, _y >> self->my_GD->vxf);
    self->x[self->n] = _x;
    self->y[self->n] = _y;
    self->n++;
} /* end Poly_v */

/* ************************************************************************* */
