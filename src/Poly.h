#ifndef POLY_H
#define POLY_H

#include "GD_Defines.h"
#include "GD.h"

/* ************************************************************************* */

typedef struct Poly_T
{
    GD      *my_GD;
    int     x0;
    int     y0;
    int     x1;
    int     y1;
    int     x[8];
    int     y[8];
    byte    n;
} Poly;

/* ************************************************************************* */

void Poly_begin(Poly *self,
                GD *my_GD);

void Poly_draw(Poly *self);

void Poly_finish(Poly *self);

void Poly_paint(Poly *self);

void Poly_perim(Poly *self);

void Poly_restart(Poly *self);

void Poly_v(Poly *self,
            int _x, 
            int _y);

#endif POLY_H