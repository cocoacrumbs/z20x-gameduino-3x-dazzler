#include "Bitmap.h"

/* ************************************************************************* */

static uint8_t bpltab[] = 
{
    /* 0  ARGB1555  */ 0xFF,
    /* 1  L1        */ 3,
    /* 2  L4        */ 1,
    /* 3  L8        */ 0,
    /* 4  RGB332    */ 0,
    /* 5  ARGB2     */ 0,
    /* 6  ARGB4     */ 0xFF,
    /* 7  RGB565    */ 0xFF,
    /* 8  PALETTED  */ 0,
    /* 9  TEXT8X8   */ 0xFF,
    /* 10 TEXTVGA   */ 0xFF,
    /* 11 BARGRAPH  */ 0,
    /* 12           */ 0xFF,
    /* 13           */ 0xFF,
    /* 14           */ 0xFF,
    /* 15           */ 0xFF,
    /* 16           */ 0xFF,
    /* 17 L2        */ 2
};

/* ************************************************************************* */

static uint16_t Bitmap_stride(uint8_t format, 
                              uint16_t w)
{
    uint8_t     k;
    uint16_t    r;

    k = pgm_read_byte_near(bpltab + format);
    if (k == 0xFF)
        return w << 1;
    r = (1 << k) - 1;
    return ((w + r) >> k);
} /* end Bitmap_stride */

/* ************************************************************************* */

void Bitmap_begin(Bitmap *self,
                  GD *my_GD)
{
    self->my_GD = my_GD;
} /* end Bitmap_begin */

void Bitmap_bind(Bitmap *self,
                 uint8_t h)
{
    self->handle = h;
    GD_BitmapHandle(self->my_GD, self->handle);
    Bitmap_setup(self);
} /* end Bitmap_bind */

void Bitmap_fromfile(Bitmap *self,
                     const char* filename, 
                     int format)
{
    GD         *my_GD   = self->my_GD;
    uint32_t    ptr;
    uint32_t    w;
    uint32_t    h;

    my_GD->loadptr = (my_GD->loadptr + 1) & ~1;
    GD_cmd_loadimage(my_GD, my_GD->loadptr, OPT_NODL);
    GD_load(my_GD, filename);
    GD_cmd_getprops(my_GD, &ptr, &w, &h);
    GD_finish(my_GD);
    self->size.x = GD_rd16(my_GD, w);
    self->size.y = GD_rd16(my_GD, h);
    // defaults(format);
} /* end Bitmap_fromfile */

void Bitmap_fromfile_default_format(Bitmap *self,
                                    const char* filename)
{
    Bitmap_fromfile(self, filename, 7);
} /* end Bitmap_fromfile_default_format */

void Bitmap_setup(Bitmap *self)
{
    GD_BitmapSource(self->my_GD, self->source);
    GD_BitmapLayout(self->my_GD, self->format, Bitmap_stride(self->format, self->size.x), self->size.y);
    GD_BitmapSize(self->my_GD, NEAREST, BORDER, BORDER, self->size.x, self->size.y);
} /* end Bitmap_setup */

void Bitmap_wallpaper(Bitmap *self)
{
    GD  *myGD   = self->my_GD;
    int  x;
    int  y;

    if (self->handle == -1) 
    {
        GD_BitmapHandle(myGD, 15);
        Bitmap_setup(self);
    }
    else 
    {
        GD_BitmapHandle(myGD, self->handle);
    } /* end if */

    GD_Begin(myGD, BITMAPS);

    // if power-of-2, can just use REPEAT,REPEAT
    // otherwise must draw it across whole screen
    if (IS_POWER_2(self->size.x) && IS_POWER_2(self->size.y)) 
    {
        GD_BitmapSize(myGD, NEAREST, REPEAT, REPEAT, myGD->w, myGD->h);
        GD_Vertex2f(myGD, 0, 0);
    } 
    else 
    {
        for (x = 0; x < myGD->w; x += self->size.x)
            for (y = 0; y < myGD->h; y += self->size.y)
                GD_Vertex2f(myGD, x << 4, y << 4);
    } /* end if */
} /* end Bitmap_wallpaper */
