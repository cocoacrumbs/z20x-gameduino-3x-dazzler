#include <eZ80.h>
#include <stdio.h>

#include "spi.h"
#include "timer.h"

#include "GD.h"

/* ************************************************************************* */

// Big examples
#include "blobs.h"
#include "cobra.h"
#include "cube.h"
#include "cube_movie.h"
#include "frogger.h"
#include "logo.h"
#include "manic_miner.h"
#include "terminal.h"

/* ************************************************************************* */

GD              my_GD;
GD_Transport    my_GD_Transport;
GD_Reader       my_GD_Reader;
GD_SDCard       my_GD_SDCard;

/* ************************************************************************* */

void run_HelloWorld_GD3X() 
{
    GD_begin_default_options(&my_GD, &my_GD_Transport, &my_GD_SDCard, &my_GD_Reader);

    GD_ClearColorRGB(&my_GD, 0x803000);
    GD_Clear(&my_GD);
    GD_cmd_text(&my_GD, (my_GD.w / 2), (my_GD.h / 2), 31, OPT_CENTER, "Hello World from Z20X!");
    GD_swap(&my_GD);
} /* end run_HelloWorld_GD3X */


void run_LargePoints_GD3X()
{
    GD_begin_default_options(&my_GD, &my_GD_Transport, &my_GD_SDCard, &my_GD_Reader);

    GD_ClearColorRGB(&my_GD, 0x103000);
    GD_Clear(&my_GD);
    GD_cmd_text(&my_GD, (my_GD.w / 2), (my_GD.h / 2), 31, OPT_CENTER, "Hello world");
    GD_PointSize(&my_GD, 16 * 30);              // means 30 pixels
    GD_Begin(&my_GD, POINTS);
    // The screen resolution is 1280 by 720 pixels. This exceeds what can be addressed 
    // with the shorthand GD_Vertex2ii(). Hence we use here the combo of GD_VertexFormat()
    // and GD_Vertex2f(). 
    GD_VertexFormat(&my_GD, 0);                 // integer coordinates
    GD_Vertex2f(&my_GD, (my_GD.w / 2) - 20, (my_GD.h / 2) - 35);
    GD_Vertex2f(&my_GD, (my_GD.w / 2) + 20, (my_GD.h / 2) + 35);
    GD_swap(&my_GD);
} /* end run_LargePoints */


void run_LargePointsColorTransparency_GD3X()
{
    GD_begin_default_options(&my_GD, &my_GD_Transport, &my_GD_SDCard, &my_GD_Reader);

    GD_ClearColorRGB(&my_GD, 0x103000);
    GD_Clear(&my_GD);
    GD_cmd_text(&my_GD, (my_GD.w / 2), (my_GD.h / 2), 31, OPT_CENTER, "Hello world");
    GD_PointSize(&my_GD, 16 * 30);              // means 30 pixels
    GD_Begin(&my_GD, POINTS);
    // The screen resolution is 1280 by 720 pixels. This exceeds what can be addressed 
    // with the shorthand GD_Vertex2ii(). Hence we use here the combo of GD_VertexFormat()
    // and GD_Vertex2f(). 
    GD_VertexFormat(&my_GD, 0);                 // integer coordinates
    GD_ColorRGB(&my_GD, 0xFF8000);              // orange    
    GD_Vertex2f(&my_GD, (my_GD.w / 2) - 20, (my_GD.h / 2) - 35);
    GD_ColorRGB(&my_GD, 0x0080FF);              // teal
    GD_Vertex2f(&my_GD, (my_GD.w / 2) + 20, (my_GD.h / 2) + 35);    
    GD_swap(&my_GD);
} /* end run_LargePoints */


void run_Fizz_GD3X()
{
    short   i;

    GD_begin_default_options(&my_GD, &my_GD_Transport, &my_GD_SDCard, &my_GD_Reader);

    while (1)
    {
        GD_Clear(&my_GD);
        GD_Begin(&my_GD, POINTS);
        for (i = 0; i < 100; i++)
        {
            GD_PointSize(&my_GD, GD_random_1(&my_GD, 16 * 50));
            GD_ColorR_G_B(&my_GD, GD_random_1(&my_GD, 256),
                                  GD_random_1(&my_GD, 256),
                                  GD_random_1(&my_GD, 256));
            GD_ColorA(&my_GD, GD_random_1(&my_GD, 256));
            GD_VertexFormat(&my_GD, 0);             // integer coordinates
            GD_Vertex2f(&my_GD, GD_random_1(&my_GD, my_GD.w), GD_random_1(&my_GD, my_GD.h));
        } /* end for */
        GD_swap(&my_GD);
    } /* end while */
} /* end run_Fizz_GD3X */


void run_Play_GD3X()
{
    GD_begin_default_options(&my_GD, &my_GD_Transport, &my_GD_SDCard, &my_GD_Reader);

    // Long delay so that the monitor can sync with HDMI before outputting any sound.
    delayms(3000);                          
    GD_playNote(&my_GD, PIANO, 60);
    delayms(1000);
    GD_playNote(&my_GD, ORGAN, 64);
    delayms(1000);
    GD_play(&my_GD, CLICK, 0);
    delayms(1000);
    GD_play(&my_GD, SWITCH, 0);
    delayms(1000);
    GD_play(&my_GD, COWBELL, 0);
    delayms(1000);
    GD_play(&my_GD, NOTCH, 0);
    delayms(1000);
    GD_play(&my_GD, HIHAT, 0);
    delayms(1000);
    GD_play(&my_GD, KICKDRUM, 0);
    delayms(1000);
    GD_play(&my_GD, POP, 0);
    delayms(1000);
    GD_play(&my_GD, CLACK, 0);
    delayms(1000);
    GD_play(&my_GD, CHACK, 0);
    delayms(1000);
} /* end run_Play_GD3X */


void run_loadJPEG_GD3X()
{
    GD_begin_default_options(&my_GD, &my_GD_Transport, &my_GD_SDCard, &my_GD_Reader);
    GD_cmd_loadimage(&my_GD, 0, 0);
    GD_load(&my_GD, "healsky3.jpg");

    GD_Clear(&my_GD);
    GD_Begin(&my_GD, BITMAPS);
    // GD_Vertex2ii(&my_GD, 0, 0);
    GD_VertexFormat(&my_GD, 0);                 // Use integer coordinates with GD_Vertex2f
    GD_Vertex2f(&my_GD, 0, 0);
    GD_swap(&my_GD);
} /* end run_loadJPEG_GD3X */

/* ************************************************************************* */

void run_blobs_GD3X()
{
    setup_blobs(&my_GD, &my_GD_Transport, &my_GD_SDCard, &my_GD_Reader);
    run_blobs(&my_GD);
} /* end run_blobs_GD3X */

void run_cobra_GD3X()
{
    setup_cobra(&my_GD, &my_GD_Transport, &my_GD_SDCard, &my_GD_Reader);
    run_cobra(&my_GD);
} /* end run_cobra_GD3X */

void run_cube_GD3X()
{
    setup_cube(&my_GD, &my_GD_Transport, &my_GD_SDCard, &my_GD_Reader);
    run_cube(&my_GD);
} /* end run_cube_GD3X */

void run_cube_movie_GD3X()
{
    setup_cube_movie(&my_GD, &my_GD_Transport, &my_GD_SDCard, &my_GD_Reader);
    run_cube_movie(&my_GD);
} /* end run_cube_GD3X */

void run_frogger_GD3X()
{
    setup_frogger(&my_GD, &my_GD_Transport, &my_GD_SDCard, &my_GD_Reader);
    run_frogger(&my_GD);
} /* end run_logo_GD3X */

void run_logo_GD3X()
{
    setup_logo(&my_GD, &my_GD_Transport, &my_GD_SDCard, &my_GD_Reader);
    run_logo(&my_GD);
} /* end run_logo_GD3X */

void run_manic_miner_GD3X()
{
    setup_manic_miner(&my_GD, &my_GD_Transport, &my_GD_SDCard, &my_GD_Reader);
    run_manic_miner(&my_GD);
} /* end run_manic_miner_GD3X */

void run_terminal_GD3X()
{
    GD_Terminal my_GD_terminal;

    setup_terminal(&my_GD, &my_GD_Transport, &my_GD_SDCard, &my_GD_Reader, &my_GD_terminal);
    run_terminal(&my_GD, &my_GD_terminal);
} /* end run_terminal_GD3X */

/* ************************************************************************* */

int main()
{
    int result  = 0;

    timer2_init(1);             // Set to 1 ms interval
    init_hw();
    result = spi_begin();
       
    // Quick Start
    // ===========
    // run_HelloWorld_GD3X();
    // run_LargePoints_GD3X();
    // run_LargePointsColorTransparency_GD3X();
    // run_Fizz_GD3X();
    // run_Play_GD3X();

    // Bitmap examples
    // ===============
    // run_loadJPEG_GD3X();

    // Big examples
    // ============
    // run_blobs_GD3X();
    run_cobra_GD3X();
    // run_cube_GD3X();
    // run_cube_movie_GD3X();
    // run_frogger_GD3X();
    // run_logo_GD3X();
    // run_manic_miner_GD3X();
    // run_terminal_GD3X();

    return 0;
} /* end main */
