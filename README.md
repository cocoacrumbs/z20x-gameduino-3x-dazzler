# Gameduino 3X Dazzler on the Z20X

![picture](./Images/2021-10-22-pic000.jpg)

This repo contains a ported GD2 library to support the 
Gameduino 3X Dazzler on the Z20X computer.

For more information see the [blogpost](<https://www.cocoacrumbs.com/blog/2021-10-22-gameduino-3x-on-z20x/>)

![picture](./Images/2021-10-22-pic001.jpg)
